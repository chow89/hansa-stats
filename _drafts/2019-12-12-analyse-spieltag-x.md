---
title: "Kurzanalyse 20. Spieltag: Viktoria Köln gegen Hansa Rostock"
description: "Kurzanalyse zum 20. Spieltag 2019/20 zwischen Viktoria Köln und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Viktoria Köln, Albert Bunjaku, Lukas Scherff, Tanju Öztürk, Markus Kolke, Elsamed Ramaj, Pascal Breier, Simon Handle, Rasmus Pedersen, Mike Wunderlich, Richmond Tachie]
categories: []
---

Am letzten Spieltag des Jahres und gleichzeitig erstem Spiel der Rückrunde konnte der FC Hansa bei der Viktoria aus Köln nochmal richtig überzeugen und mit 5:1 gewinnen.

![xG-Plot 20. Spieltag 2019/2020 Viktoria Köln - Hansa Rostock]({{ site.url }}/images/xg-plot-day-20-viktoria-koeln.png)

<canvas id="xgplot192020"></canvas>
<script>
var start = function() {
    new HansaStatsChart.XgPlot("#xgplot192020", {
        team1: {
            name: "Hansa",
            points: [[11, 0.04, 'Tor Scherff'], [16, 0.05, 'Tor Öztürk'], [29, 0.21, 'Tor Breier'], [35, 0.01], [65, 0.08], [75, 0.5], [80, 0.28, 'Tor Pedersen'], [89, 0.67, 'Tor Pedersen']]
        },
        team2: {
            name: "Vikoria Köln",
            points: [[6, 0.5], [37, 0.1], [39, 0.02], [60, 0.13], [68, 0.16], [86, 0.3], [87, 0.24, 'Tor Tachie']]
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Rostocker versuchten von Beginn an das Spiel zu bestimmen und pressten früh im Spiel gegen den Ball.
Zunächst konnten sie sich dadurch keine Chancen herausarbeiten, stattdessen nutzten die Kölner die hochstehende Abwehr ihrerseits und konterten.
Der freistehende Albert Bunjaku konnte den Ball in der 6. Minute aus kurzer Distanz aber nicht über die Torlinie bringen.
Die Rostocker ließen sich davon aber nicht einschüchtern.
In der 11. Minuten nutzten sie einen Fehler der Kölner im Spielaufbau zur 1:0-Führung.
Lukas Scherff verwertete eine etwas zu lang geratene Ahlschwede-Flanke und netzte früh im Spiel ein.
Nur fünf Minuten später konnten die Hanseaten wieder über die rechte Seite eine weitere Chance einleiten, die am Ende Tanju Öztürk mit dem 2:0 veredelte.
Anschließend wurde es etwas ruhiger, ohne dass eine der beiden Mannschaften gefährlich vor das Tor kam.
Die Rostocker begannen sich etwas zurückzuziehen und den Kölner mehr Spielanteile zu geben.
In der 29. Minute konnten die Rostocker die dann höher stehenden Rheinländer mit einem langen Abschlag von Markus Kolke und einer Kopfballverlängerung vom Startelf-Debütanten Elsamed Ramaj überraschen und Pascal Breier alleine auf das Kölner Gehäuse zulaufen lassen.
Breier nutzte die Möglichkeit und erhöhte auf 3:0.
In den letzten 15 Minuten der ersten Halbzeit passierte dann nichts mehr und so gingen beide Teams mit dem 3:0 in die Kabinen.

Mit Beginn des zweiten Spielabschnittes verflachte das Spiel. Die Ostseestädter verwalteten die komfortable Führung, während die Kölner kein Weg vor das Rostocker Gehäuse fanden.
Und so blieb auch das erste Drittel der zweiten Halbzeit ohne nennenswerte Ereignisse.
In der 60. Minute haben es die Kölner dann geschafft die Rostocker Abwehr zu überspielen, aber Bunjaku konnte die flache Hereingabe erneut nicht verwerten und schoss den Ball auf die Tribüne.
Acht Minute später machte es Simon Handle aus ähnlicher Position besser, er scheiterte aber an Kolke.
In der Schlussviertelstunde begannen die Rostocker wieder damit aktiv am Spiel teilzunehmen und versuchten das Spiel endgültig zu entscheiden.
In der 75. Minute war es zunächst Rasmus Pedersen, der eine Breier-Hereingabe etwas verstolperte und dann am Kölner Torhüter scheiterte.
Weitere fünf Minuten später machte er es dann besser und köpfte eine Vollmann-Ecke unbedrängt zum 4:0 ein.
Die Kölner gaben sich danach aber noch nicht auf und versuchten durch Mike Wunderlich in der 86. Minute noch den Anschlusstreffer zu erzielen.
Aber auch er scheiterte an Kolke.
In der nächste Minute war es dann Richmond Tachie, dem mit etwas Glück der Ehrentreffer zum 4:1 gelang.
Doch damit war das Spiel noch nicht zu Ende.
Kurz vor Schluss war es erneut Pedersen, der eine Hereingabe von Scherff zum 5:1 verwandelte.
Nur wenige Minuten später pfiff der Schiedsrichter das Spiel ab.

Aus stochastischer Sicht fällt das Spiel nicht so deutlich aus. Zwar gewannen die Rostocker nach expected Goals das Spiel mit 1.84:1.45, daraus ergibt sich aber nur eine Siegwahrscheinlichkeit von 47,36% und eine Niederlagenwahrscheinlichkeit von 25,96% (Unentschieden 26,68%).
Das wahrscheinlichste Ergebnis ist demnach ein 2:1-Auswärtssieg (14,32%).
Es folgen ein 2:2 und 1:1 (11,49% bzw. 11,48%).
Ein 5:1-Sieg ist aufgrund der wenigen Torchancen mit nur 0,18% sehr unwahrscheinlich.
