---
layout: page
title: "Vorhersage der Abschlusstabelle"
share: false
---

<canvas id="prediction2021"></canvas>
<script>
    var data = {{ site.data.fixtures['2021'] | jsonify }};
    var keys = Object.keys(data);
    var matches = keys.flatMap((key) => data[key].map((match) => ({...match, matchday: parseInt(key)})));
    new HansaStatsChart.SeasonPlot('#prediction2021', matches, true);
</script>

Stand: <time datetime="{{ site.data.fixtures.updatedAt | date_to_xmlschema }}">{{ site.data.fixtures.updatedAt | date: "%-d. " }}{% assign monthAsString = site.data.fixtures.updatedAt | date: "%B" | downcase | strip %}{{ site.data.languages.locales[site.locale][monthAsString] }}{{ site.data.fixtures.updatedAt | date: " %Y" }}</time>
