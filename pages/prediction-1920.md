---
layout: page
title: "Vorhersage der Abschlusstabelle"
share: false
permalink: /vorhersage-1920
---

<canvas id="prediction1920"></canvas>
<script>
    var data = {{ site.data.fixtures['1920'] | jsonify }};
    var keys = Object.keys(data);
    var matches = keys.flatMap((key) => data[key].map((match) => ({...match, matchday: parseInt(key)})));
    new HansaStatsChart.SeasonPlot('#prediction1920', matches, true);
</script>

Stand: <time datetime="{{ site.data.fixtures.updatedAt | date_to_xmlschema }}">{{ site.data.fixtures.updatedAt | date: "%-d. " }}{% assign monthAsString = site.data.fixtures.updatedAt | date: "%B" | downcase | strip %}{{ site.data.languages.locales[site.locale][monthAsString] }}{{ site.data.fixtures.updatedAt | date: " %Y" }}</time>
