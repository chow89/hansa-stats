---
layout: page
title: "Justice Table 3. Liga"
share: false
---

<table id="justice-table"></table>
<canvas id="points-xpoints-plot" style="margin-top: 20px;"></canvas>
<script>
    var data = {{ site.data.fixtures['2021'] | jsonify }};
    var keys = Object.keys(data);
    var matches = keys.flatMap((key) => data[key].map((match) => ({...match, matchday: parseInt(key)})))
    new HansaStatsChart.JusticeTable('#justice-table', matches);

    new HansaStatsChart.XpointsToPointsPlot('#points-xpoints-plot', matches);
</script>

Stand: <time datetime="{{ site.data.fixtures.updatedAt | date_to_xmlschema }}">{{ site.data.fixtures.updatedAt | date: "%-d. " }}{% assign monthAsString = site.data.fixtures.updatedAt | date: "%B" | downcase | strip %}{{ site.data.languages.locales[site.locale][monthAsString] }}{{ site.data.fixtures.updatedAt | date: " %Y" }}</time>