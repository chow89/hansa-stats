---
layout: page
title: "Mannschaften im Vergleich"
share: false
---

### Offensive und Defensive

<canvas id="offense-defense-comparison"></canvas>
<script>
var start = function() {
    var data = {{ site.data.fixtures['2021'] | jsonify }};
    var keys = Object.keys(data);
    var matches = keys.flatMap((key) => data[key].map((match) => ({...match, matchday: parseInt(key)})))
    new HansaStatsChart.OffenseDefenseComparisonPlot("#offense-defense-comparison", matches);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Stand: <time datetime="{{ site.data.fixtures.updatedAt | date_to_xmlschema }}">{{ site.data.fixtures.updatedAt | date: "%-d. " }}{% assign monthAsString = site.data.fixtures.updatedAt | date: "%B" | downcase | strip %}{{ site.data.languages.locales[site.locale][monthAsString] }}{{ site.data.fixtures.updatedAt | date: " %Y" }}</time>