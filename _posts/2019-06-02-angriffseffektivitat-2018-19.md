---
title: "Angriffseffektivität 2018/19"
description: "Überblick zur Effektivität der Offensive"
tags: [Marco Königs, Pascal Breier, Cebio Soukou, Merveille Biankadi, Marcel Hilßner, Lukas Scherff, Kai Bülow, Oliver Hüsing, Jonas Hildebrandt, Mirnes Pepic]
categories: []
---

Der [letzte Beitrag]({% post_url  2019-05-25-saison-ruckblick-2018-19 %}) hat gezeigt, dass die Hanseaten in der abgelaufen Saison Probleme mit der Chancenverwertung hatten.
In diesem Beitrag soll es nun darum zu gehen, diesen Punkt detaillierter zu beleuchten.

Das folgende Diagramm soll die Effektivität der Spieler zeigen.
Dabei sind nur Spieler berücksichtigt, die mindestens 20% der maximal möglichen Spielzeit gespielt{% fn %}  und mindestens ein Tor geschossen haben{% fn %}.
Auf der x-Achse werden die durchschnittlichen Tore dargestellt.
Die y-Achse zeigt die durchschnittlichen erwarteten Tore (xG).
Durch die Größe der Blasen wird die durchschnittliche Anzahl der Schüsse auf das Tor abgebildet.
Je größer die Blase, desto mehr Schüsse wurden abgegeben.
Alle drei Werte sind auf 90 Minuten Spielzeit normalisiert.
Aus dem Verhältnis von geschossenen Toren zu erwarteten Tore lässt sich somit feststellen, welche Spieler ihre Torchancen auch effektiv nutzen und welche Spieler zu viele Chancen liegen lassen.
Zudem lässt sich mit der Blasengröße auch eine Aussage über die Qualität der Torchance treffen.
Die Diagonale zeigt die "Effektivitätsgrenze".
Spieler unter der Linie sind effektiver als erwartet, Spieler darüber schießen zu wenige Tore.

![Angriffseffektivität 2018/19]({{ site.url }}/images/offensive-1819.png)

Auf den ersten Blick ist zu erkennen, dass es eine Zweiteilung in der Offensive gibt, oben rechts befinden sich alle Stürmer (Königs, Breier, Soukou) und Biankadi.
Unten links sind dann die restlichen Spieler (Hilßner, Scherff, Bülow, Hüsing, Hildebrandt, Pepic).
Diese Teilung ist durchaus üblich, kann aber darauf hindeuten, dass vom Mittelfeld zu wenig Torgefahr ausgeht.

Einen weiterer Punkt ist, dass alle drei Stürmer über der "Effektivitätsgrenze" liegen.
So sind Pascal Breier 50%, Marco Königs 36% und Cebio Soukou 20% über dem Erwartungswert.
Im Gegensatz dazu ist Merveille Biankadi deutlich effektiver als es seine Torchance zu lassen würde (28% unter der Erwartung).
Anders ausgedrückt heißt das, dass Biankadi selbst aus schlechten Chancen Tore macht, währende die Stürmer aus guten Chancen zu selten Tore machen.
Alle vier Spieler haben zudem ungefähr gleich oft auf das Tor geschossen.

Im Bereich unten links sieht es etwas anders aus.
Alle Spieler liegen dicht an der "Effektivitätsgrenze".
Einzig Kai Bülow fällt dort positiv heraus.
Bülow hat aus seinen Chancen fasst doppelt soviele Tore gemacht, wie es zu erwarten gewesen wäre (47% unter dem Erwartungswert).
Bei Marcel Hilßner ist zu sehen, dass die Blase im Vergleich zu den anderen Spieler in seinem Bereich sehr groß ist, sogar größer als die Blasen der Spieler oben rechts.
Daraus lässt sich folgern, dass Hilßner sehr oft auf das Tor schießt, aber dass meist aus sehr schlechten Positionen, wodurch der kleine xG-Wert zustande kommt.

---

{% footnotes %}
{% fnbody %}
mindestens 684 Minuten
{% endfnbody %}
{% fnbody %}
Durch diese Einschränkungen sind Anton Donkor und Del-Angelo Williams aus der Wertung herausgefallen.
{% endfnbody %}
{% endfootnotes %}
