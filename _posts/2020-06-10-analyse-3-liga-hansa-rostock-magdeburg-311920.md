---
title: "Kurzanalyse 31. Spieltag: Hansa Rostock gegen FC Magdeburg"
description: "Kurzanalyse zum 31. Spieltag 2019/20 zwischen Hansa Rostock und FC Magdeburg"
tags: [3. Liga, FC Hansa Rostock, FC Magdeburg, Korbinian Vollmann, Aaron Opoku, John Verhoek, Lukas Scherff, Thore Jacobsen, Nils Butzen, Maximilian Ahlschwede, Sören Bertram, Markus Kolke, Marcel Costly, Mirnes Pepic, Nico Granatowski, Jürgen Gjasula, Pascal Breier, Daniel Hanslik]
categories: []
---

Am 31. Spieltag konnte der FC Hansa nach dem [Sieg in Meppen]({% post_url 2020-06-07-analyse-3-liga-sv-meppen-hansa-rostock-301920 %}) auch einen Sieg im prestigeträchtigen Spiel gegen den FC Magdeburg feiern.
Nach einer umkämpften Partie gewannen die Rostocker mit 3:1.

<canvas id="xgplot311920"></canvas>
<canvas id="winratio311920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['31'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot311920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_MAGDEBURG,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio311920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hanseaten traten wie gewohnt dominant auf und versuchten die Magdeburger nicht ins Spiel kommen zu lassen.
Da die Gäste ihrerseits auch kein Interesse daran hatten, war die gesamte erste Halbzeit ein Spiel auf ein Tor.
Schon in der zweiten Minute hatten die Rostocker Kicker ihre erste Möglichkeit.
Die Flanke von Korbinian Vollmann auf Aaron Opoku war jedoch etwas zu hoch, so dass die Leihgabe vom HSV den Ball nicht aufs Tor bringen konnte.
Danach konnte die Madgeburger Defensive die Rostocker Angriffsbemühungen besser unter Kontrolle bringen.
Erst in der 21. Minute wurde es vor dem Gästetor wieder gefährlich.
John Verhoek kam fünf Meter vor dem Tor fast unbedrängt zum Kopfball.
Auch eher konnte den Ball nicht richtig platzieren und so ging dieser knapp neben das Tor.
Wenig später hatte Vollmann nach einer Scherff-Flanke in der Nähe des Elfmeterpunktes die nächste Chance.
Sein Schuss konnte aber von Thore Jacobsen geblockt werden.
In der 36. Minute durfte sich Nils Butzen mit einem Schuss versuchen, doch auch dieser konnte von der Abwehr geblockt werden.
Bei der anschließenden Ecke hatte erneut Verhoek die Möglichkeit zum inzwischen verdienten Führungstreffer.
Sein Kopfball war aber zu schlecht getimed und ging weit am Tor vorbei.
Drei Minuten vor Ende der Halbzeit drangen die Rostocker erneut in den Strafraum ein.
Das Tempodribbling von Opoku konnte dieses Mal nur noch mit einem Foul gestoppt werden.
Den fälligen Strafstoß verwandelte Maximilian Ahlschwede souverän zum 1:0.
Mit diesem Zwischenstand gingen die beiden Teams anschließend in die Halbzeitpause.

In der ersten Viertelstunde der zweiten Halbzeit spielte sich die Partie im Mittelfeld ab.
Die Rostocker bemühten sich mit langen Ballpassagen um Sicherheit und die Gäste hatten immer noch kein Interesse daran am Spiel teilzunehmen.
In der 58. Minute war es wieder Opoku, der eine Vollmann-Flanke ungehindert im Strafraum annehmen konnte.
Den anschließenden Torschuss konnten die Gäste dann wieder abblocken.
Zwei Minuten danach hatten die Magdeburger dann ihre erste nennenswerte Torchance des Spiels.
Der Freistoß von Sören Bertram stellte Markus Kolke im Rostocker Gehäuse aber vor keine großen Probleme.
Weitere sechs Minuten später hatte Marcel Costly nach einer Ecke die nächste Chance zum Ausgleich.
Sein Kopfball landete jedoch nur auf dem Tornetz.
Durch den sich aufbauenden Druck der Gäste ergaben sich für die Gastgeber immer wieder Lücken in der gegnerischen Defensive.
In der 70. Minute konnten sie eine dieser Lücken nutzten.
Aus einer Umschaltsituation spielten Lukas Scherff, Mirnes Pepic und Verhoek Nico Granatowski direkt vor dem Magdeburger Tor frei, so dass dieser nur noch zum 2:0 einschieben musste.
Nach einem kurzen Schock warfen die Gäste dann nochmal alles nach vorne um zumindest noch einen Punkt mitzunehmen.
In der 79. Minute wurden ihr Bemühungen dann von der Rostocker Abwehr und dem Schiedsrichter mit einem Elfmeter belohnt.
Diesen verwandelte Jürgen Gjasula sicher zum 2:1.
Die Hansa-Abwehr fing nun an zu wackeln und so hatte Costly in der 82. Minute die große Chance zum Ausgleich.
Sein Schuss verfehlte das Tor nur knapp.
Den Schlusspunkt durfte in der Nachspielzeit dann Pascal Breier setzen.
Er schloss einen Konter über Daniel Hanslik und Granatowski erfolgreich zum 3:1-Endstand ab.

Das dominante Spiel spiegelt sich auch in der stochastischen Betrachtung wider.
In 69% der Fällen gewinnen die Rostocker, wobei das 2:0 (14%), das 2:1 (13%) und das 1:0 (12%) die wahrscheinlichsten Ergebnisse sind.
In 20% der Spielen teilen sich beide Mannschaften die Punkte und in immer noch 11% aller Spiele gewinnen die Gäste.
Die Wahrscheinlichkeit eines 3:1-Heimerfolgs liegt bei etwa 8%.