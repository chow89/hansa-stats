---
title: "Kurzanalyse 1. Spieltag: Hansa Rostock gegen Viktoria Köln"
description: "Kurzanalyse zum 1. Spieltag 2019/20 zwischen Hansa Rostock und Viktoria Köln"
tags: [3. Liga, FC Hansa Rostock, Viktoria Köln]
categories: []
---

Der 1. Spieltag ist rum und der FC Hansa konnte gleich beeindrucken - aber auch enttäuschen.
Bereits nach 20 Minuten führten die Koggekicker mit 3:0, mussten sich am Ende aber mit einem 3:3 und einem Punkt begnügen.

<canvas id="xgplot011920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['1'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot011920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.VIKTORIA_KÖLN,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Der xG-Plot zeigt, dass beide Teams im gesamten Spiel ungefähr gleichauf bei der Qualität der Torchancen waren.
Es ist gut zuerkennen, dass die ersten beiden Tore des Spiels (9. und 13. Minute) im Plot nicht relevant sind.
Das liegt daran, dass das 1:0 ein Eigentor war und das zweite Tor ein Schuss von der Strafraumgrenze.
Erst das 3:0 zeigt einen signifikanten Anstieg.
Zu diesem Zeitpunkt hat Hansa einen Gesamt-xG von etwa 0,3 Tore/Spiel und war damit zehnmal effektiver erwartet.
Erst in der anschließenden Viertelstunde konnten die Hanseaten die Führung mit entsprechenden Torchancen untermauern.
Leider waren das auch die letzten nennenswerten Torchancen der Rostocker.

Ganz anders lief das Spiel für Viktoria Köln.
Die Kölner fanden anfangs überhaupt nicht ins Spiel, konnten ihre erste Torchance in der 27. Minute aber gleich zum Anschlusstreffer nutzen.
In der Folge konnten die Domstädter sich immer wieder Torchancen erarbeiten und zweimal auch nutzen.
Mit dem Unentschieden gab sich die Mannschaft von Pavel Dotchev jedoch nicht zufrieden und blieb bis zum Ende des Spiels gefährlich.
Am Ende waren sie dem 4:3 sogar näher als die Hanseaten.
