---
title: "Kurzanalyse 20. Spieltag: MSV Duisburg gegen Hansa Rostock"
description: "Kurzanalyse zum 20. Spieltag 2020/21 zwischen MSV Duisburg und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, MSV Duisburg, Nico Neidhart, John Verhoek, Manuel Farrona Pulido, Stefan Velkov, Jan Löhmannsröben, Arne Sicker, Julian Hettwer, Ahmet Engin, Markus Kolke]
categories: []
---

Nach der Zwangspause am letzten Wochenende ging es für die Rostocker am 20. Spieltag zum MSV Duisburg.
Nach 90 Minuten gewannen die Rostocker das Spiel mit 1:2.

<canvas id="xgplot202021"></canvas>
<canvas id="winratio202021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['20'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot202021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.MSV_DUISBURG,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio202021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio202021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften begegneten sich von Anfang an auf Augenhöhe.
Nach weniger als fünf Minuten wurde es dann erstmals vor dem Duisburger Tor gefährlich.
Nico Neidhart flankte den Ball von der rechten Seiten auf John Verhoek, der das Spielgerät dann auf Manuel Farrona Pulido ablegte.
Der Mittelfeldspieler zog sofort ab und verwandelte unter Mithilfe des Verteidiger, der den Ball unhaltbar abfälschte, zum 0:1.
Die Hausherren ließen sich vom frühen Rückstand aber nicht beeindrucken und hatten ihrerseites nur drei Minuten danach die große Chance zum Ausgleich.
Bei einem Freistoß kam Stefan Velkov als Erster an den Ball, er konnte die Kugel aber nicht voll treffen und so ging es mit einem Abstoß weiter.
In der Folge wurde es vor den Tore etwas ruhiger, ehe Farrona Pulido in der 29. Minute mit dem 0:2 einen Doppelpack schnürte.
Wieder war es Neidhart der nach der Balleroberung schnell umschaltete und dann die Vorlage gab.
Farrona Pulido wurde beim Schuss von der Strafraumgrenze nicht von der Verteidigung gestört und netzte so sehenswert ein.
Auch nach dem erneuten Nackenschlag gaben sich die Duisburger nicht auf und erspielten sich nur wenigen Minuten später einen Elfmeter, nachdem Jan Löhmannsröben eine Flanke mit dem Arm klärte.
Kapitän Arne Sicker verwandelte sicher zum 1:2.
Die Hanseaten versuchten anschließend den Zwei-Tore-Vorsprung wiederherzustellen.
So war es Löhmannsröben der nach einer Ecke in der 37. Minute mit dem Kopf das nächste Tor erzielen wollte, er scheiterte aber am Duisburger Torwart.
Danach ging es für beide Teams erstmal wieder in die Kabine.

Nach der Pause verlor die Partie etwas an Schwung und die Rostocker fokussierten sich mit zunehmender Spielzeit darauf das Ergbebnis über die Zeit zu bringen.
In der 52. Minute hatten somit auch die Duisburger die erste Torchance der Halbzeit.
Eine Flanke landete auf dem Kopf von Julian Hettwer, der aber ohne große Bedrängnis nur über das Tor köpfte.
Auf der Gegenseite hatte der Doppeltorschütze Farrona Pulido in der 62. Minute die nächste Möglichkeit um auf 1:3 zu erhöhen.
Sein Schuss aus etwa zehn Metern wurde aber geblockt.
Nur vier Minuten danach kam erneut Hettwer freistehend zum Kopfball, doch auch bei diesem Versuch gelang es ihm nicht den Ball auf das Tor zu bringen.
In der 79. Minute tauchte dann Ahmet Engin nach einem guten Steilpass vor dem Rostocker Tor auch.
Markus Kolke im Tor konnte den Schuss aber entschärfen.
Zwei Minuten später kam wieder Löhmannsröben nach einer Ecke an den Ball, sein Schuss ging aber knapp am Tor vorbei und so endete die Partie mit einem 1:2-Auswärtssieg für die Kicker von der Ostsee.

So ausgeglichen wie das Spiel sieht auch die Statistik aus.
Nach Expected Goals liegen die Zebras hier mit 0,90:0,88 vorne.
Daraus ergeben sich für alle drei Möglichkeiten des Spielausganges eine Eintrittswahrscheinlichkeit von etwa 33%.
Auch bei den zu erwartenden Ergebnissen ist es sehr ausgeglichen.
Am wahrscheinlichsten ist ein 1:1-Unentschieden (16,2%), gefolgt von einem 0:1-Auswärtssieg (15,5%), einem 1:0-Heimsieg (15,1%) und einem 0:0-Unentschieden (14,4%).
Auf einen 2:1-Auswärtssieg entfallen 6,8%.