---
title: "Kurzanalyse 1. Spieltag: Hansa Rostock gegen Karlsruher SC"
description: "Kurzanalyse zum 1. Spieltag 2021/22 zwischen Hansa Rostock und Karlsruher SC"
tags: [2. Bundesliga, FC Hansa Rostock, Karlsruher SC, Svante Ingelsson, Lukas Scherff, Marius Gersbeck, Philipp Hofmann, Markus Kolke, Hanno Behrens, Calogero Rizzuto, John Verhoek, Simon Rhein, Streli Mamba, Philip Heise, Marvin Wanitzek, Christoph Kobald, Thomas Meißner, Lukas Cueto, Damian Roßbach, Sebastian Jung, Fabian Schleusener, Ryan Malone, Ridge Munsy]
categories: []
---

Zu Beginn der neuen Zweitligasaison empfing der FC Hansa den Karlsruher SC im heimischen Ostseestadion.
Die Rostocker zeigten eine gute Leistung und bewiesen, dass sie in der Liga mithalten können, verloren aber dennoch mit 1:3.

<canvas id="xgplot012122"></canvas>
<canvas id="winratio012122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['1'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot012122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.KARLSRUHER_SC,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio012122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio012122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Während die Hanseaten als Aufsteiger in ihr erstes Zweitligaspiel seit über neun Jahren gingen, begann für die Badener die dritte Zweitligasaison in Folge.
Der Vorjahressechste ging somit als Favorit in das Spiel.
Von dieser Rollenverteilung war in den ersten Minuten aber nichts zu sehen.
Die Rostocker hatten schon nach fünf Minuten ihre erste gute Torchance.
Nach einem schnellen Ballgewinn im Aufbauspiel der Gäste lief Svante Ingelsson auf dem linken Flügel durch und bediente Lukas Scherff am Fünfmeterraum.
Der Schuss des Rostocker Eigengewächs konnte aber dann von einem Karlsruher Verteidiger geblockt werden.
In der zehnten Minute versuchte es dann der schwedische Neuzugang selbst.
Nachdem eine Flanke von rechts, die von der Gästeverteidigung nur in die Mitte des Strafraums geklärt werden konnte, kam der Mittelfeldspieler aus guter Position zum Schuss.
Mit seinem Versuch schoss Ingelsson jedoch nur den Karlsruher Schlussmann Marius Gersbeck an.
Auf der Gegenseite wurde es dann nur eine Minute später erstmals gefährlich.
Philipp Hofmann kam nach einer flachen Hereingabe von der rechten Seite als Erster an den Ball, konnte im Fallen aber nicht genug Kraft hinter den Ball bringen und so Markus Kolke im Rostocker Tor nicht entscheidend herausfordern.
Eine weitere Minute später kamen die Rostocker zu ihrer nächsten Möglichkeit.
Hanno Behrens fand mit seiner Flanke von links Calogero Rizzuto im Rücken der Abwehr.
Der Italiener ließ sich mit dem Abschluss aber zu lange Zeit und konnte beim Schuss noch von einem Gästespieler gestört werden, so dass der Ball keine Gefahr für Gersbeck darstellte.
In der 16. Minute hallte dann der erste Torjubel durch das Ostseestadion.
John Verhoek köpfte eine Ecke von Simon Rhein unbedrängt in das Tor, der VAR erkannte dabei aber eine Abseitssituation von Streli Mamba und so blieb es zunächst beim 0:0.
Danach wurde es erstmal etwas ruhiger im Spiel ehe die Gäste in der 42. Minute ihrerseits eine Ecke hatten.
Der Eckstoß von Philip Heise landete zunächst bei Marvin Wanitzek, der mit seinem Schuss nur den Pfosten traf.
Den Abpraller verwandelte dann Christoph Kobald aus kurzer Distanz zum 0:1.
Nur zwei Minuten später erhöhten die Badener schon auf 0:2.
Zuvor hatte Thomas Meißner Probleme bei der Ballannahme und verlor dabei das Spielgerät an Lukas Cueto, der ohne Gegenspieler auf Kolke zu lief und schließlich auf Hofmann ablegte, der dann in das leere Tor einschieben konnte.
Mit diesem Zwischenstand ging es für beide Mannschaften wenig später in die Halbzeitpause.

Nach dem Wiederanpfiff wirkten die Rostocker wieder konzentrierten und versuchten wieder mehr Kontrolle über das Spiel zu erlangen.
Mit der ersten Torchance im zweiten Durchgang gelang den Hanseaten dann auch sofort der Anschlusstreffer.
Ein Freistoß von Rhein flog bis zum zweiten Pfosten zu Damian Roßbach, der den Ball zurück in die Mitte zu Verhoek passen konnte.
Der Mittelstürmer schob den Ball von dort zum 1:2 in das Tor.
In der 57. Minute hatte Behrens dann sogar den Ausgleich auf dem Fuß als er bei einer Ecke nur knapp über das Tor schoss.
Zehn Minuten vor dem Schluss machten die Gäste aber den Deckel drauf und erzielten das 1:3.
Mit einem langen Ball von Wanitzek in den Lauf von Sebastian Jung war die Rostocker Verteidigung aus dem Spiel genommen und der Karlsruher Verteidiger hatte nur noch Kolke vor sich, der aber auch nicht mehr entscheidend eingreifen konnte.
Fünf Minuten später konnten die Gäste sogar fast noch nachlegen als Fabian Schleusener am Ende einer Angriffssequenz im Rostocker Strafraum an den Ball kam.
Kolke konnte aber Schlimmeres verhindern.
In der Nachspielzeit schickte der eingewechselte Ryan Malone den ebenfalls eingewechselten Ridge Munsy steil, doch der Stürmer konnte aus dem immer spitzer werdenen Winkel kein Tor mehr erzielen.
So blieb es bis zum Abpfiff bei der 1:3-Niederlage für die Hausherren.

Bei den Expected Goals sieht es ählich aus.
Hier endet das Spiel mit 1,70:2,56, woraus sich für die Gastgeber eine Siegwahrscheinlichkeit von etwas über 19% ergibt.
Die Badener gewinnen gute 59% der Spiele und über 21% der Partien enden Unentschieden.
Die wahrscheinlichsten Ergebnisse, die sich daraus ergeben sind ein 1:2 (10,5%), ein 2:2 (10,4%), ein 1:3 (9,9%) und ein 2:3 (9,7%).