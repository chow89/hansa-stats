---
title: "Kurzanalyse 30. Spieltag: SV Meppen gegen Hansa Rostock"
description: "Kurzanalyse zum 30. Spieltag 2019/20 zwischen SV Meppen und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, SV Meppen, Jens Härtel, John Verhoek, Aaron Opoku, Daniel Hanslik, Mirnes Pepic]
categories: []
---

Drei Spiele hat es gedauert ehe die Rostocker wieder siegreich vom Platz gehen konnten.
Nach einer chancenarmen Partie gewinnt das Team von Trainer Jens Härtel mit 3:0.

<canvas id="xgplot301920"></canvas>
<canvas id="winratio301920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['30'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot301920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SV_MEPPEN,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio301920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Meppener Hausherren kontrollierten das Spiel über die gesamte Spielzeit, während sich die Rostocker vornehmlich auf Konter und lange Bälle hinter die gegnerische Abwerreihe fokussierten.
Und so dauerte es nur sieben Minute bis die Hanseaten nach einem schnellen Einwurf und einer starken Vorlage von John Verhoek Aaron Opoku vor dem Heimtor freispielten.
Der Ball von Opoku kullerte dann mit etwas Glück zur 1:0-Führung ins Tor.
Das nächste Geschenk gab es in der 26. Minute als die Niedersachsen vor dem eigenen Strafraum einen Fehlpass zu Opoku spielten.
Dieser setzte zu einem Solo an und schoss den Ball aus gut 20 Metern zum 2:0 ins Tor.
Mit diesem Spielstand gingen beide Mannschaften später auch in die Halbzeitpause.

Im zweiten Spielabschnitt ging es genauso weiter.
Die erste etwas größere Chance hatte in der 82. Minute der kurz zuvor eingewechselte Daniel Hanslik, die er so gleich zum 3:0 nutzte.
In der Nachspielzeit durfte sich nochmal Mirnes Pepic an einem Kopfball aus guter Position probieren.
Sein Versuch ging aber ein Stück über das Tor und markierte gleichzeitig das Ende der Partie.

Stochastisch betrachtet geht der Sieg in Ordnung, allerdings ist er etwas zu hoch ausgefallen.
Die Rostocker gewinnen mit 41% Wahrscheinlichkeit und spielen mit 54% Wahrscheinlichkeit Unentschieden.
5% Wahrscheinlichkeit entfallen auf einen Meppener Sieg.
Aufgrund der wenigen und qualitativ schlechten Chancen ist ein 0:0 mit 50% Wahrscheinlichkeit das zu erwartende Ergebnis.
Mit immerhin noch 32% Wahrscheinlichkeit gewinnt der FC Hansa mit 1:0.
Die Eintrittswahrscheinlichkeit eines 3:0-Auswärtssieges liegt bei 0,87%.