---
title: "Kurzanalyse 4. Spieltag: SpVgg Unterhaching gegen Hansa Rostock"
description: "Kurzanalyse zum 4. Spieltag 2019/20 zwischen SpVgg Unterhaching und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, SpVgg Unterhaching, Felix Schröter, Marco Königs, Stefan Schimmer, Dominik Stahl, Korbinian Vollmann]
categories: []
---

Am 4. Spieltag ging für den FC Hansa in den tiefen Süden zur Spielvereinigung Unterhaching.
Nach einem über weite Strecken kampf- und lustlosen Auftreten der Rostocker gewannen die Bayern mit 1:0.

<canvas id="xgplot041920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['4'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot041920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SPVGG_UNTERHACHING,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

In der Anfangsphase der ersten Halbzeit sahen die Zuschauer zunächst ein ausgeglichenes Spiel, bei dem sich beide Mannschaften offensiv kaum in Szene setzen konnten.
Im Verlauf fanden die Hachinger immer besser ins Spiel und konnten sich einige kleinere Chancen erarbeiten.
In der 29. Minute gingen die Bayern nach einer gut herausgespielten Möglichkeit durch Felix Schröter mit 1:0 in Führung.
Auf dieser Führung ruhten sich die Gastgeber bis zur Halbzeit aus, während Hansa versuchte wieder besser ins Spiel zu finden.
Einzig eine Aktion von Marco Königs in der 40. Minute kam dabei zustande.
So ging es mit dem 1:0 und 1,24:0,51 xG in die Halbzeit.

Im zweiten Spielabschnitt waren es wieder die Hachinger, die besser ins Spiel fanden und sich Torchancen rausarbeiten konnten.
In der 57. Minute hatte Stefan Schimmer die Vorentscheidung auf den Fuß, konnte sie aber nicht nutzen.
Es folgten noch weiter Chancen von Dominik Stahl und Felix Schröter, die aber genauso erfolglos blieben.
Danach fingen auch die Rostocker wieder an sich gegen die sich abzeichende Niederlage zu wehren.
Außer dem Zufallslattentreffer von Vollmann gelang es den Hanseaten aber nicht mehr entscheidend vor das Tor zu kommen, so dass es am Ende beim 1:0 blieb.
Unterm Strich war es bei 2,18:0,71 xG eine verdiente Niederlage für Hansa, die auch hätte höher ausfallen können.
