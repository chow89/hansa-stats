---
title: "Kurzanalyse 25. Spieltag: Hansa Rostock gegen FC Ingolstadt"
description: "Kurzanalyse zum 25. Spieltag 2019/20 zwischen Hansa Rostock und FC Ingolstadt"
tags: [3. Liga, FC Hansa Rostock, FC Ingolstadt, Stefan Kutschke, Nils Butzen, Lukas Scherff, Robin Krauße, Björn Paulsen, John Verhoek, Aaron Opoku, Markus Kolke, Dennis Eckert Ayensa, Nico Neidhart, Peter Kurzweg, Fabijan Buntic, Pascal Breier]
categories: []
---

Am 25. Spieltag wurde der Tabellenzweite FC Ingolstadt im Ostseestadion begrüßt.
Nach 90 Minuten gingen die Hanseaten mit einem souveränen 3:0-Heimsieg vom Platz.

<canvas id="xgplot251920"></canvas>
<canvas id="winratio251920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['25'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot251920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_INGOLSTADT,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio251920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften taten sich mit Beginn des Spiels zunächst schwer mit dem starken und böigen Wind.
So war die Anfangsviertelstunde geprägt von vielen Fehlern im Spielaufbau.
So ergab sich auch erst in der 14. Minute die erste nennenswerte Offensivaktion.
Die Gäste konnten nach schnellem Umschaltspiel die Angriffssequenz mit einem Kopfball von Stefan Kutschke abschließen.
Der Ball war aber keine Gefahr für das Rostocker Gehäuse und ging weit am Tor vorbei.
Sieben Minuten später kamen auch die Gastgeber erstmals vor das Tor und konnten sogleich das Führungstor erzielen.
Nils Butzen verwandelte dabei eine flache Hereingabe von Lukas Scherff zum 1:0.
Nach dem Tor passierte erstmal lange nichts ehe die Ingolstädter in der 31. Minute zu ihrer nächsten Möglichkeit kamen.
Eine Ecke des Ex-Rostockers Robin Krauße konnte Björn Paulsen nur knapp neben den Pfosten setzen.
Wenig später konnten die Hanseaten ihrerseits wieder offensive Akzente setzen und mit der zweiten guten Torchance gelang auch das zweite Tor.
John Verhoek konnte die Vorarbeit von Butzen und Aaron Opoku zum 2:0 einnetzen.
Mit dem Pausenpfiff hatten die Ingolstädter erneut nach einer Krauße-Ecke die Möglichkeit auf ein Tor, doch erst hielt Markus Kolke einen Paulsen-Kopfball, dann blockte Butzen einen Schuss von Dennis Eckert Ayensa.

Nach der Pause hatte Hansas Nico Neidhart die erste Chance der Halbzeit.
Sein Schuss wurde aber von Peter Kurzweg zur Ecke geblockt.
In der 56. Minute konnten auch die Bayern wieder vor das Tor kommen.
Eckert Ayensas Schuss aus kurzer Distanz konnte aber von Kolke abgewehrt und anschließend aus der Gefahrenzone geschlagen werden.
Anschließend verteidigten die Hanseaten etwas tiefer und ließen die Gäste zu keiner nenneswerten Chance mehr kommen.
Die Rostocker konnten ihrerseites in der 73. Minute wieder vor das Tor kommen.
Der FCI-Torwart Fabijan Buntic konnte den Schuss vom freistehenden Neidhart aber entschärfen.
Fünf Minuten vor Schluss hatten die Hanseaten dann ihre letzte Chance.
Neidhart fing einen schlechten Pass im Ingolstadt Aufbauspiel ab und legte auf den sich frei laufenden Pascal Breier ab, der seinerseits ohne Probleme zum 3:0 einnetzen konnte.
Danach war das Spiel entschieden und beide Mannschaften trennten sich mit diesem Resultat.

Auch nach expected Goals hat der FC Hansa gewonnen, wenn auch nicht so deutlich mit 1,65:1,01.
Daraus ergibt sich eine Siegwahrscheinlichkeit von circa 54% und eine Unentscheidenwahrscheinlichkeit von etwa 26%.
Mit immerhin fast 20% Wahrscheinlichkeit gewinnen die Ingolstädter.
Nach Ergebnissen waren ein 1:1-Unentschieden und 2:1-Heimsieg mit 14% am wahrscheinlichsten.
Es folgen ein 1:0-Heimsieg mit 11% und 2:0-Heimsieg mit 10%.
Einen 3:0-Heimsieg gibt es nur in 5% aller Spiele.