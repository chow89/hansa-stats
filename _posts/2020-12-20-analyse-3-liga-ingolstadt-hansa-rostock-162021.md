---
title: "Kurzanalyse 16. Spieltag: FC Ingolstadt gegen Hansa Rostock"
description: "Kurzanalyse zum 16. Spieltag 2020/21 zwischen FC Ingolstadt und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, FC Ingolstadt, Caniggia Elva, Dennis Eckert Ayensa, Markus Kolke, Merlin Röhl, Thomas Keller, Nico Neidhart, John Verhoek, Nik Omladic, Bentley Baxter Bahn, Korbinian Vollmann, Björn Rother]
categories: []
---

Am 16. Spieltag reisten die Hanseaten zum Aufstiegsaspiranten FC Ingolstadt.
Nach einer umkämpften Partie gewannen die Gastgeber knapp, aber verdient mit 1:0.

<canvas id="xgplot162021"></canvas>
<canvas id="winratio162021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['16'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot162021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_INGOLSTADT,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio162021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio162021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Während die Rostocker von Beginn an versuchten die Defensive stabil zu halten, legten die Bayern gleich offensiv los.
In der dritten Minute hatte Caniggia Elva gleich die erste gute Möglichkeit.
Bei einer Ecke landete der Ball zwei Meter vor dem Tor beim Kanadier, der dann aber das Tor knapp verfehlte.
Nach einer guten Viertelstunde kam Dennis Eckert Ayensa nach einer Flanke aus ähnlicher Position zum Kopfball.
Dieses Mal landete der Ball in den Händen von Markus Kolke im Rostocker Tor.
Nach einer längeren Phase ohne große Torchancen kamen die Gastgeber in der 37. Minute erneut vor das Gästetor.
Der Ingolstädter Youngster Merlin Röhl konnte dabei eine Flanke von der rechten Seite ganz abgeklärt zum 1:0 verwandeln.
Auch nach dem Rückstand schafften es die Hanseaten nicht offensiver zu agieren.
Stattdessen hatten die Hausherren vier Minuten nach dem Tor die nächste Möglichkeit.
Nach einem weiten Abstoß landete der Ball schnell bei Eckert Ayensa, der sofort abschloss.
Kolke konnte den Schuss mit einem Reflex zur Ecke klären.
Eine Minute später setzte sich Thomas Keller im Rostocker Strafraum gegen zwei Verteidiger durch, scheiterte aber auch an Kolke, der in höchster Not parieren konnte.
Anschließend begaben sich beide Mannschaften wieder in die Kabinen.

Nach dem Wiederanpfiff traten die Rostocker etwas offensiver auf, bis zur ersten guten Torchance sollte es aber noch etwas dauern.
Erst in der 57. Minute hatten die Gäste ihre erste Chance durch Nico Neidhart, doch sein Schuss ging weit am Tor vorbei.
In der 72. Minuten hatten die Ingolstädter ihrerseits die erste, aber auch letzte Chance der Halbzeit.
Wieder war es Eckert Ayensa, der für Torgefahr sorgte, doch auch sein Schuss nach einem Freistoß ging am Gehäuse vorbei.
Anschließend begann der Rostocker Sturmlauf um doch noch einen Punkt mit nach Hause zu nehmen.
In der 83. Minute war es zunächst John Verhoek, der nach einer Flanke von Nik Omladic artistisch versuchte auf das Tor zu schießen, der Ball aber dennoch in den Armen des Torhüters landete.
Zwei Minuten später spielten die Rostocker Bentley Baxter Bahn im Strafraum frei, sein Schuss ging aber ein gutes Stück neben das Tor.
In der fünften Minute der Nachspielzeit hatten dann nochmal Korbinian Vollmann und Björn Rother die Ausgleichsmöglichkeit.
Während der Schuss von Vollmann noch an die Latte klatschte, konnte der Schuss von Rother von der Verteidigung ohne weiteres geklärt werden.
Anschließend beendete der Schiedsrichter das Spiel.

In der Expected Goals-Statistik gewinnen die Bayern das Spiel ebenfalls mit 1,83:1,33.
Daraus resultiert eine Siegwahrscheinlichkeit von circa 25% für die Rostocker und etwa 50% für die Ingolstädter.
Die restlichen 25% der Spiele enden Unentschieden.
Als wahrscheinlichste Ergebnisse für dieses Spiel ergeben sich entsprechend ein 2:1-Heimsieg (12,5%) und ein 1:1-Unentschieden (11,7%).
Mit 1:0 endet das Spiel mit 6,9% Wahrscheinlichkeit.