---
title: "Kurzanalyse 24. Spieltag: 1860 München gegen Hansa Rostock"
description: "Kurzanalyse zum 24. Spieltag 2020/21 zwischen 1860 München und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, 1860 München, Dennis Dressel, Markus Kolke, Stephan Salger, Sascha Mölders, Stefan Lex, John Verhoek, Lorenz Knöferl]
categories: []
---

Am 24. Spieltag mussten die Hanseaten beim Tabellennachbarn 1860 München antreten.
Die umkämpfte, aber chancenarme Partie endete mit einem 0:0-Unentschieden.

<canvas id="xgplot242021"></canvas>
<canvas id="winratio242021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['24'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot242021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.TSV_1860_MÜNCHEN,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio242021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio242021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Der erste Spielabschnitt begann mit zwei Teams auf Augenhöhe, die sich vorrangig auf die Defensivarbeit konzentrierten und sich so gegenseitig neutralisierten.
Dennoch hatten die Münchener in der dritten Minute die erste gute Torchance.
Dennis Dressel kam nach einem Eckstoß als Erster an den Ball, konnte das Spielgerät aber dann nicht auf das Tor bringen.
Zwanzig Minuten später tauchten die Gastgeber erneut vor dem Gehäuse von Markus Kolke auf.
Bei diesem Angriff war es Stephan Salger, der bei einer Flanke einen Fuß an den Ball bekam, aber dann nur die Latte traf.
In der 31. Minute hatte dann auch der Sechziger-Kapitan Sascha Mölders seine erste Möglichkeit.
Sein Schuss aus über zwanzig Metern ging jedoch weit am Tor vorbei.
Nur sieben Minuten später bekam Stefan Lex die Möglichkeit zum Führungstreffer, doch sein Kopfball aus kurzer Distanz ging am Rostocker Gehäuse vorbei.

Nach der Pause und dem Platzverweis gegen John Verhoek in der 48. Minute entwickelte sich ein Spiel auf ein Tor mit nur wenigen Entlastungsangriffen der Rostocker.
In der 54. Minute kam erneut Mölders zur Torchance.
Sein Schuss aus kurzer Distanz, aber sehr spitzen Winkel flog ein gutes Stück über die Latte.
Acht Minute vor dem Ende der regulären Spielzeit hatte Mölders dann die vermeintlich beste Torchance des Spiels.
Sein gut platzierter Schuss aus etwas zehn Metern konnte aber von Kolke noch geradeso mit einer Parade geklärt werden.
In der Nachspielzeit folgte dann noch der Torschuss vom Sechziger-Eigengewächs Lorenz Knöferl.
Dieser konnte aber von der Rostocker geblockt werden und so blieb es bis zum Abpfiff wenige Minuten später beim torlosen Unentschieden.

Bei den Expected Goals zeichnet sich jedoch ein ganz anderes Bild ab.
Hier gewinnen die Münchener deutlich mit 1,91:0,07.
Daraus ergibt sich für die Rostocker nur eine Siegwahrscheinlichkeit von 0,8%, während die Löwen in fast 87% der Spiele gewinnen.
12% der Spiele enden Unentschieden.
Entsprechend der Expected Goals ist ein 2:0-Heimsieg das wahrscheinlichste Ergebnis mit 29%, knapp gefolgt vom 1:0-Heimsieg mit 26% Wahrschienlichkeit.
Mit 0:0 enden nur 10% der Spiele.