---
title: "Kurzanalyse 31. Spieltag: Hansa Rostock gegen FC Magdeburg"
description: "Kurzanalyse zum 31. Spieltag 2020/21 zwischen Hansa Rostock und FC Magdeburg"
tags: [3. Liga, FC Hansa Rostock, FC Magdeburg, Björn Rother, Nico Neidhart, Bentley Baxter Bahn, Philip Türpitz, Morten Behrens, Andreas Müller, Baris Atik, John Verhoek, Pascal Breier, Sirlord Conteh, Markus Kolke, Damian Roßbach, Lion Lauberbach, Sören Bertram]
categories: []
---

Zum 31. Spieltag begrüßte der FC Hansa den 1. FC Magdeburg an der Ostseeküste.
Das umkämpfte Ostduell gewann die Gäste am Ende mit 0:2.

<canvas id="xgplot312021"></canvas>
<canvas id="winratio312021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['31'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot312021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_MAGDEBURG,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio312021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio312021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Obwohl die Magdeburger den besseren Start in die Partie hatten, hatte Björn Rother für die Hausherren in der zweiten Minute die erste gute Möglichkeit.
Nach einem langen Einwurf von Nico Neidhart stieg der Defensivspezialist am höchsten und köpfte den Ball nur knapp über die Latte.
Sieben Minuten später spielte Bentley Baxter Bahn einen langen Ball in den Lauf von Philip Türpitz, der dann von der Strafraumkante abzog aber in Morten Behrens seinen Meister fand.
In der 25. Minute kamen dann auch die Gäste zu ihrer ersten Torchance und nutzten diese gleich zur 0:1-Führung.
Nach einem Fehlpass von Rother im Mittelfeld schickte Andreas Müller Baris Atik steil, der den Ball dann einnetzte.
Die Rostocker fanden nach dem Gegentreffer etwas besser in das Spiel, machten aber weiterhin zu wenig um das Spiel zu bestimmen.
In der 31. Minute hatte John Verhoek dann schon den Ausgleich auf dem Fuß, doch auch er scheiterte aus kurzer Distanz an Behrens.
Kurz vor dem Halbzeitpfiff hatten die Hanseaten dann die nächsten Möglichkeiten.
Die Dreifachchance von Pascal Breier und Bahn ging aber nur an den Pfosten und landete schließlich im Toraus.

Nach dem Seitenwechsel setzten die Gäste in der 58. Minuten den ersten Akzent.
Wieder war es ein Steilpass in den Rücken der Abwehr, der in Sirlord Conteh einen Abnehmer fand.
Markus Kolke im Rostocker Tor war aber zur Stelle und bereinigte die Situation.
Die Hanseaten blieben im gesamten zweiten Spielabschnitt bis auf einige Halbchance blass und hatten erst in der 84. Minute eine erfolgsversprechende Torchance.
Ein langer Ball von Damian Roßbach hoppelte irgendwie zu Lion Lauberbach, der bei seinem Abschluss von der Strafraumkante aber nicht genügend Kraft hinter das Spielgerät brachte.
Auch bei diesem Versuch landete der Ball in den Händen von Behrens.
Den Schlusspunkt der Partie setzte Sören Bertram in der letzten Minute als er einen Elfmeter sicher zum 0:2 verwandelte.

Entgegen des eindeutigen Ergebnisses gewinnen die Hanseaten nach Expected Goals klar mit 2,51:1,07.
In Wahrscheinlichkeiten ausgedrückt bedeutet das eine fast 73%ige Siegwahrscheinlichkeit für die Rostocker, bei fast 18% Unentschieden- und 9% Niederlagenwahrscheinlichkeit.
Ein 2:1-Heimsieg ist dabei mit 14,3% Wahrscheinlichkeit das häufigste Endergebnis.
Dahinter folgen ein 3:1-Heimsieg (12,5%) und ein 1:1-Unentschieden (8,6%).
Einen Magdeburger 0:2-Sieg gibt es nur in 0,9% aller Spiele.