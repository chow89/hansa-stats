---
title: "Kurzanalyse 21. Spieltag: Hansa Rostock gegen Hallescher FC"
description: "Kurzanalyse zum 21. Spieltag 2019/20 zwischen Hansa Rostock und Halleschem FC"
tags: [3. Liga, FC Hansa Rostock, Hallescher FC, Korbinian Vollmann, Aaron Opoku, Niklas Kastenhöfer, Patrick Göbel, Julian Guttau, Terrence Boyd, Markus Kolke, Pascal Breier, Pascal Sohm]
categories: []
---

Im ersten Spieltag nach der Winterpause begrüßten die Hanseaten den Halleschen FC im heimischen Ostseestadion.
Nach einer umkämpften Partie unter Flutlicht gewannen die Hausherren mit 1:0.

<canvas id="xgplot211920"></canvas>
<canvas id="winratio211920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['21'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot211920", {
        team1: {
            name: "Hansa",
            points: team1Shots
        },
        team2: {
            name: "Hallescher FC",
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio211920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Das Spiel begann zunächst verhalten.
Beide Mannschaften versuchten durch lange Ballbesitzphasen an Sicherheit zu gewinnen und traten somit nur selten offensiv in Erscheinung.
Die erste Chance hatten dann die Rostocker in der 10. Minute durch einen Konter, den Korbinian Vollmann mit einem Schuss aus vollem Lauf weit neben das Tor schoss.
Fünf Minuten später konterten die Hanseaten die Hallenser erneut aus.
Diesmal war es Aaron Opoku, der den Angriff beendete, aber auch sein Schuss verfehlte das Gästetor knapp.
Auf der Gegenseite dauerte es bis zur 21. Minute ehe die Saalestädter zur ersten nennenswerten Tormöglichkeit kamen.
Niklas Kastenhöfer konnte seinen Kopfball nach einem Freistoß von Patrick Göbel jedoch nur über das Tor setzen.
Die Gäste übernahmen in der Folge die Spielkontrolle und hatten in der 32. Minute durch eine Doppelchance die nächsten Chancen auf die Führung.
Erst war es Julian Guttau mit einem Schuss von der Strafraumgrenze, danach Terrence Boyd, der den abgeprallten Schuss aus spitzen Winkel in das Tor befördern wollte.
Beide Mal war es Markus Kolke, der die Schüsse parierte.
In der 39. Minute war es erneut Boyd, der Kolke mit einem Schuss von der Strafraumgrenze prüfte.
Auch diesen Schuss konnte Kolke klären.
In den letzten Minute der Halbzeit passierte dann nichts mehr und so ging es für beiden Mannschaften mit 0:0 in die Kabine.

Der zweite Spielabschnitt begann ebenfalls mit gegenseitigen Abtasten und wenigen Offensivaktionen.
Erst nach fast einer Viertelstunde kam wieder Schwung in die Partie.
Zunächst war es Guttau, der erneut Kolke prüfte, und im direkten Gegenzug Vollmann, der den Ball weit über das Tor schoss.
Wenig später machte es Vollmann dann besser.
Opoku spielte einen Steilpass auf Pascal Breier, der den Ball auf den im Strafraumzentrum freistehenden Vollmann passte.
Dieser umkurvte zwei Gegenspieler und schoss den Ball dann zum 1:0 in die Maschen.
Nach dem Tor zogen sich die Hanseaten zurück, pressten erst ab der Mittellinie und standen in der Defensive insgesamt tiefer in der eigenen Hälfte.
Die Hallenser hatten damit Probleme und haben es erst in der 78. Minute durch eine Ecke wieder vor das Tor geschafft.
Pascal Sohm köpfte knapp neben das Tor.
Eine letzte Möglichkeit ergab sich in der 84. Minute ebenfalls nach einer Ecke.
Dieses Mal war es Boyd, der unter Bedrängnis zum Kopfball ansetzte, aber keine Gefahr erzeugen konnte.
So blieb es bis zum Abpfiff bei der 1:0-Führung für die Hanseaten.

Nach Expected Goals haben die Gäste das Spiel knapp mit 1,03:1,44 gewonnen, die Gastgeber hatten aber mehr Torchancen.
Daraus ergibt sich für die Rostocker eine Siegwahrscheinlichkeit von 25,57% und für die Hallenser von 47,03%. In 27,4% der Fällen endet das Spiel unentschieden.
Das wahrscheinlichste Ergebnis bei diesem Spielverlauf ist ein 1:1 mit 14,38% Wahrscheinlichkeit, gefolgt vom 0:1 (11,8%) und 1:2 (11,04%).
Mit 8,69% Wahrscheinlichkeit endet das Spiel 1:0.