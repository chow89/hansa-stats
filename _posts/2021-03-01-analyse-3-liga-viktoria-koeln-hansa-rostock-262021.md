---
title: "Kurzanalyse 26. Spieltag: Viktoria Köln gegen Hansa Rostock"
description: "Kurzanalyse zum 26. Spieltag 2020/21 zwischen Viktoria Köln und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Viktoria Köln, Julian Riedel, Markus Kolke, Marcel Risse, Nico Neidhart, Björn Rother, Philip Türpitz, Aaron Herzog, Pascal Breier, Sebastian Mielitz, Michael Schultz]
categories: []
---

Der FC Hansa Rostock reiste am 26. Spieltag zum FC Viktoria Köln.
Die ausgeglichene Partie entschieden die Rostocker schlussendlich mit 1:2 für sich.

<canvas id="xgplot262021"></canvas>
<canvas id="winratio262021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['26'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot262021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.VIKTORIA_KÖLN,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio262021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio262021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften begegneten sich von Beginn an auf Augenhöhe.
Bereits in der sechsten Minute nutzten die Hausherren einen Stockfehler der Rostocker Verteidigung und gingen mit 1:0 in Führung.
Julian Riedel wollte einen Rückpass auf den herauseilenden Markus Kolke spielen, legte den Ball dabei aber perfekt für den ehemaligen Bundesligaspieler Marcel Risse auf, der dann ohne Probleme in das leere Tor einschieben konnte.
Die Hanseaten zeigten sich von dem frühen Rückstand aber nicht geschockt und nutzten ihrerseits die erstbeste Möglichkeit zum Ausgleich.
Nico Neidhart brachte eine Flanke von der rechten Seite halbhoch rein, die Björn Rother dann ungehindert im gegnerischen Tor versenken konnte.
Nach der aufsehenerregenden Anfangsphase beruhigte sich das Spiel etwas.
Erst in der 40. Minute hatten die Hanseaten wieder eine Torchance.
Philip Türpitz' Volleyschuss ging jedoch weit am Tor vorbei.
Wenig später bat der Schiedsrichter beide Mannschaften zur Halbzeitpause.

Nach dem Seitenwechsel ging es unverändert weiter.
Die Kölner hatten die größeren Spielanteile, schafften es aber nicht sich durch die Rostocker Abwehr zu spielen.
Die Hanseaten ihrerseite fokussierten sich ihrerseits auf das Kontern.
Nennenswerte Strafraumszene ergaben sich dabei zunächst nicht.
Es dauerte bis zur 72. Minute ehe die Hansa-Kicker wieder vor das Tor kamen.
Aaron Herzog eroberte nach gutem Pressing den Ball im Angriffsdrittel und legte auf den freistehenden Pascal Breier im Strafraum ab, der dann zum 1:2 verwandelte.
Nur zwei Minuten danach hatte Breier sogar die Chance zum Erhöhen, dieses Mal hatte Sebastian Mielitz im Kölner Tor aber das bessere Ende für sich.
Weitere vier Minuten später hatte Herzog aus ähnlicher Position die nächste Möglichkeit, doch auch hier blieb Mielitz Sieger.
Wenige Sekunden vor dem Abpfiff hatten die Kölner dann noch die Chance zum Ausgleich.
Der Schuss von Michael Schultz flog aber weit über das Tor und so blieb es beim 1:2-Sieg der Hanseaten.

Auch bei den Expected Goals liegen die Rostocker knapp vorne und gewinnen hier mit 1,23:1,02.
Daraus ergibt sich eine Siegwahrscheinlichkeit von fast 39% und eine Unentschiedenwahrscheinlichkeit von fast 32%.
Die Kölner gewinnen in knapp über 29% der Spiele.
Die wahrscheinlichsten Ergebnisse bei diesem Spielverlauf sind ein 1:1-Unentschieden (19,9%), ein 1:0-Heimsieg (13,0%) und ein 2:1-Auswärtssieg (12,7%).