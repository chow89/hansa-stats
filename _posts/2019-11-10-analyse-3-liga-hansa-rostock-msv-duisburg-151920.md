---
title: "Kurzanalyse 15. Spieltag: Hansa Rostock gegen MSV Duisburg"
description: "Kurzanalyse zum 15. Spieltag 2019/20 zwischen Hansa Rostock und MSV Duisburg"
tags: [3. Liga, FC Hansa Rostock, MSV Duisburg, Nik Omladic, Pascal Breier, Leo Weinkauf, Aaron Opoku, Max Reinthaler, Mirnes Pepic, Nikolas Nartey, Lukas Scepanik, Maximilian Ahlschwede, Vincent Vermeij]
categories: []
---

Nach der [Niederlage gegen den Tabellenletzten aus Jena]({% post_url 2019-11-05-analyse-3-liga-carl-zeiss-jena-hansa-rostock-141920 %}) gibt es nur eine Woche später gegen den Tabellenführer ebenfalls nichts zu holen.
Die Rostocker verloren nach einer beeindruckenden Partie mit 1:2 gegen das Team aus Duisburg.

<canvas id="xgplot151920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['15'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot151920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.MSV_DUISBURG,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Das Spiel begann sehr verhalten mit nur wenigen Chancen auf beiden Seiten, die aber für keine allzu große Gefahr sorgten.
Erst nach der Anfangsviertelstunde spielten sich die Hanseaten in einen regelrechten Rausch.
Zunächst hatten Nik Omladic und Pascal Breier in der 19. Minute die Chance zur Führung, scheiterten aber beide freistehend vor dem herausragend aufgelegten Leo Weinkauf im Duisburger Tor.
Zehn Minuten später waren es dann Aaron Opoku, Max Reinthaler und Mirnes Pepic, die ebenfalls am Duisburger Schlussmann scheiterten oder den Ball nicht mehr entscheidend auf das Tor brachten.
Kurz vor der Pause hatte dann noch Nikolas Nartey die Chance, der aus kurzer Distanz aber am Fuß eines Duisburger Verteidigers hängen blieb.
Somit ging es ohne Tore und einem eindeutigen xG-Verhältnis von 2,3:0,15 in die Halbzeit.

Nach der Pause spielten die Gäste dann besser mit und konnten sich auch gleich mit der ersten Chance in der 52. Minute mit 1:0 in Führung bringen.
Lukas Scepanik netzte mit einem sehenswerten Seitfallzieher ein.
Die Rostocker konnte das aber wenig beeindrucken und so konnten die Hanseaten nur drei Minuten später das 1:1 bejubeln.
Nartey machte es dieses Mal besser und beförderte eine Ahlschwede-Hereingabe in das Gäste-Gehäuse.
Danch verflachte das Spiel zunächst, wodurch es auch fast keine Torraumszenen mehr gab.
Erst in der 82. Minute ereignete sich wieder eine Torchance, die von den Gästen auch sogleich zur 2:1-Führung genutzt wurde.
Vincent Vermeij köpfte dabei den Ball an den Pfosten, der von da aus zum anderen Pfosten hoppelte und dann in das Tor sprang.
Die Rostocker versuchten anschließend noch zumindest das Unentschieden zu erzielen, konnten aber nicht mehr zwingend vor das Tor der Duisburger kommen.
Damit blieb es bei der 1:2-Niederlage und einem xG-Verhältnis von 2,97:0,53.

Ebenso einseitg wie das Spiel sind auch die daraus resultierenden Wahrscheinlichkeiten.
Die Rostocker gewinnen bei so einem Spielverlauf in 90,41% der Fällen, während die Duisburger nur 2,18% der Spiele gewinnen.
Die restlichen 7,41% der Spiele enden mit einer Punkteteilung.
Bei den wahrscheinlichsten Ergebnissen sind dementsprechend auch nur Heimsiege zu verzeichnen.
3:0 (16,07%), 2:0 (13,83%) und 4:0 (11,45%) sind die häufigsten Endergebnisse.
Eine 1:2-Niederlage tritt nur mit 0,9%iger Wahrscheinlichkeit ein.