---
title: "Trainervergleich: Dotchev gegen Härtel"
description: "Dotchev oder Härtel - wer ist der Bessere?"
tags: [Pavel Dotchev, Jens Härtel]
categories: []
---

Wie der Zufall es will, treffen am nächsten Wochenende die beiden Hansa-Trainer der vergangenen Saison aufeinander.
Während Pavel Dotchev in der Hinrunde am Ruder war und inzwischen Viktoria Köln trainiert, übernahm Jens Härtel zur Rückrunde das Traineramt.
Doch wer von den beiden war in der letzten Saison eigentlich der Bessere?
Oder anders gefragt: Hat sich der Trainerwechsel für Hansa gelohnt?

| Dotchev                     |                | Härtel                    |
|-----------------------------|----------------|---------------------------|
| 7-6-7 (1,35 Punkte/Spiel)   | **Bilanz**     | 7-7-4 (1,55 Punkte/Spiel) |
| 27:30 (1,35:1,5)            | **Tore**       | 20:16 (1,11:0,88)         |
| 1,42:1,16                   | **xG**         | 1,35:0,88                 |
| 48,2%                       | **Ballbesitz** | 54,5%                     |

Die obige Tabelle zeigt einige Statistiken, an denen sich die Manschaftsleistungen aus der Vor- und Rückrunde gut analysieren lassen.
Zuerst sei erwähnt, dass beide Trainer die Mannschaft in ungefähr der gleichen Anzahl an Spielen betreut haben (Dotchev 20 Spiele, Härtel 18 Spiele).
Dadurch lassen sich die Leistungen sehr gut miteinander vergleichen und bewerten.

Sowohl Dotchev als auch Härtel haben den Platz siebenmal als Sieger verlassen. Zudem konnten sechs (Dotchev) beziehungsweise sieben (Härtel) Spiele mit einem Unentschieden abgeschlossen werden.
Einzig bei der Anzahl der Niederlagen gibt es einen bedeutenden Unterschied.
Während in der Hinrunde noch sieben Spiele verloren wurden, waren es danach nur noch vier.
Davon waren zwei Niederlagen in den ersten beiden Spielen nach dem Trainerwechsel.
Dementsprechend hat das Team in den restlichen 16 Spielen nur noch zweimal verloren.
Daraus ergibt sich dann auch das etwas bessere Punkte/Spiel-Verhältnis.
0,2 Punkte/Spiel scheinen im ersten Moment nur eine Kleinigkeit zu sein, sind aber auf eine komplette Drittliga-Saison gerechnet immerhin 7,6 Punkte.

Etwas deutlichere Unterschiede gibt es bei den Toren und Gegentoren.
Unter Pavel Dotchev war das Torverhältnis mit 27:30 (1,35:1,5 pro Spiel) noch leicht negativ.
In den Spielen unter Jens Härtel konnte dann jedoch ein positives Torverhältnis mit 20:16 (1,11:0,88 pro Spiel) erzielt werden.
In der Hinrunde wurden mehr Tore geschossen, aber auch mehr Gegentore kassiert.
In der Rückrunde konnte dann die Defensive stabilisiert und die Gegentore fast halbiert werden.
Das ging jedoch auf Kosten der Offensive, die sich weniger auszeichnen konnte als noch in der ersten Saisonhälfte.

Unterstützt wird diese Beobachtung auch durch die xG/xGA- und die Ballbesitzstatistik.
In der Hinrunde war das erwartete Torverhältnis pro Spiel entgegen den tatsächlichen Werten sogar positiv.
In der Offensive konnte die Erwartung von 1,42 Toren fast erfüllt werden.
Die Defensive war aber bei 1,16 erwarteten Gegentoren und 1,5 tatsächlichen Gegentoren ein großes Problem.
Bei knapp unter 50% Ballbesitz deutet das auf Probleme in der Spielkontrolle oder bei der Spieleröffnung hin.\\
Nach dem Trainerwechsel wurde dann die Offensive zum Problem.
Erwarteten 1,35 Toren standen nur 1,11 Toren pro Spiel gegenüber.
Dafür konnte die Defensive exakt die Erwartung erfüllen.
Durch die defensivere Grundausrichtung konnte der Ballbesitz so auf gute 54,5% gesteigert werden.

<style>
th:first-child, td:first-child {text-align: right;}
th:nth-child(2), td:nth-child(2) {text-align: center;}
</style>
