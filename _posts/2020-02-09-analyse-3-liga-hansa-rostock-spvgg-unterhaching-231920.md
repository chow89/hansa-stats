---
title: "Kurzanalyse 23. Spieltag: Hansa Rostock gegen SpVgg Unterhaching"
description: "Kurzanalyse zum 23. Spieltag 2019/20 zwischen Hansa Rostock und SpVgg Unterhaching"
tags: [3. Liga, FC Hansa Rostock, SpVgg Unterhaching, Pascal Breier, Aaron Opoku, Alexander Winkler, Sven Sonnenberg, Lukas Scherff, Daniel Hanslik, Nico Neidhart, Sascha Bigalke, Jim-Patrick Müller, Dominik Stroh-Engel]
categories: []
---

Am 23. Spieltag begrüßten die Hanseaten den Tabellendritten aus Unterhaching.
Nach einem chancenarmen und umkämpften Spiel trennten sich beide Mannschaften mit 1:1.

<canvas id="xgplot231920"></canvas>
<canvas id="winratio231920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['23'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot231920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SPVGG_UNTERHACHING,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio231920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Partie begann mit zwei abwarteten Teams.
Die Rostocker versuchten erneut mit langen Passsequenzen in der Defensive die Spielkontrolle zu gewinnen, während die Gäste versuchten mit Kontern Nadelstiche zu setzen.
Dementsprechend kam es auch nur zu wenigen Torchancen.
Die Zuschauer mussten so bis zur 26. Minute auf die erste gefährliche Torchance warten.
Pascal Breier wurde von Aaron Opoku am Fünf-Meter-Raum angespielt.
Der anschließende Torschuss konnte von Alexander Winkler schon frühzeitig geblockt werden.
Danach wurde es wieder etwas ruhiger und erst in der 39. Minute wurde es wieder gefährlich vor dem Hachinger Tor.
Wieder waren es Breier und Opoku - dieses Mal mit vertauschten Rollen - die im Strafraum für Aufregung sorgten.
Der Schuss von Opoku verfehlte das Gästegehäuse jedoch knapp.
Es blieb dann auch bis zur Halbzeitpause beim 0:0.

Im zweiten Spielabschnitt ging das Spiel in ähnlicher Weise weiter, nur dass die Bayern sich offensiv etwas mehr trauten.
Mehr Torchancen ergaben sich daraus aber nicht.
Erst in der 59. Minute konnten die Rostocker wieder vor das Tor kommen.
Nach einer Opoku-Ecke köpfte Sven Sonnenberg den Ball ein gutes Stück neben das Tor.
Nur eine Minute später gab es dann Grund zum Jubeln.
Ein sehr gut herausgespielte Torchance über Opoku, Lukas Scherff und Daniel Hanslik konnte Nico Neidhart aus kurzer Distanz zum 1:0 verwerten.
Mit dem Tor stellten die Hanseaten ihre Offensivaktionen weitestgehend ein und überließen den Gästen das Feld.
Das rächte sich in der 79. Minute als die Rand-Münchener ein Spielzug über Sascha Bigalke, Jim-Patrick Müller und Dominik Stroh-Engel zum 1:1 nutzten.
In den letzten zehn Minuten passierte dann nicht mehr viel und es blieb bis zum Schluss beim 1:1.

In der stochastischen Betrachtung gewinnt Hansa aufgrund der Mehrzahl an Torchancen das Spiel mit 1,23:0,93 xG.
Daraus resultiert in knapp über 40% der Fälle ein Heimsieg, während die Gäste in ungefähr 24% aller Spiele gewinnen.
Über 35% der Spiele enden mit einer Punkteteilung.
Das 1:1 ist bei so einem Spielverlauf mit fast 27% Wahrscheinlichkeit auch das zu erwartende Ergebnis.
Es folgen ein 2:1-Heimsieg mit rund 17% und ein 0:1-Auswärtssieg mit fast 16% Wahrscheinlichkeit.
