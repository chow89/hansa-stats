---
title: "Kurzanalyse 1. Runde DFB-Pokal: Hansa Rostock gegen VfB Stuttgart"
description: "Kurzanalyse zur 1. DFB-Pokalrunde 2020/21 zwischen Hansa Rostock und VfB Stuttgart"
tags: [FC Hansa Rostock, VfB Stuttgart, DFB-Pokal, Gonzalo Castro, Daniel Didavi, Markus Kolke, Manuel Farrona Pulido, Silas Wamangituka, Damian Roßbach, Wataru Endo, John Verhoek, Darko Churlinov, Erik Engelhardt]
categories: []
---

Nach der kurzen Sommerpause begann die neue Saison in diesem Jahr mit der 1. Runde des DFB-Pokals und zum dritten Mal in Folge gastierte der VfB Stuttgart im Ostseestadion.
Nach 90 Minuten endete das Spiel genauso wie im letzten Jahr mit einem 0:1-Sieg für den Bundesligisten.

<canvas id="xgplotcup012021"></canvas>
<canvas id="winratiocup012021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].cup['1'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplotcup012021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: "VfB Stuttgart",
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratiocup012021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Gäste aus Süddeutschland ließen von Anfang an keinen Zweifel daran, wer in dem Spiel der Favorit ist und kontrollierten die Begegnung von der ersten Minute an.
Die Rostocker fokussierten sich ihrerseits auf die Defensive und versuchten die Schwaben mit Kontern zu überraschen.
Entsprechend hatten die Stuttgarter nach einer langen Abtastphase in der 19. Minute auch die erste Chance der Partie.
Ex-Nationalspieler Gonzalo Castro konnte einen verunglückten Schuss von Daniel Didavi nochmal scharf machen und Markus Kolke im Hansa-Tor aus kurzer Distanz zu einem Reflex zwingen.
Nach etwas als einer halben Stunde konnten auch die Rostocker ihre erste große Chance verbuchen.
Manuel Farrona Pulido kam nach einem Freistoß und einem schwachen Klärungsversuch der Gäste aus etwa 15 Metern frei zum Schuss.
Der Versuch ging aber deutlich am Tor vorbei.
Nur eine Minute später hatte Sasa Kalajdzic auf der Gegenseite die nächste Chance.
Doch auch er scheiterte an Kolkes Reflexen.
In der 40. Minute tauchte erneut Didavi vor dem Tor auf.
Sein Kopfball nach einer sehenswerten Vorarbeit und Flanke von Silas Wamangituka ging aber weit am Tor vorbei.
Zwei Minuten später machte er es dann aber etwas besser, als er einen Schuss aus 18 Meter an den rechten Pfosten setzte.
Den Abpraller konnte dann Wamangituka ohne Problem zur 0:1-Führung in das leere Tor schieben.
Kurz darauf endete der erste Spielabschnitt.

Die zweite Spielhälfte eröffneten wieder die Gäste mit der ersten Chance in der 52. Minute.
Kalajdzic hatte zunächst eine Distanzschuss aus 20 Meter, den Kolke in Volleyballer-Manier parieren konnte, ehe sein zweiter Versuch von Damian Roßbach geblockt wurde.
Nach einer Stunde hatten die Hausherren die vermutlich größte Chance auf den Ausgleich.
Wataru Endo spielte einen Rückpass direkt in die Füße von John Verhoek, der dann alleine auf das Tor zu laufen konnte und nur am Stuttgarter Schlussmann scheiterte.
Nach dem kurzen Schockmoment erhöhten die Gäste wieder den Druck versuchten die Vorentscheidung zu erzwingen.
In der 67. Minute hatte Darko Churlinov die Möglichkeit zur 2:0-Führung, aber auch er konnte den herausstürmenden Kolke nicht überwinden.
Drei Minuten später tauchte nach einem Konter erneut Kalajdzic vor dem Rostocker Gehäuse auf, doch auch sein Kopfball ging am Tor vorbei.
In der Schlussviertelstunde versuchten die Rostocker nochmal den Ausgleich zu erzwingen, doch bis auf eine Chance von Erik Engelhardt, dessen Kopfball aus sehr spitzem Winkel nicht aufs Tor gebracht werden konnte, passierte in dem Spiel nichts mehr.
So blieb es bis zum Schluss beim 0:1-Sieg für die Schwaben.

Nach Expected Goals gewinnen die Süddeutschen deutlich mit 0,53:3,37.
Daraus ergibt sich aus Rostocker Sicht eine Siegwahrscheinlichkeit von 1,3% und eine Unentscheidenwahrscheinlichkeit von 5%.
In fast 94 von 100 Fällen gewinnen die Gäste.
Ein ähnliches Bild zeichnet sich bei den zu erwartenden Ergbnissen.
Mit 15,7% Wahrscheinlichkeit gewinnen die Stuttgarter dieses Spiel mit 0:3 und mit immer noch 14% Wahrscheinlichkeit sogar mit 0:4.
Das wahrscheinlichste Ergebnis mit einem Rostocker Sieg ist ein 1:0 mit nur 0,4% Wahrscheinlichkeit.