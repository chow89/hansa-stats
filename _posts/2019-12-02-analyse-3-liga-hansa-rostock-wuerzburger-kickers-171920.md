---
title: "Kurzanalyse 17. Spieltag: Hansa Rostock gegen Würzburger Kickers"
description: "Kurzanalyse zum 17. Spieltag 2019/20 zwischen Hansa Rostock und Würzburger Kickers"
tags: [3. Liga, FC Hansa Rostock, Würzburger Kickers, Vincent Müller, Daniel Hägele, Nik Omladic, Dominik Widemann, Markus Kolke, Mirnes Pepic, Korbinian Vollmann, Pascal Breier, Luca Pfeiffer, Leroy Kwadwo, John Verhoek, Aaron Opoku]
categories: []
---

Nach drei Niederlagen in Folge konnte der FC Hansa am 17. Spieltag das Heimspiel gegen die Würzburger Kickers wieder mit einem Sieg beenden.

<canvas id="xgplot171920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['17'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot171920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.WÜRZBURGER_KICKERS,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Das Spiel war zu Beginn ausgeglichen, wobei die Gäste aus Bayern den etwas besseren Start erwischten und in der 4. Minute zu ihren ersten beiden Chancen kamen.
Anschließend wurden auch die Rostocker in der Offensive aktiver und konnten in der 12. Minute den Würzburger Torwart Vincent Müller erstmals testen.
In den nächsten Minute übernahmen die Hausherren immer weiter die Spielkontrolle und erarbeiten sich immer wieder Abschlussgelegenheiten, ohne die Gäste aber ernsthaft in Gefahr bringen zu können.
Auf der Gegenseite passierte bis auf eine Kopfballchance von Daniel Hägele ebenso wenig und so sah es lange nach einer torlosen ersten Halbzeit aus, bis sich die Unterfranken den Ball in der 45. Minute nach einem Omladic-Freistoß ins eigene Tor legten.
Mit dieser 1:0-Führung ging es dann auch in die Halbzeit.
Nach expected Goals stand es zu diesem Zeitpunkt 0,44:0,41.

In der zweiten Hälfte fanden zunächst die Rostocker wieder besser ins Spiel, die erste Großchance hatten aber die Gäste.
Dominik Widemann schaffte es aus kurzer Distanz aber nicht mehr den Ball an Markus Kolke vorbeizulegen.
Im direkten Gegenzug hatten die Rostocker gleich die Möglichkeit ihrerseits das Tor zu erzielen.
Mirnes Pepic scheiterte aber ebenso freistehend am Würzburger Schlussmann.
In den darauffolgenden Minuten drängten die Rostock weiter auf die Vorentscheidungen, konnten die diverse Chancen (Vollmann, Pepic, Breier) jedoch nicht nutzen.
Kurz danach versuchten dann die Würzburger wieder den Ausgleich zu erzielen, aber auch Luca Pfeiffer und Leroy Kwadwo gelang es nicht den Ball hinter die Linie zu befördern.
In den letzten Minuten der Partie konnten die Rostocker nochmal die freien Räume, die durch inzwischen sehr hoch stehenden Würzburger Abwehr entstanden sind, nutzen und zwei Konter gefährlich bis an den gegnerischen Strafraum bringen.
Dort scheiterten aber die Rostocker Angriffsbmeühungen (Verhoek, Opoku) wieder am Gäste-Torwart.
So blieb es bis zum Abpfiff beim 1:0 für die Gastgeber, bei einem xG-Verhältnis von 2,55:1.75.

Somit ist das Spiel auch aus stochastischer Sicht ein verdienter Sieg.
Die Hanseaten gewinnen so ein Spiel mit 56,13% Wahrscheinlichkeit.
20,76% der Spiele enden mit einem Unentschieden und 23,11% mit einem Sieg für die Würzburger.
Das Bild bei den Ergebnissen ist sehr ausgeglichen.
Das wahrscheinlichste Ergebnis ist ein 2:2 mit 9,17%.
Es folgen ein 2:1 (9,01%), 3:2 (8,23%) und 3:1 (8,09%).
Mit 1:0 endet so ein Spiel mit nur 2,11% Wahrscheinlichkeit.