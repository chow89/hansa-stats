---
title: "Kurzanalyse 18. Spieltag: KFC Uerdingen gegen Hansa Rostock"
description: "Kurzanalyse zum 18. Spieltag 2019/20 zwischen KFC Uerdingen und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, KFC Uerdingen, Nik Omladic, Tom Boere, Pascal Breier, Korbinian Vollmann, Lukas Scherff, Dominic Maroh, Franck Evina, Boubacar Barry, Markus Kolke]
categories: []
---

Im letzten Auswärtsspiel der Hinrunde mussten sich die Rostocker dem KFC Uerdingen geschlagen geben.
Nach 90 Minuten gewannen die Krefelder deutlich mit 4:1.

<canvas id="xgplot181920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['18'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot181920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.KFC_UERDINGEN,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Teams begannen das Spiel sehr verhalten, so dass es lange keine Torraumszenen gab und die Zuschauer bis zur 21 Minute auf die erste gefährliche Torchance warten mussten.
Diese Chance führte sogleich zum 1:0 für die Rostocker.
Nik Omladic köpfte einen vom Uerdinger Torwart schwach geklärten Ball ohne Gegenwehr in die Maschen.
Die Freude über die Führung hielt aber nicht lange.
Nur eine Minute später gelang den Hausherren durch Tom Boere der 1:1-Ausgleich.
In den Folgeminuten hatten die Rostocker nochmal zwei gute Chancen.
Die beiden über die rechte Seite von Omladic eingeleiteten Kopfball-Möglichkeiten konnten Pascal Breier und Korbinian Vollmann aber nicht zur erneuten Führung nutzen.
In der 37. Minute gelang dafür den Krefeldern die Führung.
Nach einem Evina-Freistoß nutze Boere das Chaos im Rostocker Strafraum zu einem Torschuss, den Lukas Scherff unglücklich und unhaltbar in den eigenen Kasten abfälschte.
Mit dem 2:1 gingen beide Mannschaften in die Halbzeit. In der xG-Statistik stand es zu diesem Zeitpunkt 1,11:1,02.

Die zweite Halbzeit begann dann wieder genauso verhalten wie der erste Spielabschnitt.
Aber wieder war es die erste Torchance der Halbzeit, die zum Tor führte.
Diesesmal war es der Uerdinger Dominic Maroh, der nach einem Freistoß auf 3:1 erhöhte (61. Minute).
Nur wenige Minuten später hätten die Uerdinger durch Franck Evina (65. Minute) und Boere (67. Minute) den Sack zu machen können.
Anschließend wurde es wieder etwas ruhiger, ehe Boubacar Barry in der 86. Minute das 4:1 auf dem Fuß hatte, aber an Markus Kolke scheiterte.
In der Nachspielzeit holte Boere das dann aber nach und so endete das Spiel mit 4:1 und 2,79:1,23 xG.

In der stochastischen Betrachtung liegen die Gastgeber ebenfalls deutlich vorne.
In 73,56% der Fälle gewinnen die Uerdinger das Spiel, während die Rostocker nur 10,94% der Spiele für sich entscheiden.
Mit 15,5% Wahrscheinlichkeit endet das Spiel Unentschieden.
Insgesamt ist die Niederlage der Rostocker aber zu hoch ausgefallen.
Nur 6,92% der Spiele enden mit diesem Ergebnis.
Wahrscheinlicher sind dagegen ein 3:1 (10,63%) und ein 2:1 (10,26%).