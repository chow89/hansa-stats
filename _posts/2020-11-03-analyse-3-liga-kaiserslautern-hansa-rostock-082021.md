---
title: "Kurzanalyse 8. Spieltag: 1. FC Kaiserslautern gegen Hansa Rostock"
description: "Kurzanalyse zum 8. Spieltag 2020/21 zwischen 1. FC Kaiserslautern und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, 1. FC Kaiserslautern, Kenny Prince Redondo, John Verhoek, Marvin Pourié, Damian Roßbach, Markus Kolke]
categories: []
---

Der FC Hansa musste am 8. Spieltag zum Montagsspiel nach Kaiserslautern.
Das umkämpfte und chancenarme Spiel endete ohne Tore 0:0.

<canvas id="xgplot082021"></canvas>
<canvas id="winratio082021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['8'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot082021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_KAISERSLAUTERN,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio082021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften waren von Beginn an auf Sicherheit bedacht, wobei die Lauterer die Spielkontrolle übernahmen und die Hanseaten auf Konter lauerten.
Die Hausherren konnten so in der zweiten Minute erstmals vor das Rostocker kommen.
Der Kopfball von Kenny Prince Redondo verfehlte das Ziel jedoch knapp und ging über die Latte.
In der 26. Minuten kamen die Rostocker zu ihrer ersten Torchance.
John Verhoek kam nach einer Ecke von John Verhoek als erster an den Ball, doch auch sein Kopfball ging über die Latte.
Aufgrund der guten Defensivarbeit von beiden Mannschaften ereigneten sich im Rest der Halbzeit keine weiteren nennenswerten Torchancen.

Im zweiten Spielabschnitt traten die Gäste mutiger auf und übernahmen die Spielkontrolle.
Doch auch aus die Lage ergaben sich für die Hanseaten keine weiteren Torchancen.
Die einzige gute Chance der Halbzeit hatten die Gastgeber in der 75. Minute.
Marvin Pourié konnte nach einem Fehler im Spielaufbau der Rostocker fast unbedrängt auf das Gastetor zulaufen.
Erst durch den Einsatz von Damian Roßbach konnte der Stürmer aus dem Tritt und in eine schlechtere Schussposition gebracht werden.
Markus Kolke im Rostocker Tor hatte mit dem Schuss dann keine Probleme mehr.
Auch in der letzten Viertelstunde des Spiels ereigneten sich keine großen Torchancen mehr, so dass sich beiden Teams mit 0:0 trennten.

Nach Expected Goals liegen die Hausherren knapp mit 0,72:0,54 vorne.
Daraus resultiert für die Rostocker eine Gewinnwahrscheinlichkeit von etwas über 23%, während die Lauterer in fast 36% aller Spiele gewinnen.
Über 40% der Spiele enden Unentschieden.
Durch die geringe Anzahl an Torchancen ist ein 0:0 mit 23,7% das wahrscheinlichste Ergebnis in diesem Spiel.
Danach folgen ein 1:0-Heimsieg (22,6%), ein 0:1-Auswaärtssieg (17,1%) und 1:1-Unentschieden (16,3%).