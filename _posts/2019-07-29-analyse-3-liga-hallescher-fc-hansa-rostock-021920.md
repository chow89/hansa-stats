---
title: "Kurzanalyse 2. Spieltag: Hallescher FC gegen Hansa Rostock"
description: "Kurzanalyse zum 2. Spieltag 2019/20 zwischen Halleschem FC und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Hallescher FC, Mirnes Pepic, Pascal Breier, Jonas Hildebrandt, Korbinian Vollmann, Niklas Landgraf, Jonas Nietfeld, Sebastian Mai, Björn Jopek]
categories: []
---

Für den FC Hansa ging es am 2. Spieltag zum Viertplatzierten der vergangenen Saison, dem Halleschen FC.
Die Rostock zeigten sich im Vergleich zum letzten Spieltag defensiv verbessert, verloren dennoch knapp mit 1:0.

<canvas id="xgplot021920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['2'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot021920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.HALLESCHER_FC,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Im xG-Plot ist zu sehen, dass das Team von Jens Härtel in der ersten Halbzeit offensiv kaum in Erscheinung trat und erst in der 44. Minute durch Mirnes Pepic eine Torchance erspielen konnte, die allerdings schon vom Verteidiger geblockt wurde.
Nach der Pause fanden die Hanseaten dann besser ins Spiel und konnten sich einige nennenswerte Chancen erarbeiten (Pascal Breier, zweimal Jonas Hildebrandt, Korbinian Vollmann).
In der Folge verflachten die Offensivebemühungen aber wieder, ehe Pascal Breier in der 82. Minute, nach dem Platzverweis für Halles Niklas Landgraf, durch einen Fernschuss erneut zu einer Torchance kam.
Am Ende reichte es dieses Mal jedoch nicht für ein Tor, obwohl es von der Qualität der Torchancen durchaus verdient gewesen wäre (0,92 Gesamt-xG).

Aus Hallenser Sicht sah das Spiel in der ersten Halbzeit ähnlich aus.
In der 8. Minute konnten sich erst Jonas Nietfeld mit einem sehenswerten Freistoß und dann Sebastian Mai mit einem Kopfball bei der anschließenden Ecke auszeichnen.
In der restlichen Zeit konnten sich die Hallenser dann keine großen Torchancen mehr herausspielen.
Erst nach dem Seitenwechsel übten die Saalestädter wieder vermehrt Druck auf das Rostocker Gehäuse aus.
Aber auch diese Offensivbemühungen fanden nach der 60. Minute ein Ende, sodass es in der Folge lange nach einem torlosen Unentschieden aussah.
Zwei Minuten vor Ende der regulären Spielzeit gelang der Heimmannschaft dann aber doch noch der Siegtreffer durch einen Distanzschuss von Björn Jopek.
Unterm Strich kamen die Hallenser auf 1,62 Gesamt-xG, womit der Sieg am Ende auch verdient ist.
