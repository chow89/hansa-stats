---
title: "Kurzanalyse 36. Spieltag: Hansa Rostock gegen FSV Zwickau"
description: "Kurzanalyse zum 36. Spieltag 2020/21 zwischen Hansa Rostock und FSV Zwickau"
tags: [3. Liga, FC Hansa Rostock, FSV Zwickau, Pascal Breier, Can Coskun, Marco Schikora, Ronny König, Julian Riedel, Philip Türpitz, John Verhoek, Johannes Brinkies, Sven Sonnenberg]
categories: []
---

Am drittletzten Spieltag begrüßte der FC Hansa den FSV Zwickau.
Die offen ausgetragenen und sehr umkämpfte Partie endete ohne Tore.

<canvas id="xgplot362021"></canvas>
<canvas id="winratio362021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['36'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot362021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FSV_ZWICKAU,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio362021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio362021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hanseaten versuchten von Anfang an das Spiel zu kontrollieren, während die Gäste nur über Konter und Standardsituationen vor das Rostocker Tor kamen.
Die erste Torchance hatte dann Pascal Breier in der 13. Minute nach einem Konter.
Der Stürmer spielte den letzten verbleibenden Verteidiger an der Strafraumkante aus, schoss dann aber nur knapp neben das Tor.
Nach 28 Minuten kamen auch die Gäste zu ihren ersten Torchancen.
Erst versuchte sich Can Coskun mit einem Freistoß, der aber an der Mauer hängen blieb.
Auch der Nachschuss von Marco Schikora konnte geblockt werden.
Drei Minuten später hatte Ronny König die nächste Möglichkeit für die Sachsen.
Dem Kopfball, der das Tor nur knapp verfehlte, ging wieder ein Freistoß voraus.

Nach dem Seitenwechsel hatte Julian Riedel in der 50. Minute die erste Torchance des Spielabschnitts.
Nach einem Eckstoß von Philip Türpitz kam der Verteidiger aus kurzer Distanz zum Schuss, doch auch sein Schuss wurde geblockt.
20 Minuten später hatte John Verhoek die nächste Chance.
Bei einem Freistoß kam der Niederländer als erster an das Spielgerät.
Seinen Kopfball konnte der Ex-Rostocker Johannes Brinkies im Zwickauer Tor aber halten.
Wenige Sekunden vor dem Abpfiff bekamen die Hanseaten erneut eine Ecke zugesprochen, doch auch der Schuss von Sven Sonnenberg endete in den Händen von Brinkies.
Direkt danach beendete der Schiedsrichter das Spiel mit dem torlosen Unentschieden.

Nach Expected Goals liegen die Rostocker mit 1,8:0,73 klar vorne.
Dementsprechend gewinnen die Hanseaten 66% aller Spiele.
22% der Spiele enden Unentschieden, die restlichen 12% enden mit einem Zwickauer Auswärtssieg.
Das wahrscheinlichste Ergebnis bei diesem Spiel ist ein 2:0-Heimsieg (14,7%).
Dahinter folgen ein 1:0-Heimsieg (13,9%), ein 2:1-Heimsieg (13%) und ein 1:1-Unentschieden (12,3%).
Ein 0:0-Unentschieden kommt nur in 5,1% der Spiele zu Stande.