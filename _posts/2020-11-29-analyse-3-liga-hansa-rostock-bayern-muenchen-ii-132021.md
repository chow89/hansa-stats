---
title: "Kurzanalyse 13. Spieltag: Hansa Rostock gegen Bayern München II"
description: "Kurzanalyse zum 13. Spieltag 2020/21 zwischen Hansa Rostock und Bayern München II"
tags: [3. Liga, FC Hansa Rostock, FC Bayern München II, Lenn Jastremski, Sven Sonnenberg, Jannik Rochelt, Julian Riedel, Nicolas Feldhahn, Lukas Scherff, Oliver Daedlow, Korbinian Vollmann, Bentley Baxter Bahn, John Verhoek, Lukas Schneller, Manuel Farrona Pulido]
categories: []
---

Am 13. Spieltag begrüßten die Hanseaten den Vorjahresmeister Bayern München II im heimischen Ostseestadion.
Nach 90 Minuten behielten die Rostocker die Oberhand und gewannen mit 2:0.

<canvas id="xgplot132021"></canvas>
<canvas id="winratio132021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['13'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot132021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.BAYERN_MÜNCHEN_II,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio132021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio132021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die jungen Bayern begannen die Partie wie die Feuerwehr und hatten nur wenige Sekunden nach dem Anpfiff die erste große Torchance.
Nach einem Steilpass von Lenn Jastremski und einer verunglückten Grätsche von Sven Sonnenberg läuft Jannik Rochelt frei auf das Tor zu und wird im Moment des Schusses nur von Julian Riedel gestört, der seinen Fuß entscheidend dazwischen bringt und den Ball zur Ecke klärt.
Dieses landet dann zwei Meter vor dem Tor auf dem Kopf von Nicolas Feldhahn, der den Ball aber nicht auf das Tor bringen konnte.
Auf der Gegenseite hatten die Hausherren in der fünften Minute ihren ersten Torschuss.
Nach einer Flanke von Lukas Scherff und einer Kopfball-Verlängerung von Oliver Daedlow kommt Korbinian Vollmann ebenfalls mit dem Kopf an den Ball.
Sein Kopfstoß lässt den Ball dann allerdings ein gutes Stück über das Tor gehen.
In der 17. Minute hatten die Rostocker die nächsten Tormöglichkeiten.
Bentley Baxter Bahns Hereingabe landete bei John Verhoek, der aber nur den Münchener Torwart Lukas Schneller anköpfte.
Den Nachschuss von Manuel Farrona Pulido konnte ein Verteidiger abblocken.
Zwei Minuten später versuchte sich Vollmann mit einem Schuss von der Strafraumkante, doch auch dieser Schuss wurde geblockt und ging dann ins Toraus.
Nach einer halben Stunde kamen die kleinen Bayern wieder vor das Rostocker Tor.
Der Kopfball von Jastremski ging aber auch hier weit über das Tor.
In der letzten Viertelstunde der Halbzeit passierte dann nichts mehr vor den Toren und so ging es beim Stand von 0:0 zurück in die Kabinen.

Nach dem Wiederanpfiff standen die Rostocker etwas besser in der Defensive und ließen so keine Chancen der Münchener zu und sorgten durch das ruhigere Aufbauspiel auch für mehr Akzente nach vorne.
Trotzdem dauerte es bis zur 60. Minute ehe Bahn eine Flanke in den bayerischen Strafraum brachte, die dann von Verhoek auf Riedel abgelegt wurde und dieser dann zum 1:0 verwandelte.
Nachdem die Gegenwehr des Vorjahresmeisters ein wenig gebrochen wurde, konnten die Gastgeber nur sechs Minuten später auf 2:0 erhöhen.
Farrona Pulido nahm dabei einen Pass von Nico Neidhart an und schloss sofort mit einem sehenswerten Dropkick ab.
Nach der Vorentscheidung stellten die Hanseaten die Offensivbemühungen weitestgehend ein und verwalteten nur noch die Führung bis der Schiedsrichter das Spiel schlussendlich abpfiff.

In der Expected Goals Statistik verlieren die Hanseaten das Spiel knapp mit 1,21:1,36.
Daraus ergibt sich für die Hausherren eine Gewinnchance von etwa 30%.
Die Wahrscheinlichkeit für ein Unentschieden ist in etwa gleich groß.
Die Bayern gewinnen dieses Spiel in circa 40% der Fälle.
Entsprechend der Expected Goals ist 1:1 auch das wahrscheinlichste Ergebnis mit einer Eintrittswahrscheinlichkeit von über 16%.
Dahinter folgt ein 1:2-Auswärtssieg mit fast 13% und ein 0:1-Auswärtssieg mit etwa 10% Wahrscheinlichkeit.
Ein 2:0-Heimsieg tritt nur in 4% aller Spiele ein.