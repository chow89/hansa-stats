---
title: "Kurzanalyse 15. Spieltag: Hansa Rostock gegen SV Meppen"
description: "Kurzanalyse zum 15. Spieltag 2020/21 zwischen Hansa Rostock und SV Meppen"
tags: [3. Liga, FC Hansa Rostock, SV Meppen, Valdet Rama, Manuel Farrona Pulido, Erik Domaschke, René Guder, Pascal Breier, Julian Riedel, Bentley Baxter Bahn, Markus Kolke, Jan Löhmannsröben, Hassan Amin]
categories: []
---

Am 15. Spieltag begann für den FC Hansa die nächste englische Woche mit dem Spiel gegen den SV Meppen.
Nach 90 Minuten gingen die Kicker von der Ostsee mit einer 0:2-Niederlage vom Platz.

<canvas id="xgplot152021"></canvas>
<canvas id="winratio152021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['15'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot152021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SV_MEPPEN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio152021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio152021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften kämpften von Beginn an um jeden Ball und versuchten schnell in Führung zu gehen.
So ging die erste gute Torchance in der fünften Minute auf das Konto der Meppener.
Die Gäste spielten nach einem Ballgewinn in der eigenen Hälfte schnell nach vorne zu Valdet Rama, der dann mit einem Schuss von der Strafraumkante sein Glück versuchte und scheiterte.
In der zwölfte Minute spielten sich dann auch die Hausherren die erste Möglichkeit durch Manuel Farrona Pulido heraus.
Sein Versuch konnte aber vom Gäste-Keeper Erik Domaschke in Handball-Torwart-Manier zur Ecke geklärt werden.
Danach wurde es bis auf einige Halbchancen ruhig, ehe die Gäste in der 36. Minute wieder durch einen Konter vor das Tor kamen.
Dieses Mal machte es Rama besser und köpfte die Flanke von René Guder zur 0:1-Führung in das Tor.
In der verbleibenden Zeit des ersten Spielabschnittes passierte nichts mehr und so gingen die beiden Mannschaften mit diesem Zwischenstand auch wieder in die Kabinen.

Nach dem Seitenwechsel hatten die Rostocker die besseren Start.
In der 53. Minute hatte Pascal Breier die nächste Torchance für die Hanseaten, sein Schuss konnte aber geblockt werden.
Auch danach wurde es erstmal wieder ruhig bis auf eine Vielzahl an kleineren Chancen.
Acht Minuten vor Schluss hatte dann Julian Riedel den Ausgleich auf dem Fuß, doch auch hier sollte das Spielgerät nicht den Weg ins Tor finden.
In der zweiten Minuten der Nachspielzeit kam Bentley Baxter Bahn zur letzten Möglichkeit der Rostocker.
Doch auch er konnte den Ball nur den Pfosten befördern.
Wenige Sekunden vor dem Abpfiff rollte dann noch ein Meppener Konter auf das Tor von Markus Kolke zu, den Jan Löhmannsröben nur mit einem Foul im Strafraum unterbinden konnte.
Hassan Amin konnte den anschließenden Elfmeter sicher zum 0:2 verwandeln.
Direkt danach beendete der Schiedsrichter das Spiel.

Auch wenn das Spiel nur wenige große Torchance bat, endete das Spiel nach Expected Goals mit 1,42:0,73.
Aufgrund dieses großen Unterschiedes gewinnen die Rostocker so eine Partie in 54% aller Spiele und holen in über 28% der Spiele zumindest einen Punkt.
Die Meppener gewinnen in über 17% der Spiele.
Entsprechend deutlich sind auch die wahrscheinlichsten Ergebnisse.
Mit über 16% Wahrscheinlichkeit endet das Spiel 1:1-Unentschieden.
Dahinter folgen ein 1:0-Heimsieg (15,2%), ein 2:1-Heimsieg (12,1%) und ein 2:0-Heimsieg (11,3%).
Ein Meppener 0:2-Sieg hat nur eine Eintrittswahrscheinlichkeit von 2,5%.