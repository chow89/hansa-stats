---
title: "Kurzanalyse 34. Spieltag: MSV Duisburg gegen Hansa Rostock"
description: "Kurzanalyse zum 34. Spieltag 2019/20 zwischen MSV Duisburg und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, MSV Duisburg, Pascal Breier, Nils Butzen, Kai Bülow, Nico Granatowski, Aaron Opoku, Nico Neidhart, Lukas Daschner, Julian Riedel, Markus Kolke, Sven Sonnenberg, Daniel Hanslik, Leo Weinkauf, John Verhoek, Lukas Scherff]
categories: []
---

Nach vier Siegen in Folge konnte der FC Hansa seine Serie gegen den MSV Duisburg nicht fortsetzen.
Das ereignisarme, aber dennoch spannende Spiel endet ohne Tore mit 0:0.

<canvas id="xgplot341920"></canvas>
<canvas id="winratio341920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['34'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot341920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.MSV_DUISBURG,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio341920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften agierten von Anfang an mutig und versuchten die Spielkontrolle auf ihre Seite zu ziehen.
Während die Duisburg zunächst mehr Spielanteile hatten, waren die Rostocker vor dem gegnerischen Tor gefährlicher.
Bereits in der zweiten Minute hatte Pascal Breier nach einem langen Ball von Nils Butzen die große Chance zur Führung, doch sein Schuss ging deutlich über das Tor.
Nur zwei Minuten später stieg Kai Bülow bei einer Ecke von Nico Granatowski am höchsten, aber auch sein Kopfball verfehlte das Tor.
Die nächste Möglichkeit hatte Aaron Opoku in der elften Minute.
Wie bei den ersten beiden Torchancen ging auch sein Ball am Tor vorbei.
In der 15. Minute versuchte es Nico Neidhart, der aber ebenfalls das Tor verpasste.
In der Folge stellte sich die Duisburger Verteidigung besser auf die Rostocker Angriffsbemühungen ein und so wurde es erstmal etwas ruhiger im Spiel.
Den nächsten großen Aufreger gab es in der 32. Minute vor dem Rostocker Tor, als sich Lukas Daschner erst gegen Julian Riedel und dann gegen Markus Kolke durchsetzte und der Ball auf das leere Tor zukullerte.
Sven Sonnenberg konnte den Ball aber noch in höchster Not klären.
Den Rest der Halbzeit ereignete sich nichts nennenswertes mehr vor den Toren und so gingen die beiden Mannschaften torlos in die Kabinen.

Nach der Halbzeitpause waren die Hanseaten das dominierende Team und die Gastgeber versuchten aus Kontern Kapital zu schlagen, scheiterten aber immer wieder an der Rostocker Defensive.
Die erste Möglickeit des Spielabschnitts hatte Daniel Hanslik, der eine Opoku-Hereingabe im Strafraum auf das Tor beförderte, aber am Duisburger Torwart Leo Weinkauf scheiterte.
In der 71. Minute hatte nochmal John Verhoek die Möglichkeit die Führung zu erzielen, doch auch er konnte die Scherff-Flanke aus fünf Metern nicht aufs Tor bringen.
Und so blieb es bis zum Ende ohne Tore und beide Teams teilten sich die Punkte.

Stochastisch betrachtet gewinnen die Hanseaten in 60% der Spielen, während die Zebras nur 12% der Spiele gewinnen. 28% der Spiele enden Unentschieden.
Mit 18% Wahrscheinlichkeit ist ein 1:0-Auswärtssieg das erwartbarste Ergebnis.
Mit 17% fast genauso wahrscheinlich ist ein 1:1-Unentschieden.
Nur in 9% der Fällen endet das Spiel mit 0:0.