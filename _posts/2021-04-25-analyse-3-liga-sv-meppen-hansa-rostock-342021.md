---
title: "Kurzanalyse 34. Spieltag: SV Meppen gegen Hansa Rostock"
description: "Kurzanalyse zum 34. Spieltag 2020/21 zwischen SV Meppen und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, SV Meppen, Jeron Al-Hazaimeh, Nik Omladic, Simon Rhein, John Verhoek, Marcus Piossek, Julian Riedel, Markus Kolke, Bentley Baxter Bahn, Pascal Breier, Markus Ballmert, Christoph Hemlein, Jan Löhmannsröben, Dejan Bozic, Philip Türpitz]
categories: []
---

Der FC Hansa gastierte am 34. Spieltag beim SV Meppen.
Die Partie, die zur Halbzeit vorentschieden schien, wurde im zweiten Abschnitt nochmal spannend und endete mit einem knappen 2:3-Auswärtserfolg.

<canvas id="xgplot342021"></canvas>
<canvas id="winratio342021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['34'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot342021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SV_MEPPEN,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio342021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio342021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hausherren waren von Beginn an das spielbestimmende Team und hatten nach 13 Minuten ihre erste gute Chance.
Jeron Al-Hazaimeh kam bei einem Freistoß völlig frei vor dem Rostocker Tor zum Kopfball, verfehlte das Gehäuse aber deutlich.
Nach rund der Hälfte der ersten Halbzeit fanden auch die Rostocker besser in das Spiel und kamen folglich in der 24. Minute durch Nik Omladic zu ihrer ersten Tormöglichkeit.
Der Schuss des Slowenen konnte aber geblockt und zur Ecke geklärt werden.
Drei Minuten später konnte Simon Rhein im Aufbauspiel der Meppener den Ball für sich gewinnen und den Ball von rechts auf John Verhoek flanken.
Der Niederländer stand vor dem Meppener Tor komplett frei und köpfte den Ball zum 0:1 ins Netz.
Die Gastgeber zeigten sich von dem Rückstand nicht sonderlich geschockt und kamen ihrerseits in der 31. Minute zu ihrer nächsten Chance.
Marcus Piossek zog dabei mit einem Volleyschuss von der Strafraumkante ab.
Julian Riedel konnte den Schuss aber noch für den bereits geschlagenen Markus Kolke auf der Linie retten.
Wenige Minuten vor dem Halbzeitpfiff erarbeiteten sich die Hanseaten noch zahlreiche Chance auf der Gegenseite.
Erst war es Bentley Baxter Bahn, dessen Schuss nur knapp am Tor vorbeiging.
Kurz darauf kam Omladic zu seiner nächsten Chance, doch auch diese konnte geblockt werden.
In der 43. Minute machte es dann Pascal Breier besser, der einen Freistoß von Rhein zum 0:2 in das Tor verlängerte.
Mit diesem Zwischenstand ging es dann auch in die Halbzeit.

Nach der Pause kamen zunächst die Hanseaten wieder besser in das Spiel zurück, konnten sich aber keine Chance erspielen um das Spiel zu entscheiden.
Und so waren es die Hausherren, die in der 60. Minute den Anschlusstreffer erzielen konnte.
Wieder ging dem Tor ein Freistoß voraus, den Markus Ballmert zum 1:2 einnicken konnte.
Sieben Minuten später folgte dann sogar der Ausgleich, als die Rostocker bei einer kurz ausgeführten Ecke nicht aufmerksam waren und Christoph Hemlein die Flanke ohne Gegenwehr zum 2:2 einköpfen konnte.
Das Momentum war nur bei den Meppener, die in der 71. Minute sogar die Führung auf den Fuß hatten.
Erneut kam Ballmert vor dem Tor an den Ball, dieses Mal fand sein Schuss aber nicht den Weg ins Tor.
In den letzten Minuten entwickelte sich das Spiel zu einem offenen Schlagabtausch.
Jan Löhmannsröben hatten in der 82. Minute die nächste Möglischkeit für die Gäste.
Sein Kopfball nach einer Ecke ging aber knapp über das Tor.
In der 85. Minute tauchten erneut die Meppener vor dem Tor von Kolke auf.
Doch auch der Kopfball von Dejan Bozic ging über das Tor.
In der Nachspielzeit erhielten die Gäste dann noch einen Freistoß in Strafraumnähe zugesprochen.
Die Niedersachsen taten sich aber schwer den Ball aus der Gefahrenzone zu klären und so gelang Philip Türpitz per Volleyschuss doch noch der 2:3-Siegtreffer.

Bei den Expected Goals liegen die Meppener mit 1,84:1,65 leicht vorne.
Daraus ergibt sich für die Hanseaten eine Gewinnwahrscheinlichkeit von fast 33%, während die Hausherren in über 42% der Spiele vorne liegen.
Rund 25% der Spiele enden Unentschieden.
Das wahrscheinlichste Ergebnis bei diesem Spiel ist ein 2:1-Heimsieg mit 11% Wahrscheinlichkeit.
Dahinter folgen ein 2:2 und 1:1-Unentschieden mit jeweils 10% Wahrscheinlichkeit.
Mit 2:3 enden etwa 5,1% der Spiele.