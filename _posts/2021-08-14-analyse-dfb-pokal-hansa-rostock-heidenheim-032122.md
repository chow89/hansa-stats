---
title: "Kurzanalyse 1. Runde DFB-Pokal: Hansa Rostock gegen 1. FC Heidenheim"
description: "Kurzanalyse zur 1. DFB-Pokalrunde 2020/21 zwischen Hansa Rostock und 1. FC Heidenheim"
tags: [FC Hansa Rostock, 1. FC Heidenheim, DFB-Pokal, Oliver Hüsing, Patrick Mainka, Tobias Mohr, Patrick Schmidt, Damian Roßbach, Bentley Baxter Bahn, Jan Schöppner, Markus Kolke, Ridge Munsy, Kevin Schumacher, Calogero Rizzuto, Christian Kühlwetter, Stefan Schimmer, Tim Kleindienst, Julian Riedel]
categories: []
---

Nach drei Gastspielen des VfB Stuttgarts begrüßten die Rostocker in der diesjährigen Erstrunden-Pokalpartie den 1. FC Heidenheim im heimischen Ostseestadion.
Nach 120 hochumkämpften Minuten gewannen die Hanseaten knapp mit 3:2 und zogen in die nächste Pokalrunde ein.

<canvas id="xgplotcup012122"></canvas>
<canvas id="winratiocup012122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].cup['1'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplotcup012122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.HEIDENHEIM,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratiocup012122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratiocup012122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften taten sich schwer in das Spiel zu finden, wobei die Gäste zunächst etwas aktiver waren.
Torraumszenen blieben in der Anfangsphase jedoch Mangelware.
Es dauerte bis zur 21. Minute ehe die Gäste in Person von Oliver Hüsing ihre erste gute Torchance hatten.
Der Ex-Rostocker köpfte aber knapp über das Rostocker Tor.
Nur vier Minuten später machten es die Heidenheimer dann aber besser.
Patrick Mainka köpfte eine Ecke von Tobias Mohr ungehindert zum 0:1 in das Tor.
Nach dem Gegentor wurde es erstmal wieder etwas ruhiger in den Strafräumen, ehe die Gäste in der 37. Minute erneut gefährlich vor das Tor kamen.
Patrick Schmidt wurde dabei aus einer Umschaltsituation heraus von Mohr steil geschickt und tauchte plötzlich vor dem Rostocker Tor auf.
Damian Roßbach konnte seinen Schuss aber im letzten Augenblick noch blocken.
Anschließend passierte nichts mehr und so gingen die Mannschaften beim Zwischenstand von 0:1 in die Pause.

Im zweiten Spielabschnitt wurden die Rostocker dann aktiver und hatten in der 57. Minute auch die erste Torchance der Halbzeit.
Nach einem Freistoß von Bentley Baxter Bahn kam Roßbach an den Ball, traf aber nur den Pfosten.
Der Abpraller landete auf dem Fuß von Mainka und ging dann zum 1:1 in das Tor.
Auch nach diesem Tor wurde es in den Strafräumen auf beiden Seiten wieder ruhiger.
In der 77. Minute kamen die Gäste zu ihrer nächsten Möglichkeit.
Nach einem Fehler im Aufbauspiel der Rostocker gelang der Ball zu Jan Schöppner, der mit seinem Abschluss Markus Kolke aber nicht wirklich fordern konnte.
Nachdem auch in den letzten Minuten der regulären Spielzeit wenig passierte, blieb es beim 1:1 und so ging es in die Verlängerung.

Auch im dritten Spielabschnitt gehörte den Hanseaten die erste Torchance.
Über die linke Seite leitete Ridge Munsy den Angriff in der 94. Minute ein und fand Kevin Schumacher im Rücken der Abwehr.
Schumacher zog sofort ab, doch auch eher traf nur den Pfosten, ehe der Abpraller bei Calogero Rizzuto landete, der dann zum 2:1 einnetzen konnte.
Bereits in der 2. Halbzeit der Verlängerung fanden auch die Heidenheimer wieder den Weg vor das Rostocker Tor.
Christian Kühlwetter zog einfach mal aus rund 20 Metern ab und zwang Kolke so zu einer Glanzparade.
Den zur Seite abgewehrten Ball konnte dann aber Stefan Schimmer nutzen und zum 2:2 im Tor versenken.
Fünf Minuten vor dem Ende hatte Tim Kleindienst dann sogar die Chance zur Führung, doch die Flanke von der rechten Seite war so scharf vor das Tor geschlagen worden, dass der Heidenheimer den Ball nicht gezielt auf das Tor bringen konnte.
Munsy machte es in der letzten Minute auf der Gegenseite dagegen besser.
Nach einer Halbfeldflanke von Julian Riedel kam der Stürmer unbedrängt zum Kopfball und erzielte das entscheidende 3:2.
Eine Minute später pfiff der Schiedsrichter das Spiel ab und die Rostocker gingen als Sieger vom Platz.

Nach Expected Goals geht das Spiel andersrum aus, dort gewinnen die Gäste mit 1,03:1,65.
Daraus ergibt sich für die Rostocker eine Siegwahrscheinlichkeit von etwas über 21%, während die Heidenheimer mit über 53% Wahrscheinlichkeit gewinnen.
Rund 25% der Spiele enden unentschieden und müssen mit einem Elfmeterschießen entschieden werden.
Die wahrscheinlichsten Ergebnisse sind ein 1:1-Unentschieden (13,2%), ein 1:2-Auswärtssieg (12,1%) und ein 0:1-Auswärtssieg (11,0%).
Mit 3:2 gewinnen die Rostocker nur in 1,8% aller Spiele.