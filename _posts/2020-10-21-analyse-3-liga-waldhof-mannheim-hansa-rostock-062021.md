---
title: "Kurzanalyse 6. Spieltag: Waldhof Mannheim gegen Hansa Rostock"
description: "Kurzanalyse zum 6. Spieltag 2020/21 zwischen Waldhof Mannheim und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Waldhof Mannheim, Nik Omladic, Dominik Martinovic, Rafael Garcia, Markus Kolke, Bentley Baxter Bahn, Maurice Litka, John Verhoek, Marcel Costly, Anton Donkor]
categories: []
---

Zu Beginn der englischen Woche ging es für die Rostocker zu Waldhof Mannheim.
Durch eine vorallem defensiv starke Leistung gewannen die Gäste mit 1:2.

<canvas id="xgplot062021"></canvas>
<canvas id="winratio062021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['6'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot062021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.WALDHOF_MANNHEIM,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio062021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Partie brauchte lange um in Fahrt zu kommen.
Erst in der 20. Minute hatten die Hanseaten durch Nik Omladic die erste Torchance des Spiels.
Sein Kopfball ging jedoch nur an den Kopf seines Gegenspielers und von da aus in die Arme des Torhüters.
In der 31. Minute bekamen die Hausherren durch einen Elfmeter die Riesenchance zur Führung.
Der gefoulte Dominik Martinovic trat selbst an, traf aber nur den Pfosten und so blieb es zunächst beim 0:0.
Acht Minuten später hatten die Mannheimer die nächste Chance.
Rafael Garcias Schuss von der Strafraumkante landete dann aber in den Händen von Markus Kolke.
Drei Minuten vor Schluss dribbelte sich Bentley Baxter Bahn in den Strafraum und legte schließlich auf den mitgelaufenen Maurice Litka ab, der dann zum 0:1 einnetzen konnte.
Die Heimherren ließen sich davon jedoch nicht beeindrucken und machten im Gegenzug gleich wieder Druck.
Eine Minute vor der Halbzeitpause wurden sie dafür belohnt.
Martinovic spitzelte eine Hereingabe an Kolke vorbei ins Tor, sodass die beiden Mannschaften beim Stand von 1:1 wieder in die Kabinen durften.

Gleich nach dem Wiederanpfiff holten sich die Hanseaten die Führung wieder zurück.
Ein Freistoß von Litka landete auf dem Kopf von John Verhoek, der den Ball dann in das Tor köpfte.
In der 54. Minute kamen die Mannheimer durch Marcel Costly wieder gefährlich das Tor, sein Ball ging aber ein gutes Stück am Tor vorbei.
Zwei Minuten später hatten die Gastgeber die nächste Möglichkeit.
Der Ex-Hanseat Anton Donkor bekam einen Eckball auf den Kopfball, doch auch er konnte den Ball nicht in Richtung Tor drücken.
Nach einer längeren Ruhephase kamen die Mannheimer in der 73. Minute wieder gefährlich vor das Tor.
Wieder war es Costly, der dieses Mal die Latte traf.
Die letzte große Chance hatte Martinovic in der 85. Minute.
Sein Kopfball war kein Problem für Kolke.

Nach Expected Goals gewinnen die Mannheimer mit 1,78:0,86.
Daraus ergibt sich für Hansa eine Siegwahrscheinlichkeit von etwa 15% und eine Niederlagenwahrscheinlichkeit von über 61%.
Etwas über 23% der Spiele enden Unentschieden.
Entsprechend ist das wahrscheinlichste Ergebnis ein 2:1-Heimsieg mit 13,2%.
Danach folgen ein 1:1-Unentschieden mit 13,0%, ein 2:0-Heimsieg mit 11,9% und ein 1:0-Heimsieg mit 11,7%.
Mit 5% Wahrscheinlichkeit endet das Spiel mit 1:2.