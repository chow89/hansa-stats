---
title: "Kurzanalyse 26. Spieltag: Preußen Münster gegen Hansa Rostock"
description: "Kurzanalyse zum 26. Spieltag 2019/20 zwischen Preußen Münster und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, SC Preußen Münster, Nico Neidhart, Nils Butzen, Maximilian Schulze Niehues, John Verhoek, Marco Königs, Lucas Cueto, Fridolin Wagner, Aaron Opoku, Luca Schnellbacher, Markus Kolke, Oliver Steurer]
categories: []
---

Nach dem [starken Auftritt in der Vorwoche]({% post_url 2020-02-24-analyse-3-liga-hansa-rostock-ingolstadt-251920 %}) konnte der FC Hansa seine Leistung nicht bestätigen.
Das Spiel gegen Preußen Münster am 26. Spieltag ging mit 1:0 verloren.

<canvas id="xgplot261920"></canvas>
<canvas id="winratio261920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['26'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot261920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.PREUSSEN_MÜNSTER,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio261920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

In einer insgesamt chancenarmen, aber sehr unkämpften Partie fanden zunächst die Gäste von der Ostsee besser ins Spiel.
Die erste Möglichkeit hatten die Rostocker bereits nach sechs Minuten.
Die Flanke von Nico Neidhart fand in Nils Butzen zwar einen Abnehmer, dieser konnte den Ball aber nicht mehr zielgerichtet auf das Tor bringen.
Sieben Minute später versuchte es dann Neidhart selbst.
Er nutzte die Unordnung der Münsteraner Abwehr und schloss aus zentraler Position.
Maximilian Schulze Niehues im Tor der Gastgeber konnte den Ball jedoch ohne Probleme aufnehmen.
Drei Minuten später rollte der nächste Rostocker Angriff auf das Münsteraner Tor zu, doch auch der Kopfball von John Verhoek verfehlte das Tor.
In der 25. Minute gelang es den Hausherren dann auch erstmals gefährlich vor das Gäste-Tor zu kommen.
Der Ex-Rostocker Marco Königs verzog den Schuss deutlich und der Ball ging weit am Tor vorbei.
Die restlichen 20 Minuten der Halbzeit gestalteten sich dann ausgeglichener und so kam es in der Halbzeit zu keiner weiteren Torchance mehr.

In der zweiten Spielhälfte drehte sich das Spiel.
Die Preußen waren von nun an das bessere Team und hatten in der 53. Minute ihre erste Chance nach der Halbzeit.
Lucas Cueto verfehlte das Tor mit seinem Schuss nur knapp.
Der Schuss von Fridolin Wagner bei der anschließenden Ecke ging ebenfalls am Tor vorbei.
In der 63. Minute hatten die Hanseaten in Person von Aaron Opoku ihre erste, aber auch einzige Chance der Halbzeit.
Schulze Niehues hatte mit dem Schuss jedoch keine Probleme.
Im direkten Gegenzug hatte Luca Schnellbacher nach einer verunglückten Rückgabe von Butzen die Möglichkeit zur Führung.
Markus Kolke konnte den Ball mit den Fingerspitzen noch zur Ecke klären.
Zwei Minuten später standen die Preußen bei einer Ecke erneut vor dem Rostocker Tor.
Den Kopfball von Oliver Steurer konnte Kolke noch gerade so parieren.
Der Ball sprang beim Klärungsversuch aber unglücklich an das Bein von Butzen und von dort in das Tor zur 1:0-Führung für die Gastgeber.
Die Münsteraner blieben nach dem Tor weiter aktiv, ließen die Rostocker nicht wieder zurück uns Spiel kommen und hatten in der 82. Minute noch die Chance auf 2:0 zu erhöhen.
Königs lief alleine auf Kolke zu, konnte den Ball aber nicht am Rostocker Schlussmann vorbei ins Tor schießen.
So blieb es bis zum Ende bei der knappen 1:0-Führung für die Hausherren.

Die geringe Anzahl von Torchancen spiegelt sich auch in der stochastischen Betrachtung wider.
Mit etwa 23% Wahrscheinlichkeit endet das Spiel torlos.
Jeweils rund 18% der Spiele enden mit 1:0 oder 0:1.
Entsprechend sehen auch die Gesamtwahrscheinlichkeiten für den Spielausgang aus.
29% der Spiele gewinnen die Rostock, 31% der Spiele die Münsteraner.
Die restlichen fast 40% Wahrscheinlichkeit entfallen auf ein Unentschieden.