---
title: "Kurzanalyse 16. Spieltag: 1. FC Kaiserslautern gegen Hansa Rostock"
description: "Kurzanalyse zum 16. Spieltag 2019/20 zwischen 1. FC Kaiserslautern und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, 1. FC Kaiserslautern, Nikolas Nartey, Christian Kühlwetter, Kevin Kraus, Florian Pick, Timmy Thiele]
categories: []
---

Am 16. Spieltag der Saison verlieren die Rostocker beim gastgebenden 1. FC Kaiserslautern mit 2:0.
Es war nach den Spielen in [Jena]({% post_url 2019-11-05-analyse-3-liga-carl-zeiss-jena-hansa-rostock-141920 %}) und gegen [Duisburg]({% post_url 2019-11-10-analyse-3-liga-hansa-rostock-msv-duisburg-151920 %}) die dritte Niederlage in Folge.

<canvas id="xgplot161920" ></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['16'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot161920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_KAISERSLAUTERN,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Das Spiel war von Beginn an ausgeglichen, die Rostocker kontrollierten mit deutlich höheren Ballbesitzanteilen das Spiel ohne aber offensiv in Erscheinung zu treten.
Im Gegensatz dazu konzentrierten sich die Hausherren auf das Kontern.
Bis zur verletztenbedingten Auswechslung von Nikolas Nartey in der 23. Minute ging der Spielplan der Hanseaten auf, danach fehlte in der Offensive jegliche Kreativität und der Defensive die Stabilität.
Dadurch kamen die Pfälzer auch besser und gefährlicher vor das Rostocker Gehäuse und konnten sogleich in der 24. Minute durch Christian Kühlwetter die 1:0-Führung erzielen.
Weitere zehn Minuten später hatte Kevin Kraus die nächste gute Chance, ehe Florian Pick drei Minuten vor der Pause nach einer sehenswerten Einzelaktion das 2:0 erzielte.
Kurz vor der Pause hatte nochmal Timmy Thiele die Chance auf 3:0 zu erhöhen, blieb dabei aber erfolglos.
So ging es mit der Zwei-Tore-Führung (1,6:0 xG) in die Halbzeit.

Nach dem Wiederanpfiff schalteten die Gastgeber schon einen Gang zurück, konnten sich aber immer wieder kleinere Chancen erarbeiten, ohne aber richtig gefährlich zu werden.
Die Rostocker konnten aus der defensiveren Grundausrichtung der Lauterer keinen Profit schlagen und blieben die gesamte Halbzeit blass.
Am Ende blieb es beim 2:0-Heimsieg (2,28:0,16 xG) für die Rotern Teufel.

Laut der stochastischen Betrachtung konnten die Pfälzer einen ungefährdeten Heimsieg feiern.
In 89,45% der Fällen enden solche Spiele siegreich, während 9,32% Unentschieden und 1,24% mit einem Auswärtssieg enden.
Das 2:0 war mit 25,72% auch das wahrscheinlichste Ergebnis, gefolgt vom 3:0 (20%) und 1:0 (18,83%).