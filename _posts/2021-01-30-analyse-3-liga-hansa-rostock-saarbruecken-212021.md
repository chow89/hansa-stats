---
title: "Kurzanalyse 21. Spieltag: Hansa Rostock gegen 1. FC Saarbrücken"
description: "Kurzanalyse zum 21. Spieltag 2020/21 zwischen Hansa Rostock und 1. FC Saarbrücken"
tags: [3. Liga, FC Hansa Rostock, 1. FC Saarbrücken, Tobias Jänicke, Sven Sonnenberg, Nicklas Shipnoski, Markus Kolke, Damian Roßbach, John Verhoek, Maurice Deville, Julian Riedel, Pascal Breier, Manuel Farrona Pulido, Daniel Batz, Nik Omladic, Nico Neidhart, Kianz Froese, Gian Luca Schulz]
categories: []
---

Die Hanseaten begrüßten am 21. Spieltag den 1. FC Saarbrücken im Ostseestadion.
Nach einer ereignisreichen Schlussphase gewann der FC Hansa mit 4:2.

<canvas id="xgplot212021"></canvas>
<canvas id="winratio212021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['21'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot212021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_SAARBRÜCKEN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio212021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio212021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften fanden gut in das Spiel und die Gäste gingen bereits nach sieben Minuten in Führung.
Der Ex-Rostocker Tobias Jänicke spielte einen hohen Ball an den Strafraum, den Sven Sonnenberg nicht klären konnte und so zu Nicklas Shipnoski durchrutschte.
Dieser konnte das Spielgerät ohne Probleme an Torhüter Markus Kolke vorbei ins Tor befördern.
Die Rostocker versuchten sofort den frühen Rückstand wieder auszugleichen und bekamen in der 14. Minute einen Elfmeter zugesprochen nachdem Damian Roßbach bei einer Ecke im Strafraum gefoult wurde.
John Verhoek verwandelte sicher und glich das Spiel somit wieder aus.
Danach beruhigte sich das Spiel etwas, ehe die Gäste in der 28. Minute zur nächsten Torchance kamen.
Wieder war es Shipnoski, der dieses Mal aber keinen Druck hinter den Ball bekam und Kolke den Ball so einfach aufnehmen konnte.
Sieben Minuten danach hatte Maurice Deville die nächste Möglickeit.
Doch sein artistischer Versuch mit der Hacke blieb an den Beinen von Julian Riedel hängen und trudelte ins Toraus.
Nur eine Minute danach spielten die Rostocker einen Konter über Pascal Breier, der einen Traumpass über 40 Meter in den Rücken der Abwehr und genau in die Füße von Manuel Farrona Pulido spielte.
Der Doppeltorschütze aus dem [vorangegangenen Spiel]({% post_url 2021-01-24-analyse-3-liga-msv-duisburg-hansa-rostock-202021 %}) scheiterte dann aber an der glänzenden Reaktion von Gäste-Keeper Daniel Batz.
Somit ging es dann mit dem 1:1 in die Pause.

Nach dem Wiederanpfiff gelang den Hanseaten aber dann doch noch die Führung.
In der 54. Minute rollte der nächste Konte über Breier und Farrona Pulido, der dann von links flach in den Strafraum legte, wo Björn Rother wartete und zum 2:1 verwandelte.
Ein paar Minuten später hatten die Saarländer die Möglichkeit zum Ausgleich.
Deville, der nach einem Freistoß, an der Strafraumkante an den Ball kam, traf das Spielgerät aber nicht richtig und so ging sein Schuss ein gutes Stück am Tor vorbei.
Nachdem vor dem Tor dann lange nichts mehr passierte, machten die Rostocker in der 86. Minute den Deckel drauf.
Der eingewechselte Nik Omladic spielte Nico Neidhart im Strafraum mit einem Steilpass frei.
Der Verteidiger zog direkt ab und verwandelte so zum 3:1.
Die Gäste gaben sich trotzdem noch nicht auf und hatten in der 89. Minute durch Shipnoski noch eine Möglichkeit.
Markus Kolke konnte seinen Schuss jedoch parieren.
Bei der anschließenden Ecke landete der Ball dann bei Kianz Froese, der zum 3:2 verkürzte.
In der fünften Minute der Nachspielzeit dribbelte sich Gian Luca Schulz durch den gegnerischen Strafraum und legte schließlich auf Neidhart ab, der dann sein zweites Tor zum 4:2 Endstand erzielte.
Mit dem Tor beendete der Schiedsrichter auch das Spiel.

So deutlich wie das Ergebnis ist das Spiel bei den Expected Goals nicht; ganz im Gegenteil, hier ist sogar der 1. FC Saarbrücken mit 1,29:1,68 vorne.
Das bedeutet auch, dass die Saarländer in über 47% der Fälle gewinnen und die Rostocker nur in knapp 26% der Spiele als Sieger vom Platz gehen.
Knappe 27% der Spiele enden Unentschieden.
Bei den Ergebnissen sind ein 1:2-Auswärtssieg (13,8%) und ein 1:1-Unentschieden (13,3%) am wahrscheinlichsten.
Mit 4:2 enden nur 0,6% aller Spiele.