---
title: "Kurzanalyse 29. Spieltag: Hansa Rostock gegen Waldhof Mannheim"
description: "Kurzanalyse zum 29. Spieltag 2019/20 zwischen Hansa Rostock und Eintracht Braunschweig"
tags: [3. Liga, FC Hansa Rostock, Waldhof Mannheim, Lukas Scherff, Nikolas Nartey, Michael Schultz, Mirnes Pepic, Pascal Breier, Jan Hendrik Marx, Arianit Ferati]
categories: []
---

Auch am zweiten Spieltag nach der Saison-Unterbrechung konnten die Hanseaten nicht gewinnen.
Das umkämpfte Spiel gegen Waldhof Mannheim endet mit einer 0:1-Niederlage.

<canvas id="xgplot291920"></canvas>
<canvas id="winratio291920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['29'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot291920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.WALDHOF_MANNHEIM,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio291920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Von Beginn der Partie an traten die Rostocker dominanter auf, während die Mannheimer sich zunächst auf das Defensivspiel fokussierten.
Dadurch gab es in der Anfangsphase so gut wie keine Torchance.
Erst in der 18. Minute wurde es im Mannheimer Strafraum erstmals gefährlich als Lukas Scherff eine Nartey-Ecke nur knapp am Tor vorbei schoss.
Auf der Gegenseite hatten die Gäste in der 24. Minute ebenfalls nach einer Ecke ihre erste Möglichkeit.
Michael Schultz konnte bei seinem Seitfallzieher aber nicht genug Druck hinter den Ball bringen und so konnte Markus Kolke im Rostocker Tor den Ball ohne Probleme aufnehmen.
In der 38. Minute hatte erneut Schultz die Möglichkeit zur Führung.
Dieses Mal war es ein Kopfball nach einem Freistoß, den Kolke mit einer Glanzparade zur Ecke klärte.
Da in dieser Halbzeit vor dem Tor sonst nichts weiter passierte, ging es mit einem 0:0 in die Pause.

Im zweiten Spielabschnitt hatten wieder die Hausherren die erste Torchance.
Wieder war es eine Ecke, doch auch Mirnes Pepic' Kopfball ging weit über das Tor.
Nach einem weiteren torchancenarmen Abschnitt hatte Pascal Breier die nächste Möglichkeit auf den ersten Treffer des Spiels.
Sein Schuss ging aber zu zentral auf das Tor und war für den Mannheimer Torwart keine Herausforderung.
Fünf Minuten vor Schluss wurde es nochmal vor dem Rostocker Gehäuse heiß.
Der Schiedsrichter pfiff nach einem vermeintlichen Foulspiel von Pepic an Jan Hendrik Marx Elfmeter.
Den fälligen Strafstoß verwandelte Arianit Ferati zum 0:1-Endstand.

In der stochastischen Betrachtung sieht das Spiel für die Rostocker etwas besser aus.
Von 100 dieser Spielen würden die Hanseaten 41 gewinnen und nur 21 verlieren; 38 Spiele enden Unentschieden.
Aufgrund der Chancenarmut ist ein 0:0 mit fast 24% das wahrscheinlichste Ergebnis.
Mit 23% Wahrscheinlichkeit gewinnt der FC Hansa mit 1:0.
Der 0:1-Auswärtssieg ist mit 14% Wahrscheinlichkeit das dritthäufigste Ergebnis.