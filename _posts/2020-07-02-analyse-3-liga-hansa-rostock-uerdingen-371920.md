---
title: "Kurzanalyse 37. Spieltag: Hansa Rostock gegen KFC Uerdingen"
description: "Kurzanalyse zum 37. Spieltag 2019/20 zwischen Hansa Rostock und KFC Uerdingen"
tags: [3. Liga, FC Hansa Rostock, KFC Uerdingen, Daniel Hanslik, Nico Neidhart, John Verhoek, Lukas Scherff, René Vollath, Max Reinthaler, Pascal Breier, Erik Engelhardt, Oliver Daedlow, Assani Lukimya]
categories: []
---

Am vorletzten Spieltag der Saison machten die Rostocker das Aufstiegsrennen - auch aufgrund der Patzer der Konkurrenten - nochmal spannend.
Im Spiel gegen den KFC Uerdingen gingen die Hanseaten mit einem 1:0-Sieg vom Platz.

<canvas id="xgplot371920"></canvas>
<canvas id="winratio371920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['37'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot371920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.KFC_UERDINGEN,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio371920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften begannen das Spiel vorsichtig und fokussierten sich zunächst auf die Defensive.
Nach der Anfangsviertelstunde verstärkte der FC Hansa seine Angriffsbemühungen und konnte gleich die zweite Torchance zur 1:0-Führung nutzen.
Daniel Hanslik lief sich bei einer Flanke von Nico Neidhart im Rücken der Abwehr frei und musste nur noch den Fuß hinhalten.
Wenige Minuten später hatte John Verhoek die nächste große Torchance.
Die Flanke von Lukas Scherff strich aber nur über seinen Fuß und so konnte er die Flugbahn des Balls nicht mehr entscheidend beeinflussen und der Ball ging am Tor vorbei.
Danach bekamen die Uerdinger die Rostocker Offensive wieder besser in den Griff, wodurch es im ersten Spielabschnitt keine weitere Torschüsse gab.

Im zweiten Spielabschnitt bestimmten weiterhin die Hanseaten das Spiel und hatten in der 48. Minute gleich die nächste Möglichkeit.
Erneut tauchte Verhoek ohne Gegenspieler vor dem gegnerischen Tor auf, doch der Uerdinger Torwart René Vollath konnte noch in höchster Not retten.
Fünf Minuten später hatte Max Reinthaler nach einer Ecke seinerseits die Möglichkeit um auf 2:0 zu erhöhen.
Sein Kopfball blieb aber in der Uerdinger Abwehrkette hängen.
In den Folgeminuten wurde es wieder etwas ruhiger vor den Toren, ehe der kurz zuvor eingewechselte Pascal Breier in der 67. Minute alleine auf das Gästetor zulief.
Doch auch in dieser Szene konnte Vollath retten und sein Team im Spiel halten.
In der Nachspielzeit hatte Erik Engelhardt, der ebenso wie Oliver Daedlow sein Drittligadebüt feierte, noch die Möglichkeit den Sack zu zumachen.
Der Ball rutschte ihm aber über den Spann und ging deshalb weit am Tor vorbei.
Auf der Gegenseite hatte der Ex-Hanseat Assani Lukimya nur wenige Sekunden später dann sogar die Möglichkeit zum Ausgleich.
Aber auch sein Schuss verfehlte das Tor deutlich.
Anschließend endete das Spiel.

Nach Expected Goals gewinnt der FC Hansa das Spiel mit 1,51:0.41.
Daraus ergibt sich eine Siegwahrscheinlichkeit von etwa 68% und eine Unentschiedenwahrscheinlichkeit von fast 25%. Auf einen Uerdinger Sieg fallen nur 7%.
Bei den Ergebnis ist der 1:0-Heimsieg mit über 21% das wahrscheinlichste Resultat.
Es folgen dahinter ein 2:0-Heimsieg mit fast 18% und ein 1:1-Unentschieden mit 14% Wahrscheinlichkeit.