---
title: "Vorhersage der Abschlusstabelle"
description: "Vorhersage der Abschlusstabelle 3. Liga 2019/2020 nach der Corona-Pause"
tags: [3. Liga]
categories: []
---

Nach mehr als zwei Monaten Corona-Pause geht morgen die 3. Liga endlich wieder los.
Während die halbe Liga noch um den Aufstieg spielt, geht es für die Teams in der unteren Hälfte der Tabelle vorallem um den Klassenerhalt.
Doch wer hat im Kampf um den Aufstieg und den Klassenerhalt die besten Karten?

<canvas id="prediction1920"></canvas>
<script>
    var data = {{ site.data.fixtures['1920'] | jsonify }};
    var keys = Object.keys(data);
    var matches = keys.flatMap((key) => data[key].map((match) => ({...match, matchday: parseInt(key)}))).filter(e => e.matchday <= 27);
    new HansaStatsChart.SeasonPlot('#prediction1920', matches);
</script>

Der Graph zeigt eine Vorhersage für den restlichen Saisonverlauf. Auf der x-Achse sind die Spieltage dargestellt, auf der y-Achse die Punkte pro Team.
Über die Legende lassen sich die Teams ein- und ausblenden.
Für die ersten 27 Spieltage werden die Punkte nach dem jeweiligen Spieltag genutzt.
Ab dem 28. Spieltag werden zur Vorhersage der Durchschnitt der Expected Points der bisherigen Spiele und die einfache Standardabweichung herangezogen.
Hat zum Beispiel ein Team nach 27 Spieltagen 40 Punkte und erzielt pro Spiel durchschnittlich 1,5 Expected Points bei einer Standardabweichung von 0,25 Punkten, dann hat das Team nach dem 28. Spieltag im besten Fall 41,75 Punkte und im schlechtesten Fall 41,25 Punkte; nach dem 29. Spieltag entsprechend 43,5 Punkte beziehungsweise 42,5 Punkte.
Wenn alle restlichen Spiele nach dieser Methode durchgerechnet werden, dann ergibt der Durchschnitt des Worst Case und Best Case die folgende Tabelle:

| Platz | Verein | Punkte |
|-------|--------|--------|
| 1 | MSV Duisburg | 62,45 |
| 2 | 1860 München | 58,90 |
| 3 | SV Meppen | 58,66 |
| 4 | Waldhof Mannheim | 58,65 |
| 5 | FC Ingolstadt | 56,78 |
| 6 | Bayern München II | 56,77 |
| 7 | SpVgg Unterhaching | 55,67 |
| 8 | Würzburger Kickers | 55,24 |
| 9 | Hansa Rostock | 54,34 |
| 10 | Eintracht Braunschweig | 53,33 |
| 11 | KFC Uerdingen | 50,72 |
| 12 | 1. FC Kaiserslautern | 50,35 |
| 13 | 1. FC Magdeburg | 49,74 |
| 14 | Chemnitzer FC | 48,28 |
| 15 | Hallescher FC | 46,59 |
| 16 | Viktoria Köln | 45,28 |
| 17 | FSV Zwickau | 44,36 |
| 18 | Preußen Münster | 38,72 |
| 19 | Sonnenhof Großaspach | 32,52 |
| 20 | Carl Zeiss Jena | 27,67 |

Demnach werden der MSV Duisburg und 1860 München direkt aufsteigen und der SV Meppen in die Relegation gehen.
Den Weg in die Regionalliga werden der FSV Zwickau, Preußen Münster, Sonnenhof Großaspach und Carl Zeiss Jena beschreiten müssen.

Eine nach jedem Spieltag aktualisierte Version des obigen Graphen ist [hier]({% link pages/prediction-1920.md %}) zu finden.