---
title: "Kurzanalyse 27. Spieltag: Hansa Rostock gegen 1. FC Kaiserslautern"
description: "Kurzanalyse zum 27. Spieltag 2020/21 zwischen Hansa Rostock und 1. FC Kaiserslautern"
tags: [3. Liga, FC Hansa Rostock, 1. FC Kaiserslautern, Damian Roßbach, Avdo Spahic, Kevin Kraus, Marlon Ritter, Elias Huth, Kenny Prince Redondo, Simon Rhein, Bentley Baxter Bahn, Nik Omladic, Tobias Schwede, Markus Kolke, Anas Bakhat, Anil Gözütok]
categories: []
---

Nach der [Niederlage in Lübeck]({% post_url 2021-03-06-analyse-3-liga-luebeck-hansa-rostock-192021 %}) begrüßte der FC Hansa am 27. Spieltag den 1. FC Kaiserslautern.
Das sehr umkämpfte Spiel gewannen die Rostocker schlussendlich etwas glücklich mit 2:1.

<canvas id="xgplot272021"></canvas>
<canvas id="winratio272021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['27'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot272021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_KAISERSLAUTERN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio272021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio272021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Partie begann sehr zerfahren mit vielen kleinen Fehlern im Spielaufbau auf beiden Seiten.
Es dauert bis zur 23. Minute ehe die Hanseaten die erste Torchance des gesamten Spiels hatten.
Damian Roßbach stand bei einer Flanke von der linken Seite goldrichtig, köpfte dann aber zu zentral auf das Tor und so war der Ball für den Lauterer Torwart Avdo Spahic kein Problem.
Nur zwei Minuten später wurde es dann auf der Gegenseite das erste Mal gefährlich.
Bei einem Freistoß landete der Ball auf dem Kopf von Kevin Kraus, der dann zum 0:1 einnetzen konnte.
In der 32. Minute hatten die Gäste dann gleich die Chance zum Erhöhen.
Nach einer Passstafette im Strafraum stand auf einmal Marlon Ritter frei, sein Schuss konnte aber in letzter Sekunde geblockt werden.
In der 40. Minuten wurde es wieder vor dem Rostocker Tor gefährlich.
Eine Ecke landete bei Elias Huth, dessen Kopfball aber nur an den Pfosten ging und anschließend geklärt wurde.
Zwei Minuten später tauchten die Lauterer erneut im Rostocker Strafraum auf.
Dieses Mal war es Kenny Prince Redondo, der seinen Kopfball aber nicht auf das Tor bringen konnte.
Anschließend pfiff der Schiedsrichter zur Halbzeit.

Nach der Pause fanden die Hanseaten dann besser in das Spiel und konnte gleich in der 47. Minute den Ausgleich erzielen.
Über Simon Rhein und Bentley Baxter Bahn landete der Ball bei Nik Omladic, der dann zum 1:1 traf.
Anschließend blieb es in beiden Strafräumen lange ruhig, ehe es in der 73. Minuten auf beiden Seiten Torchancen gab.
Zunächst hatten die Hausherren durch Tobias Schwede die Riesenchance zur Führung.
Spahic konnten seinen Schuss aus kurzer Distanz mit einer Glanztat aber noch klären.
Der direkt darauffolgende Konter landete schließlich bei Redondo, der frei auf das Tor der Rostocker zulief, dann aber am herausstürmenden Markus Kolke scheiterte.
Acht Minuten später kamen die Gäste zu ihren nächsten guten Torchance.
Wieder ging der Torchance ein Konter voran, der bei Anas Bakhat landete.
Kolke konnte seine Schuss aber entschärfen.
Nur wenige Sekunden später tauchten die Pfälzer erneut vor dem Rostocker Tor auf.
Dieses Mal war es Anil Gözütok, der es versuchte.
Sein Schuss verfehlte das Gehäuse aber knapp.
In der sechsten Minute der Nachspielzeit bekamen die Rostocker dann nochmal einen Freistoß zugesprochen.
Rhein fand dabei den Kopf von Roßbach, der dann zum viel umjubelten 2:1 traf.
Direkt danach beendete der Unparteiische das Spiel.

Nach Expected Goals endet das Spiel mit 1,41:1,90 knapp für die Gäste.
Die Hanseaten gewinnen somit nur etwas über ein Viertel der Spiele.
Ein weiteres Viertel der Spiele endet Unentschieden.
Die Lauterer gewinnen somit circa jedes zweites Spiel.
Ein 1:2-Auswärtssieg ist mit 13,1% das wahrscheinlichste Ergebnis, knapp dahinter liegt ein 1:1-Unentschieden mit 12,1%.
Die Wahrscheinlichkeit für ein 2:1-Heimsieg liegt bei 9%.