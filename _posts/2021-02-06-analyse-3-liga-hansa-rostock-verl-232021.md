---
title: "Kurzanalyse 23. Spieltag: Hansa Rostock gegen SC Verl"
description: "Kurzanalyse zum 23. Spieltag 2020/21 zwischen Hansa Rostock und SC Verl"
tags: [3. Liga, FC Hansa Rostock, SC Verl, Sven Sonnenberg, Zlatko Janjic, Markus Kolke, John Verhoek, Lukas Scherff, Lion Lauberbach, Nico Neidhart, Philipp Sander, Patrick Schikowski, Simon Rhein, Nik Omladic, Leandro Putaro, Tobias Schwede, Philip Türpitz]
categories: []
---

Der SC Verl gastierte am 23. Spieltag im Ostssestadion beim FC Hansa Rostock.
In einer spielstarken und umkämpfte Partie setzte sich schlussendlich der FC Hansa mit 3:2 durch.

<canvas id="xgplot232021"></canvas>
<canvas id="winratio232021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['23'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot232021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SC_VERL,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio232021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio232021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften fanden gut in das Spiel und agierten offensiv.
Die erste Chance erspielten sich dann die Verler in der vierten Minute, die nach schnellem Umschaltspiel im Rostocker Strafraum auftauchten.
Sven Sonnenberg konnte den Vorstoß nur mit Foul unterbinden und so gab es früh in der Partie Elfmeter für den Aufsteiger.
Zlatko Janjic ließ sich die Chance nicht nehmen und verwandelte zum 0:1.
Nur zwei Minuten danach tauchte Janjic erneut vor dem Rostocker Tor auf, doch dieses Mal konnte Torhüter Markus Kolke mit einer Glanzparade schlimmeres verhindern.
Es dauerte bis zur 22. Minute bis die Rostocker zu ihrer ersten großen Torchance kamen.
Nach einer Ecke und viel Herumgestocher im Strafraum kam John Verhoek zum Abschluss.
Sein Schuss konnte jedoch geklärt werden. Der Ball landete dann bei Lukas Scherff, der das Spielgrät wieder in den Strafraum brachte und mit Lion Lauberbach einen Abnehmer fand.
Dessen Kopfball landete dann aber nur am Pfosten.
In der 31. Minute kamen die Hanseaten erneut gefährlich vor das Tor.
Eine Flanke von Lauberbach landete nur wenige Meter vor dem Tor bei Nico Neidhart, der dann aber das leere Tor nicht traf.
Drei Minute später wurde es dann wieder auf der Gegenseite gefährlich.
Wieder hatte Janjic die Möglichkeit, doch sein Schuss wurde abgefälscht und war kein Problem für Kolke.
In der 38. Minute waren die Gäste erfolgreicher.
Den ersten Schuss von Philipp Sander konnte Kolke noch parieren, der Nachschuss von Patrick Schikowski landete dann aber zum 0:2 in den Maschen.
Mit diesem Zwischenstand gingen beide Teams wenig später auch in die Halbzeitpause.

Nach der Pause und einem Vierfachwechsel bei den Rostockern wurden die Hausherren besser und erdrückten die Gästen phasenweise in der eigenen Hälfte.
Den Auftakt machte Verhoek, der bei einer Flanke von Simon Rhein in der 55. Minute am höchsten stieg und so gleich den 1:2-Anschlusstreffer erzielte.
Nur vier Minuten danach zappelte der Ball erneut im Netz.
Dieses Mal war es Nik Omladic der einfach mal aus über 20 Metern abzog und den Verler Torwart so überraschte.
Nach dem Ausgleich schalteten die Gastgeber wieder ein Stück zurück und ließen die Verler wieder mehr am Spiel partizipieren.
Diese bekamen in der 63. Minute ihre nächste Torchance, als sie sich gut durch die Rostocker Verteidigung kombinierten.
Der Winkel für Schikowski wurde dann aber zu spitz und so ging sein Schuss knapp am Tor vorbei.
In der Schlussviertelstunde wurden die Rostocker wieder aktiver und versuchten das Spiel komplett zu drehen.
So hatte Verhoek in der 78. Minute die Chance zu seinem zweiten Tor des Abend.
Sein Kopfball nach einer Ecke wurde aber abgefälscht und ging am Gehäuse vorbei.
Vier Minuten vor dem Ende der regulären Spielzeit kam der Niederländer wieder bei einer Ecke mit dem Kopf an den Ball.
Bei diesem Versuch bekam der Stürmer aber nicht genügend Druck auf den Ball und so endete das Spielgerät in den Händen des Verler Torwarts.
In der ersten Minute der Nachspielzeit gelang den Verler dann sogar fast der Lucky Punch, als Leandro Putaro mit einem Kopfball aus kurzer Distanz nochmal Kolke testen wollte.
Der Rostocker Keeper konnte den Ball unter Mithilfe von Tobias Schwede aber klären.
In der vierten Minute der Nachspielzeit machte dann aber Philip Türpitz den Deckel für die Rostocker drauf.
Sein Schuss von der Strafraumkante passte ganz genau in die linke untere Ecke des Tores und erhöhte damit auf 3:2.
Kurz darauf pfiff der Schiedsrichter das Spiel ab.

Die Hanseaten gewinnen das Spiel nach Expected Goals mit 1,99:1,26.
Daraus ergibt sich eine Gewinnwahrscheinlichkeit von über 55% und einer Unentschiedenwahrscheinlichkeit von 23%.
Die restlichen knapp 22% enfallen auf einen Sieg der Verler.
Das wahrscheinlichste Ergebnis bei einem solchen Spiel ist demnach ein 2:1-Heimsieg (12,2%), gefolgt von einem 1:1-Unentschieden (10,8%).
Mit 3:2 enden nur 5,1% der Spiele.