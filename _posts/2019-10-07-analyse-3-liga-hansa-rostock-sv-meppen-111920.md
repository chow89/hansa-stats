---
title: "Kurzanalyse 11. Spieltag: Hansa Rostock gegen SV Meppen"
description: "Kurzanalyse zum 11. Spieltag 2019/20 zwischen Hansa Rostock und SV Meppen"
tags: [3. Liga, FC Hansa Rostock, SV Meppen, Pascal Breier, Aaron Opoku, Deniz Undav, Markus Kolke, Erik Domaschke, Nikolas Nartey, Sven Sonnenberg, René Guder]
categories: []
---

Nach zwei Unentschieden in Folge kehren die Hanseaten in die Erfolgsspur zurück.
Im Heimspiel gegen den SV Meppen konnte sich die Mannschaft von Trainer Jens Härtel mit 2:1 durchsetzen.

<canvas id="xgplot111920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['11'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot111920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SV_MEPPEN,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Rostocker begannen die erste Halbzeit wie gewohnt offensiv und versuchten mit einer hohen Pressinglinie die Meppener Abwehr früh unter Druck zu setzen.
Und so gelang es Pascal Breier bereits in der 9. Minute das 1:0 mit einem Distanzschuss zu erzielen.
Die Rostocker blieben danach weiter aktiv und konnten gleich mit der nächsten Torchance das 2:0 nachlegen.
Wieder war es Breier, der in der 26. Minute den Treffer erzielte.
Danach verflachte das Spiel und beide Mannschaften neutralisierten sich, einzig Aaron Opoku hatte kurz vor dem Halbzeitpfiff noch einmal die große Chance auf 3:0 zu erhöhen, scheiterte aber am Meppener Keeper Erik Domaschke.
Zur Pause stand es somit 2:0 bei einem xG-Verhältnis von 0,42:0.

Mit Beginn der zweiten Halbzeit änderte sich das Spiel. Fortan waren die Meppener die aktivere Mannschaften und die Hanseaten konzentrierten sich auf das Kontern.
Bereits in der 47. Minute hatte Deniz Undav die Chance auf den Anschlusstreffer, scheiterte jedoch an Keeper Markus Kolke.
Nur zwei Minuten später war es erneut Undav, der vor dem Rostocker Gehäuse an den Ball kam.
Nach einer kurzen Ruhephase hatte der FC Hansa in der 57. Minute erneut die Chance zum Vorentscheid, Nikolas Nartey scheiterte aber frei vor dem gut aufgelegten Domaschke.
Zehn Minuten vor Schluss hatte nochmal Sven Sonnenberg nach einer Ecke die Möglichkeit den Sack zu zumachen, sein Gegenspieler hinderte ihn aber an einem platzierten Kopfball.
Im Gegenzug nutzten die Meppener die hochstehende Rostocker Abwehr ihrerseits und verkürzten durch René Guder auf 2:1.
In den letzten Minuten warfen die Niedersachsen nochmal alles nach vorne um den Ausgleich zu erzielen, scheiterten aber an der im ganzen Spiel sehr gut verteidigenden Abwehr der Rostocker.
So blieb es schließlich beim 2:1 für die Ostseestädter und einem xG-Verhältnis von 1,13:1,58.

Interessant ist der Blick auf das Spiel aus stochastischer Sicht.
Demnach gewinnen die Meppener in fast der Hälfte der Fälle das Spiel (49,58%), während Hansa in nur 21,6% der Fällen gewinnt.
Die restlichen 28,81% der Spiele enden Unentschieden.
Darausfolgend sind die wahrscheinlichsten Ergebnisse 1:1 (18,23%), 1:2 (14,25%), 0:1 (13,12%), 2:1 (10,45%) und 0:2 (10,26%).