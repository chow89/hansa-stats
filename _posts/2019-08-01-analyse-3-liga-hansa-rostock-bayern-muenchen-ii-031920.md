---
title: "Kurzanalyse 3. Spieltag: Hansa Rostock gegen Bayern München II"
description: "Kurzanalyse zum 3. Spieltag 2019/20 zwischen Hansa Rostock und Bayern München II"
tags: [3. Liga, FC Hansa Rostock, FC Bayern München II, Pascal Breier, Marco Königs, Kwasi Okyere Wriedt, Oliver Batista Meier, Marcus Kolke, Adam Straith, Korbinian Vollmann, Josip Stanisic]
categories: []
---

Drei Punkte am dritten Spieltag: In einem intensiven und kampfbetonten Spiel konnten sich die Rostocker mit 2:1 gegen die zweite Mannschaft vom FC Bayern durchsetzen.

<canvas id="xgplot031920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['3'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot031920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.BAYERN_MÜNCHEN_II,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

In der ersten Halbzeit konnten sich beide Mannschaften mehrere gute Torchancen rausspielen.
Für die Hanseaten sind hier vor allem Pascal Breier in der zweiten Minute und Marco Königs (18. Minute) zu nennen, die beide in aussichtsreicher Position vor dem bayerischen Tor an den Ball kamen, ihre Chancen aber nicht nutzen konnten.
In der 41. Minute machte es Breier dann besser, als er aus wenig aussichtsreicher Position zum 1:0 traf.
Auf der anderen Seite waren es Kwasi Okyere Wriedt (13. und 31. Minute) und Oliver Batista Meier (37. Minute), die ihre Chancen ungenutzt ließen.
Besonders hervorzuheben sind hier die Parade von Markus Kolke aus kürzerster Distanz bei Wriedts zweiter Chance und Adam Straith, der bei der Chance von Batista Meier erst auf der Linie für den bereits geschlagenen Kolke rettete.
So stand am am Ende der ersten Hälfte nach xG 0,66:0,54.

Die zweite Halbzeit begann dann mit offensiv starken Rostockern, die versuchten schnell die Führung auszubauen.
So war es vor allem Korbinian Vollmann, der zwischen der 50. und 60. Minute drei Chancen hatte, wovon schon die erste von Erfolg gekrönt war.
In den letzten 30 Minuten konnten sich beide Mannschaften keine großen Torchancen mehr herausarbeiten.
Einizig die Bayern kamen in der 84. Minute nochmal zu zwei kleinen Chancen.
Dabei gelang den Münchenern nach einem abgefälschten Schuss von Josip Stanisic der 2:1-Anschlusstreffer, der gleichzeitig auch den Endstand markierte.
Am Ende des Spiels kamen die beiden Teams auf 1,15:0,73 xG, so dass der Sieg unterm Strich in Ordnung geht.
