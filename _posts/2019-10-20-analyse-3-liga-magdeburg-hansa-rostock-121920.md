---
title: "Kurzanalyse 12. Spieltag: FC Magdeburg gegen Hansa Rostock"
description: "Kurzanalyse zum 12. Spieltag 2019/20 zwischen FC Magdeburg und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, FC Magdeburg, Jens Härtel, Thore Jacobsen, Jürgen Gjasula, Markus Kolke, Nik Omladic, Aaron Opoku, Sören Bertram, Christian Beck]
categories: []
---

Am 12. Spieltag der Saison ging es für die Rostocker zum Zweitligaabsteiger FC Magdeburg.
Bei der Rückkehr an Jens Härtels ehemalige Wirkungsstätte gelang dem FC Hansa ein glücklicher 1:0-Sieg.

<canvas id="xgplot121920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['12'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot121920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_MAGDEBURG,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Magdeburger versuchten ab der ersten Minute das Spiel zu dominieren und kamen damit auch schnell zu einigen Torschussmöglichkeiten.
Am aussichtsreichsten war dabei ein Kopfball von Thore Jacobsen, der nach einem Freistoß freistehend zum Abschluss kam.
In den nächsten Minuten stabilisierte sich die Abwehr der Hanseaten, so dass die Sachsen-Anhalter mehr Mühe hatten vor das Gästetor zu kommen.
So war es erst ein Freistoß von Jürgen Gjasula in der 30. Minute, der wieder gefährlich auf das Tor kam.
Aber auch diesen Schuss konnte der bestens aufgelegte Markus Kolke halten.
Im Gegenzug kam der FC Hansa dann auch erstmals gefährlich vor das gegnerische Tor und konnte sogleich das 1:0 erzielen.
Nik Omladic nutzte dafür einen sehenswerten Pass von Aaron Opoku und legte den Ball am Torwart der Hausherren vorbei ins Netz.
Bis zur Halbzeit fanden die Magdeburger nicht wieder zu ihrem Spiel zurück und so ging es mit dem einen Tor in die Halbzeit.
Nach expected Goals stand es zu diesem Zeitpunkt 0,22:0,46.

Nach der Pause waren zunächst die Hanseaten die etwas bessere Mannschaft; sie konnten ihre wenige Chancen aber nicht zur Vorentscheidung nutzen.
Nach einigen Minuten übernahmen die Gastgeber wieder die Spielkontrolle, allerdings genügte das nur für eine Schusschance von Sören Bertram, die über das Tor ging, ehe bei beiden Mannschaften aufgrund der Wechsel der Spielfluss etwas verloren ging.
So kamen die Magdeburger erst wieder in der 78. Minute durch Christian Beck zu einer nennenswerten Chance, die ebenfalls von Kolke parierte wurde.
Das war auch die letzte Möglichkeit der Hausherren, da sich die Rostocker zu dieser Zeit nur noch auf die Defensive fokussierten und gelegentlich Entlastungsangriffe forcierten.
Am Ende blieb es beim 1:0-Sieg für die Gäste und einem xG-Verhältnis von 0,43:0:76.

Aufgrund der wenigen Torchancen war ein 0:0 in diesem Spiel das wahrscheinlichste Ergebnis (28,82%), gefolgt von einem 1:0-Heimsieg (24,07%).
Nur 13,46% aller Spiele enden bei so einem Verlauf mit einem 1:0-Auswärtssieg.
Insgesamt ist der Auswärtssieg mit nur 18,72% auch der unwahrscheinlichste Spielausgang, dagegen sind ein Unentschieden (40,86%) und ein Heimsieg (40,42%) sehr wahrscheinlich gewesen.