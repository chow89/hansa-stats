---
title: "Kurzanalyse 1. Runde DFB-Pokal: Hansa Rostock gegen VfB Stuttgart"
description: "Kurzanalyse zur 1. DFB-Pokalrunde 2019/20 zwischen Hansa Rostock und VfB Stuttgart"
tags: [Korbinian Vollmann, Marco Königs, Maximilian Ahlschwede, Hamadi Al Ghaddioui, John Verhoek, Santiago Ascacibar, FC Hansa Rostock, VfB Stuttgart, DFB-Pokal]
categories: []
---

Geschichte wiederholt sich doch nicht.
In der ersten Runde des DFB-Pokals konnten die Rostocker die Überraschung aus der letzten Saison nicht wiederholen und verloren nach einem umgekämpften Spiel mit 0:1.

<canvas id="xgplotcup011920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].cup['1'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplotcup011920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: "VfB Stuttgart",
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Der FC Hansa kam gut ins Spiel und erarbeitet sich in der Anfangsviertelstunde durch Korbinian Vollmann, Marco Königs und Maximilian Ahlschwede mehrere kleine Chancen.
In der Folge stabilisierten sich die Stuttgarter und kamen nach einer schnell ausgeführten Ecke selbst zur ihrer ersten Chance durch Hamadi Al Ghaddioui, die direkt im Rostocker Gehäuse landete.
Im weiteren Verlauf des ersten Spielabschnittes neutralisierten sich beide Mannschaften.
Einzig die Stuttgarter kamen in Person von Mario Gomez in der 32. Minute nochmal vor das Tor ohne aber eine große Torgefahr auszustrahlen.
So ging es dann mit dem 0:1 und einem xG-Verhältnis von 0,36:0,56 in die Halbzeit.

Die zweite Spielhälfte begann mit etwas präsenteren Stuttgartern, die sich aber schwer taten den Ball gefährlich vor das Tor von Markus Kolke zu bringen.
Erst in der 59. Minute gab es durch Santiago Ascacibar eine gute Torchance, die der Argentinier jedoch aus kurzer Distanz über das Tor schoss.
Direkt im Gegenzug war es John Verhoek, der auf der Gegenseite aus ähnlich kurzer Distanz einen Kopfball neben das Tor setzte.
Danach verflachte das Spiel wieder etwas, bevor die Hanseaten fünf Minuten vor dem Schluss nochmal aufdrehten und alles nach vorne warfen.
Dabei kamen nochmal einige gute Schusspositionen zu Stande, die aber alle von den Stuttgartern Defensivspieler geblockt werden konnten.
So blieb es am Ende beim knappen 0:1 für den VfB. Hansa hätte sich jedoch aufgrund der Bemühungen das Unentschieden und damit die Verlängerung verdient gehabt.
Unterm Strich endete das Spiel mit einem xG-Verhältnis von 1,7:0,9.
