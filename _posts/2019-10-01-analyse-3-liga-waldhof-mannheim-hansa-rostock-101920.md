---
title: "Kurzanalyse 10. Spieltag: Waldhof Mannheim gegen Hansa Rostock"
description: "Kurzanalyse zum 10. Spieltag 2019/20 zwischen Waldhof Mannheim und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Waldhof Mannheim, Michael Schultz, Marcel Seegert, Korbinian Vollmann, Kevin Koffi, Dorian Diring, Markus Kolke, Arianit Ferati, Kai Bülow, Pascal Breier, Max Christiansen]
categories: []
---

Am 10. Spieltag ging es für die seit vier Spieltagen ungeschlagenen Rostocker zum ersten Mal seit 24 Jahren nach Mannheim. Das Spiel endete wie schon [vor einer Woche gegen Zwickau]({% post_url 2019-09-24-analyse-3-liga-hansa-rostock-fsv-zwickau-091920 %}) mit einem 1:1-Unentschieden.

<canvas id="xgplot101920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['10'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot101920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.WALDHOF_MANNHEIM,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften taten sich zu Beginn des Spiels schwer und konnten sich offensiv nur selten in Szene setzen.
Die Mannheimer konnten dabei zumindest zweimal nach einer Ecke für etwas Gefahr sorgen (Michael Schultz (7. Min.), Marcel Seegert (19. Min)), der Torerfolg blieb dabei aber aus.
Wenige Minuten später kamen auch die Rostocker erstmals zu einer Torchance, die Korbinian Vollmann aber auch nicht verwerten konnte.
In der 28. Minute hatten die Badener bei einer Dreifach-Chance das 1:0 auf dem Fuß, aber erst scheiterte Kevin Koffi am Pfosten, dann Dorian Diring an Markus Kolke und schließlich Arianit Ferati an Kai Bülow, der den Schuss blockte.
Nur fünf Minute später hatte erneut Diring eine Doppelchance, die Kolke ebenfalls glänzend parierte.
Vor der Halbzeit kam der FC Hansa nochmal mit einem Konter zu einer Chance, die Pascal Breier aber nicht nutzen konnten.
So gingen die beiden Teams mit 0:0 und 0,45:1,38 xG in die Pause.

Zu Beginn der zweiten Halbzeit zeigten die Rostocker ein anderes Gesicht und sorgten von Anfang an für Gefahr.
Gleich mit der ersten Möglichkeit in der 48. Minute konnten die Rostocker durch Pascal Breier den Führungstreffer zum 1:0 erzielen.
In den nächsten Minuten kamen die Mannheimer wieder besser ins Spiel und bauten zunehmend Druck auf, so dass der Ex-Rostocker Max Christiansen in der 60. Minute den 1:1-Ausgleich erzielen konnte.
In der letzen halben Stunde konnten beide Teams keine nennenswerten Chancen mehr erarbeiten und so blieb es beim Unentschieden und 0,81:1,61 xG.

Stochastisch gesehen ist das 1:1 der wahrscheinlichste Spielausgang (13,93%) dicht gefolgt von einem 1:0-Sieg für die Mannheimer (13,48%).
Danach folgen ein 2:1- und 2:0-Sieg für die Gastgeber mit 12,17% bzw. 11,78%.
Insgesamt ist ein Heimsieg mit 57,57% auch das erwartbarste Ergebnis.
In nur 16,9% der Fällen endet das Spiel mit einem Auswärtssieg.
Die restlichen 25,52% der Spiele enden mit einem Unentschieden.