---
title: "Kurzanalyse 25. Spieltag: Hansa Rostock gegen Waldhof Mannheim"
description: "Kurzanalyse zum 25. Spieltag 2020/21 zwischen Hansa Rostock und Waldhof Mannheim"
tags: [3. Liga, FC Hansa Rostock, Waldhof Mannheim, Lukas Scherff, Pascal Breier, Nik Omladic, Nico Neidhart, Simon Rhein, Bentley Baxter Bahn, Lion Lauberbach, Marcel Seegert]
categories: []
---

Die Hanseaten begrüßten am 25. Spieltag den SV Waldof Mannheim.
Nach einem ereignis- und torchancenarmen Spiel gewannen die Rostocker mit 1:0.

<canvas id="xgplot252021"></canvas>
<canvas id="winratio252021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['25'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot252021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.WALDHOF_MANNHEIM,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio252021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio252021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften begegneten sich von Beginn an auf Augenhöhe und ließen die gegnerischen Angreifer nur selten in den eigenen Strafraum.
So dauert es bis zur 22. Minute ehe die Hanseaten zum ersten Mal auf das Tor schossen und so gleich das 1:0 erzielten.
Nach einem Einwurf von Lukas Scherff, verlängerten Pascal Breier und Nik Omladic den Ball zu Nico Neidhart, der dann von der Strafraumgrenze abzog und so sehenswert das Tor traf.
In der restlichen Zeit der ersten Halbzeit ereigneten sich keine weiteren nennenswerten Torchancen und so gingen beide Mannschaften mit diesem Zwischenstand wieder in die Kabine.

Nach der Pause setzte sich das Spiel unverändert fort.
Dieses Mal dauerte es bis zur 75. Minute als die Rostocker erneut vielversprechend vor das Tor der Mannheimer kamen.
Ein Heber von Simon Rhein wurde von Bentley Baxter Bahn zu Lion Lauberbach verlängert.
Der Stürmer ließ dann den gegnerischen Torwart aussteigen, scheiterte dann aber an Marcel Seegert, der auf der Linie klären konnte.
So blieb es bis zum Schluss bei der knappen 1:0-Führung.

Nach Expected Goals gewinnen die Rostocker mit 0,52:0,06.
Damit gewinnen die Hausherren in rund 45% der Spiele, während das Spiel in 51% der Fälle unentschieden endet.
Die restlichen fast 4% entfallen auf einen Mannheimer Sieg.
Die wahrscheinlichsten Ergebnisse sind ein 0:0-Unentschieden (48,3%) und ein 1:0-Heimsieg (41,2%).