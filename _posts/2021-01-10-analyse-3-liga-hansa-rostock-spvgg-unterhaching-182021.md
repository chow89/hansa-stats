---
title: "Kurzanalyse 18. Spieltag: Hansa Rostock gegen SpVgg Unterhaching"
description: "Kurzanalyse zum 18. Spieltag 2020/21 zwischen Hansa Rostock und SpVgg Unterhaching"
tags: [3. Liga, FC Hansa Rostock, SpVgg Unterhaching, Patrick Hasenhüttl, John Verhoek, Nik Omladic, Sven Sonnenberg, Nico Neidhart, Moritz Heinrich, Max Dombrowka, Markus Schwabl, Oliver Daedlow, Simon Rhein, Bentley Baxter Bahn]
categories: []
---

Im ersten Spiel des neuen Jahres begrüßten die Rostocker die Spielvereinigung Unterhaching im heimischen Ostseestadion.
Das umkämpfte aber chancenarme Spiel endete mit einem 1:0-Heimerfolg.

<canvas id="xgplot182021"></canvas>
<canvas id="winratio182021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['18'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot182021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SPVGG_UNTERHACHING,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio182021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio182021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Nach dem Anpfiff waren zunächst die Hanseaten das bessere Team, jedoch ohne vor dem Tor der Gäste gefährlich zu werden.
Rund eine Viertelstunde nach Spielbeginn fanden auch die Hachinger besser in die Partie und so entwickelte sich ein ausgeglichenes Spiel.
In der 33. Minute wurde es dann erstamls gefährlich vor dem Tor.
Es waren die Bayern, die durch Patrick Hasenhüttl in aussichtsreicher Position an den Ball kamen.
Sein Schuss war aber viel zu unplatziert und landete im Ballfangnetz hinter dem Tor.
Wenige Sekunden vor dem Halbzeitpfiff gab es auch auf der Gegenseite die erste Möglichkeit.
John Verhoeks Schuss von der Strafraumkante verfehlte das Tor jedoch knapp.
Anschließend durften sich beide Mannschaften in der Kabine erstmal wieder aufwärmen.

Nach der Pause ging es genauso chancenarm weiter, nur dass die erste Chance des Spielabschnittes nicht so lange auf sich warten ließ.
Nach einer Flanke von Nik Omladic in der 54. Minute kommt Sven Sonnenberg als Erster an den Ball.
Sein Kopfstoß flog aber deutlich am Tor vorbei.
Nur eine Minute später machte es Verhoek besser.
Eine Hereingabe von Nico Neidhart landete auf seinem Kopf, die er dann zur 1:0-Führung verwerten konnte.
Mit der Führung im Rücken zogen sich die Rostocker fast gänzlich zurück und lauerten auf Konter.
Dadurch fanden auch die Hachinger besser ins Spiel und hatten in der 61. Minute die nächste gute Möglichkeit.
Nach einem Einwurf kam Moritz Heinrich im Strafraum sofort zum Abschluss; der Ball rutschte ihm aber über den Spann und landete fast im Seitenaus.
Zwei Minuten vor Schluss bekamen die Bayern noch einen Eckstoß zugesprochen, der drei Meter vor dem Tor bei Max Dombrowka ankam.
Der Verteidiger, der mit dem Rücken zum Tor stand, war aber zu überrascht und schoss den Ball ein gutes Stück über das Tor.
Nur eine Minute später hatten die Gäste bei einem Freistoß die nächste Möglichkeit zum Ausgleich.
Der freistehende Markus Schwabl schoss aber nur Oliver Daedlow an, der den Ball so zur Ecke klären konnte.
In der Nachspielzeit konnten die Hanseaten noch einmal vor das Gästetor kommen.
Winterneuzugang Simon Rhein konnte die Flanke von Bentley Baxter Bahn aber nicht auf das Tor köpfen und so blieb es bis zum Abpfiff beim 1:0.

Durch die geringe Anzahl an Torchancen endet das Spiel nach Expected Goals mit 0,74:0,94.
Das bedeutet, dass die Rostocker in etwas 25% der Spiele gewinnen und in 35% der Spiele unentschieden spielen.
Die restlichen knapp 40% der Spiele enden mit einem Sieg für Unterhaching.
Die wahrscheinlichsten Ergebnisse sind demnach ein 0:1-Auswärtssieg (21,2%) und ein 1:1-Unenschieden (19,0%).
Mit 12,3% Wahrscheinlichkeit gewinnen die Rostocker 1:0.