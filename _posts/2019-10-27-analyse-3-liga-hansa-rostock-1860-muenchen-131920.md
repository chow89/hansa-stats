---
title: "Kurzanalyse 13. Spieltag: Hansa Rostock gegen 1860 München"
description: "Kurzanalyse zum 13. Spieltag 2019/20 zwischen Hansa Rostock und 1860 München"
tags: [3. Liga, FC Hansa Rostock, 1860 München, Maximilian Ahlschwede, Nik Omladic, Aaron Opoku, Sascha Mölders, Marius Willisch, Markus Kolke, Korbinian Vollmann]
categories: []
---

Am 13. Spieltag der Saison kamen die Löwen aus München an die Ostseeküste.
Am Ende gewannen aber die Rostocker etwas glücklich mit 2:1, es war der dritte Sieg in Folge.

<canvas id="xgplot131920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['13'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot131920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.TSV_1860_MÜNCHEN,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Das Spiel war in der Anfangsphase ausgeglichen und spielte sich größtenteils in Mittelfeld ab.
Die Hanseaten waren zunächst um Spielkontrolle bemüht, ohne dabei gefährlich in das letzte Drittel zu kommen.
Die Münchener lauerten dagegen anfangs auf Konter.
Im Verlauf der Halbzeit kamen die Bayern besser ins Spiel und nahmen den Rostockern die Spielkontrolle immer weiter ab.
Beide Teams konnten sich dabei gelegentlich Torchancen erspielen, richtig gefährlich wurde es aber vor keinem der Tore.
So war es auch ein Torschuss aus spitzem Winkel den Maximilian Ahlschwede in der 43. Minute zum 1:0 verwandelte.
Mit der knappen Führung im Rücken ging es in die Halbzeitpause.
Zu diesem Zeitpunkt stand es nach xG 0,25:0,3.

Mit der Beginn der zweiten Halbzeit waren die Löwen das bessere Team, die erste Chance hatte jedoch der FC Hansa.
Nik Omladic sorgt mit einem Kopfball nach einer Flanke von Aaron Opoku für das 2:0.
Nur wenige Minute später gelang den Münchern aber schon der Anschlusstreffer durch Sascha Mölders, der im Sitzen aus fünf Metern traf.
Das Spiel beruhigte sich in der Folge wieder etwas, die Gäste bestimmten aber weiterhin das Spiel.
In den letzten Minuten wurde es durch eine taktische Umstellung der Gäste nochmal hektisch.
Erst zwang Marius Willisch Markus Kolke zu einer Parade (86. Minute), dann hatte Korbinian Vollmann das 3:1 auf dem Fuß (86.) und zum Schluss war es nochmal Mölders,der aus kurzer Distanz mit dem Kopf an Kolke scheiterte (87.).
Somit blieb es beim 2:1-Sieg für die Gastgeber, während die Gäste mit 0,81:1,53 nach erwarteten Toren den Platz verließen.

Stochastisch betrachtet hätten die Bayern mindestens ein Punkt mit nach Hause nehmen müssen.
In 57,38% der Fälle endet so ein Spiel mit einem Auswärtssieg und 27,65% der Spiele enden unentschieden.
Nur 14,97% der Spiele enden so wie gestern mit einem Rostocker Sieg.
Ebenso war ein 2:1-Sieg mit nur 5,83% sehr unwahrscheinlich.
Die wahrscheinlichsten Ergebnisse sind 0:1 (18,92%), 1:1 (17,4%), 0:2 (14,34%) und 1:2 (13,19%).