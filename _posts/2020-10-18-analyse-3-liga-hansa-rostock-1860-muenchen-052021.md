---
title: "Kurzanalyse 5. Spieltag: Hansa Rostock gegen 1860 München"
description: "Kurzanalyse zum 5. Spieltag 2020/21 zwischen Hansa Rostock und 1860 München"
tags: [3. Liga, FC Hansa Rostock, 1860 München, Stefan Lex, Markus Kolke, Maurice Litka, Daniel Wein, Pascal Breier, Dennis Erdmann, Nico Neidhart, Bentley Baxter Bahn, John Verhoek, Max Reinthaler, Korbinian Vollmann, Nik Omladic]
categories: []
---

Am 5. Spieltag der Saison begrüßten die Hanseaten die Münchener Löwen.
Die Teams teilten sich schlussendlich die Punkte mit einem 1:1-Unentschieden.

<canvas id="xgplot052021"></canvas>
<canvas id="winratio052021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['5'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot052021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.TSV_1860_MÜNCHEN,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio052021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften brauchten nicht lange um in die Partie zu kommen und versuchten frühzeitig Druck auf die gegnerische Defensive auszuüben.
Die erste Chance des Spiels hatten die Gäste in der 13. Minute.
Der Schuss von Stefan Lex konnte aber von Markus Kolke zur Ecke geklärt werden.
Auf der Gegenseite wurde es in der 17. Minute das erste Mal durch Maurice Litka gefährlich, der sich im Alleingang gegen fünf Gegenspieler durchsetzte, aber dann am Torwart scheiterte.
In der 24. Minute waren wieder die Gäste gefährlich vor dem Rostocker Tor.
Bei der Ecke ging Daniel Weins Kopfball aber deutlich über das Tor.
Nach einer halben Stunde hatte Pascal Breier für die Hanseaten die Führung auf dem Fuß.
Er fehlte aber das Tor, nach einigem Durcheinander im Strafraum in Folge eines Litka-Freistoßes.
Kurz vor der Pause sollte dann doch noch ein Tor fallen.
Es waren die Münchener, die mit 1:0 in Führung gingen.
Nach einer Ecke stieg der Ex-Rostock Dennis Erdmann am Höchsten und drückte den Ball über die Torlinie.

Mit dem Wiederanpfiff übernahmen die Hanseaten die volle Kontrolle und ließen die Gäste nicht mehr vor das eigene Tor; es entwickelte sich ein Spiel auf ein Tor.
So dauerte es nur bis zur 50. Minute ehe die Rostocker den Ausgleich erzielten.
Nico Neidhart brachte einen Ball von der rechten Seite flach in den Rückraum, den Bentley Baxter Bahn dann einschieben konnte.
In der 62. Minute hatte erneut Breier die Chance auf ein Tor, doch aus einer ähnlichen Situation wie in der ersten Halbzeit verfehlte er auch hier das Tor.
Sechs Minuten später war es wieder Breier.
Dieses Mal brachte er seinen Schuss aus der Drehung auf das Tor, doch der Torwart hatte damit keine Probleme und hielt den Ball fest.
Weitere drei Minuten später tauchte John Verhoek nach einem tiefen Pass von Max Reinthaler am gegnerischen Strafraum auf, doch der herausstürmende Torwart konnte seinen Fuß noch vor den Ball bringen und den Schuss abwehren.
In der 75. Minute wurde es dann wild im Strafraum der Löwen.
Nach einem Freistoß kam Breier zum Schuss.
Der Ball hoppelte in Richtung des Tores ehe der am Boden liegende Verteidiger den Ball mit der Hand leicht berührte und dann der Ball nach dem Überrollen der Torlinie ins Seitenaus geschlagen wurde.
Der Schiedsrichter gab weder das Tor noch den Elfmeter, sondern entschied auf Einwurf.
Kurz vor Ende der Partie hatte zunächst der eingewechselte Korbinian Vollmann die Einschusschance, die aber geblockt wurde, ehe wenig später Rückkehrer Nik Omladic in den Strafraum eindrang und schließlich am Gästekeeper scheiterte.
So blieb bis zum Abpfiff beim 1:1-Unentschieden.

Durch die dominante zweite Halbzeit gewinnt der FC Hansa das Spiel nach Expected Goals mit 1,29:0,86.
Daraus ergibt sich eine Gewinnwahrscheinlichkeit von fast 46% bei einer Niederlagenwahrscheinlichkeit von knapp 24%.
Die restlichen 30% entfallen auf ein Unentschieden.
Mit 16,8% Wahrscheinlichkeit ist ein 1:1 auch das wahrscheinlichste Ergebnis.
Auf den Plätzen folgen ein 1:0 (13,3%), 2:1 (11,2%) und 0:1 (11,1%).