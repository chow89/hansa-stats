---
title: "Kurzanalyse 30. Spieltag: Dynamo Dresden gegen Hansa Rostock"
description: "Kurzanalyse zum 30. Spieltag 2020/21 zwischen Dynamo Dresden und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Dynamo Dresden, Damian Roßbach, Simon Rhein, Kevin Broll, Yannick Stark, Christoph Daferner, Philipp Hosiner, Sebastian Mai, Bentley Baxter Bahn, Markus Kolke, Jan Löhmannsröben, Nico Neidhart]
categories: []
---

Am 30. Spieltag fand das Spitzenspiel zwischen dem FC Hansa und Dynamo Dresden statt.
Beide Mannschaften scheuten das ganz große Risiko und trennten sich so mit 0:0.

<canvas id="xgplot302021"></canvas>
<canvas id="winratio302021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['30'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot302021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.DYNAMO_DRESDEN,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio302021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio302021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften fokussierten sich von Beginn an auf die Defensivarbeit und versuchten mit einzelnen Nadelstichen vor dem gegnerischen Tor aufzutauchen.
Als erstes gelang das den Rostockern in der 22. Minute als Damian Roßbach bei einem Freistoß von Simon Rhein an den Ball kam.
Der Verteidiger konnte mit seinem Kopfball aber nicht genügend Druck auf den Ball bringen, so dass Kevin Broll im Dresdner Gehäuse das Spielgerät locker aufnehmen konnte.
Nach einer halben Stunde hatten die Hausherren ihre erste gute Möglichkeit.
Yannick Stark spielte einen langen Ball auf Christoph Daferner, der mit dem Kopf auf Philipp Hosiner querlegte.
Für den Österreicher wurde der Winkel dann aber zu spitz, so dass er nur neben das Tor köpfte.
Wenige Sekunden vor dem Pausenpfiff kam Daferner nach einem langen Einwurf von Sebastian Mai erneut an den Ball.
Dieses Mal versuchte er es selbst, schoss dann aber deutlich über das Tor.

Nach der Halbzeitpause kamen die Hanseaten etwas verbessert auf das Spielfeld zurück.
Nur vier Minuten nach dem Wiederanpfiff hatte Bentley Baxter Bahn die große Chance zur Führung.
Sein Schuss wurde durch das Abfälschen erst richtig gefährlich und konnte von Broll nur mit einem Reflex geklärt werden.
In der 63. Minute setzte Stark einen Schuss von der Strafraumkante an.
Sein Schuss ging aber zu zentral auf das Tor und war für Markus Kolke keine Herausforderung.
Sieben Minute vor Schluss setzte Jan Löhmannsröben den Schlusspunkt unter die Partie.
Nach einem langen Einwurf von Nico Neidhart, den die Dresdner nicht klären konnten, kam der Defensivspieler an den Ball.
Sein Abschluss mit der Hacke verfehlte das Tor nur knapp.
So blieb es bis zum Abpfiff beim torlosen Unentschieden.

Nach Expected Goals endet das Spiel ebenfalls unentschieden mit 1,06:1,02.
Für die Rostocker bedeutet das eine Siegwahrscheinlichkeit von rund 35%, bei einer Dresdner Siegwahrscheinlichkeit von 33%.
32% der Spiele enden mit einer Punkteteilung.
Entsprechend ist ein 1:1 auch das wahrscheinlichste Ergebnis mit 18,2%.
Dahinter folgen ein 1:0-Auswärtssieg (13,4%) und ein 1:0-Heimsieg (12,0%), torlos enden 8,8% der Spielen.