---
title: "Kurzanalyse 35. Spieltag: Hansa Rostock gegen 1. FC Kaiserslautern"
description: "Kurzanalyse zum 35. Spieltag 2019/20 zwischen Hansa Rostock und 1. FC Kaiserslautern"
tags: [3. Liga, FC Hansa Rostock, 1. FC Kaiserslautern, Pascal Breier, Lukas Scherff, Mohamed Morabet, Markus Kolke, Avdo Spahic, Daniel Hanslik, Korbinian Vollmann, Max Reinthaler, Aaron Opoku, Hendrick Zuck, Maximilian Ahlschwede]
categories: []
---

Am 35. Spieltag begrüßte der FC Hansa die Roten Teufel aus Kaiserslautern im heimischen Ostseestadion.
Nach 90 umkämpften Minuten trennten sich beide Mannschaften mit 1:1.

<canvas id="xgplot351920"></canvas>
<canvas id="winratio351920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['35'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot351920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_KAISERSLAUTERN,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio351920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften spielten von Anfang an mit offenem Visier und versuchten schnell das erste Tor zu erzielen.
So hatten die Hausherren bereits nach zwei Minute die erste Möglichkeit.
Pascal Breiers Kopfball nach einer Scherff-Flanke ging aber knapp über das Tor.
In den Folgeminuten spielte sich der Großteil der Partie im Mittelfeld ab, wobei die Gäste das Spiel etwas mehr kontrollierten und die Hanseaten aus Umschaltsituationen schnell nach vorne spielen wollten.
Die dabei erspielten Torchancen waren aber alle eher ungefährlich.
Und so dauerte es bis zur 39. Minute ehe es wieder gefährlich vor dem Tor wurde.
Dieses Mal waren es die Lauterer, die nach zwei direkt gespielten Kurzpässen in Person von Mohamed Morabet frei vor dem Tor von Markus Kolke standen.
Morabet schob den Ball anschließend zum 0:1 ins Tor.
Die Rostocker wirkten durch den Rückstand keineswegs geschockt und spielten jetzt noch mutiger nach vorne und hatten nur eine Minute nach dem Gegentor die große Chance zum Ausgleich.
Wieder war es Duo aus Scherff und Breier über den der Angriff lief, doch aus dieses Mal wollte das Tor nicht fallen.
Der Lauterer Torwart Avdo Spahic konnte den Breier-Schuss in Manier eines Handballtorwarts mit dem Oberarm klären.
Anschließend ging es für beide Teams zur Halbzeit wieder in die Kabinen.

Im zweiten Spielabschnitt behielten die Rostocker ihre mutige Spielweise bei und setzten zu einem Offensivfeuerwerk an.
In der 50. und 51. Minute hatten zunächst Daniel Hanslik, Korbinian Vollmann und Breier ihre Möglichkeiten.
Vier Minuten später hatte Max Reinthaler die größte Chance der Halbzeit. Sein Kopfball ging knapp über das Tor.
Weitere fünf Minuten später scheiterte Aaron Opoku mit einem Distanzschuss.
Die Gäste wurden gänzlich in ihrer Spielhälfte eingeschnürt, konnten in der 64. Minute aber nochmal einen Entlasstungsangriff starten.
Hendrick Zucks Schuss ging dabei knapp am Tor vorbei.
Die Rostocker ließen sich davon aber nicht beirren und setzten ihren Offensivdrang fort.
In der 74. Minute halfen die Gäste den Rostocker mit einem Foul an Hanslik im Strafraum nach.
Maximilian Ahlschwede trat zum fälligen Elfmeter an und glich souverän zum 1:1 aus.
Doch damit gaben sich die Hausherren noch nicht zufrieden.
Wenig später hatte Breier sogar noch die Führung auf den Fuß.
Kurz vor Schluss hatte Hanslik auch nochmal die Möglichkeit.
Am Ende blieb es jedoch beim 1:1 und der Punkteteilung.

In der stochastischen Betrachtung waren die Rostocker wie auch schon [beim letzten Spiel]({% post_url 2020-06-21-analyse-3-liga-msv-duisburg-hansa-rostock-341920 %}) die bessere Mannschaft.
In 62% aller Spiele gewinnen die Hanseaten und in nur 14% der Spiele gewinnen die Roten Teufel. 24% der Spiele enden Unentschieden.
Das wahrscheinlichste Ergebnis bei so einem Spielverlauf ist ein 2:1-Heimsieg mit 15% Wahrscheinlichkeit. Mit etwas geringer Wahrscheinlichkeit (14%) endet das Spiel 1:1.
