---
title: "Kurzanalyse 38. Spieltag: Hansa Rostock gegen VfB Lübeck"
description: "Kurzanalyse zum 38. Spieltag 2020/21 zwischen Hansa Rostock und VfB Lübeck"
tags: [3. Liga, FC Hansa Rostock, VfB Lübeck, John Verhoek, Thorben Deters, Soufian Benyamina, Jens Härtel, Sven Mende, Bentley Baxter Bahn, Nico Neidhart, Sebastian Hertner, Lukas Scherff]
categories: []
---

Am letzten Spieltag der Saison empfing der FC Hansa den VfB Lübeck vor 7.500 Zuschauern.
Durch das 1:1-Unentschieden machten die Rostocker den Aufstieg in die 2. Bundesliga perfekt.

<canvas id="xgplot382021"></canvas>
<canvas id="winratio382021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['38'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot382021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.VFB_LÜBECK,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio382021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio382021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Rostocker traten von Beginn an offensiv auf und hatten schon in der zweite Minute die erste große Torchance des Spiels.
Nach einer Ecke schafften die Lübecker es nicht den Ball aus den Strafraum zu bekommen und die Rostocker hatten eine Schusschance nach der anderen.
Am Ende war es John Verhoek, der seinen Fuß nur wenige Meter vor dem Tor in einen Schuss reinhielt, den Ball aber trotzdem nicht auf das Tor bringen konnte.
Auf der Gegenseite spielte Thorben Deters in der 26. Minute den Ball in die Schnittstelle der Rostocker Verteidigung auf Soufian Benyamina.
Der Ex-Rostocker konnte sich erst gegen beide Verteidiger durchsetzen und schob anschließend den Ball zum 0:1 ins Tor.
Die Rostocker wirkten nach dem Rückstand etwas geschockt und überließen den Lübeckern zunächst das Spiel.
In der 41. Minute schaffte es das Team von Jens Härtel dann doch wieder in den gegnerischen Strafraum, wo Verhoek von Sven Mende gefoult wurde.
Den darauffolgenden Strafstoß verwandelte Bentley Baxter Bahn sicher zum 1:1-Ausgleich.
Wenige Sekunden vor dem Pausenpfiff kam Verhoek nach einem langen Ball von Nico Neidhart zu einer weiteren Torchance.
Der Niederländer scheiterte mit seinem Kopfball aber am Lübecker Torwart.

Nach der Halbzeitpause entwickelte sich ein ausgeglichenes Spiel mit nur wenigen Chancen auf beiden Seiten.
Die Gäste hatten in der 53. Minute nach einer Ecke ihre einzige Torchance des Spielabschnittes.
Der Schuss von Sebastian Hertner strich dabei nur knapp am Pfosten vorbei.
Die Hausherren kamen erst in der 81. Minute wieder gefährlich vor das Tor.
Erneut war es Verhoek, der von Lukas Scherff direkt vor dem Tor angespielt wurde.
Der Stürmer konnte beim Schuss aber noch entscheidend von einem Verteidiger gestört werden, so dass sein Ball über das Tor ging.
In den letzten Minute waren die Rostocker ausschließlich darum bemüht Zeit von der Uhr zu nehmen.
Nachdem der Schiedsrichter das Spiel beendete, kannte der Jubel der Rostocker über den gelungenen Aufstieg keine Grenzen mehr.

Bei den Expexcted Goals liegen die Rostocker klar vorne und gewinnen das Spiel mit 1,43:0,70.
Entsprechend gewinnen die Rostocker stochastisch betrachtet über 57% der Spiele und verlieren in nur rund 14% der Spiele. Etwa 29% der Spiele enden Unentschieden.
Die wahrscheinlichsten Ergebnisse sind ein 1:1-Unentschieden (18,1%) gefolgt von einem 1:0-Heimsieg (17,3%).