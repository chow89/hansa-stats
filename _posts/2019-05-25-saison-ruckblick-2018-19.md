---
title: "Saison-Rückblick 2018/19"
description: "Ein statistischer Rückblick auf die Saison 2018/2019"
tags: [3. Liga]
categories: []
---

Die Saison 2018/2019 ist zu Ende.
Auch wenn der FC Hansa Rostock den angepeilten Aufstieg schon früh in der Saison abgeschrieben hat, war die Saison unterm Strich doch ganz erfolgreich.
Am Ende stehen in der Liga 14 Siege, 13 Unentschieden und 11 Niederlagen zu Buche; das bei einem Torverhältnis von 47:46.

Zudem konnte nunmehr zum fünften Mal in Folge der Landespokal gewonnen und erstmals seit zehn Jahren die zweite Runde im DFB-Pokal erreicht werden.

## Gegen Aufsteiger top und gegen Absteiger flop

Beim Blick auf die Ergebnisse der Saison wird schnell klar, wo die Rostocker ihre Probleme hatten; es waren die Mannschaften aus dem Tabellenkeller.
Gegen die fünf Letzten der Tabelle (Aalen, Köln, Lotte, Cottbus, Braunschweig) konnten die Hanseaten nur zweimal siegreich vom Platz gehen, jeweils vier Spiele endeten mit einem Unentschieden oder einer Niederlage (8:12 Tore).
Ganz anders liefen dagegen die Spiele gegen die Top 5 der Tabelle. Von den zehn Spielen konnten fünf Spiele gewonnen (drei unentschieden, zwei Niederlagen) werden und dabei sogar mehr Tore erzielt werden als gegen die Teams aus dem Tabellenkeller (12:12).

![Kreuztabelle 2018/19]({{ site.url }}/images/league-overview1819.png)

## Chancenverwertung verhindert besseres Abschneiden

Mit 47 Toren stellt die Mannschaft die fünftschwächste Offensive und ist damit in dieser Statistik schlechter als beispielsweise Absteiger Energie Cottbus mit 51 Toren.
Das Problem war aber nicht, dass sich die Hanseaten zu wenige Torchancen rausgespielt haben, sondern die Chancenverwertung.
Rein von den herausgespielten Chancen hätten die Rostocker durchschnittlich 1,46 Tore pro Spiel erzielen müssen (expected goals (xG), Daten von [wyscout.com](https://wyscout.com/){:target="_blank" rel="noreferrer"}).
Stattdessen waren es aber "nur" 1,24 Tore pro Spiel.
Runtergebrochen auf einzelne Spiele bedeutet das, dass bei einigen Spiele mehr drin gewesen wäre.
Besonders prägnant war der Unterschied zwischen dem Ergebnis und dem erwarteten Ergebnis bezüglich der Torchancen bei den folgenden Spielen.


| Spieltag | Spiel | Ergebnis | xG | Punktedifferenz |
|----------|-------|----------|----|-----------------|
| 4 | Unterhaching - Hansa | 2:1 | 1,84:2,03 | 1 |
| 16 | Lotte - Hansa | 1:0 | 0,37:1.43 | 1 |
| 25 | Hansa - Meppen | 0:2 | 2,68:0,72 | 1 |
| 27 | Hansa - Großaspach | 0:0 | 2,27:0,59 | 2 |
| 37 | Hansa - Uerdingen | 1:1 | 2,8:0,97 | 2 |
| 38 | Aalen - Hansa | 1:1 | 0,75:2,29| 2 |

Alleine bei diesen sechs Spielen wurden insgesamt neun Punkte liegengelassen.
Mit den dann 64 Punkten hätte der Verein die Saison auf Platz 5 abgeschlossen und wäre nur zwei Punkte hinter Platz 4, der zur Teilnahme am DFB-Pokal berechtigt, gewesen.

## Schwächephase vor der Winterpause  

Auch in dieser Saison gab es wieder eine Schwächeperiode.
Dieses Mal begann sie nach dem furiosen Heimsieg gegen den 1. FC Kaiserslautern im November und endete erst nach acht Spielen im Februar.
In diesem Zeitraum gelangen gerade einmal zwei Unentschieden. Nicht zu Unrecht zog der Vorstand in der Winterpause die Notbremse und entließ sowohl Trainer Pavel Dotchev als auch Sportdirektor Markus Thiele.

Die restliche Saison war dagegen sehr konstant. Nur einmal wurden zwei Spiele hintereinander verloren (4. und 5. Spieltag).
Einzig die hohe Anzahl an Unentschieden ist noch verbesserungswürdig.
 
![Trend-Kurve 2018/19]({{ site.url }}/images/trend1819.png)
