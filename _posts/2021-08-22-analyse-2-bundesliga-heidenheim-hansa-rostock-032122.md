---
title: "Kurzanalyse 3. Spieltag: 1. FC Heidenheim gegen Hansa Rostock"
description: "Kurzanalyse zum 3. Spieltag 2021/22 zwischen 1. FC Heidenheim gegen Hansa Rostock"
tags: [2. Bundesliga, FC Hansa Rostock, 1. FC Heidenheim, Calogero Rizzuto, Svante Ingelsson, Jan Schöppner, Denis Thomalla, Streli Mamba, Kevin Müller, Tim Kleindienst, Markus Kolke, Ridge Munsy, Oliver Hüsing]
categories: []
---

Eine Woche nach dem [spannenden Pokalfight]({% post_url 2021-08-14-analyse-dfb-pokal-hansa-rostock-heidenheim-032122 %}) trafen der FC Hansa Rostock und der 1. FC Heidenheim erneut aufeinander, dieses Mal aber in Heidenheim.
Wie schon in der Vorwoche stand es nach 90 Minuten 1:1-Unentschieden.

<canvas id="xgplot032122"></canvas>
<canvas id="winratio032122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['3'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot032122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.HEIDENHEIM,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio032122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio032122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hanseaten fanden gut in das Spiel und hatten schon in der siebenten Minute die erste gute Möglichkeit.
Calogero Rizzuto kam nach einem langen Einwurf von Svante Ingelsson im Fünfmeterraum an den Ball, konnte das Spielgerät dann aber nicht mit genügend Kraft auf das Tor bringen.
Die Hausherren kamen in der 18. Minute zu ihrer ersten guten Torchance.
Nach einer Ecke stieg Jan Schöppner am höchsten und köpfte den Ball an die Latte.
In der 32. Minute tauchten die Gastgeber erneut vor dem Rostocker Tor auf.
Dem Schuss von Denis Thomalla, der nur knapp am Tor vorbeiflog, ging dieses Mal ein Freistoß voraus.
Auf der Gegenseite hatte Streli Mamba fünf Minute später die nächste Chance.
Der Stürmer, der zuvor von Ingelsson steil geschickt wurde, scheiterte aber am Gästetorhüter Kevin Müller.
Kurz vor der Halbzeitpause erspielten sich die Heidenheimer noch eine Chance.
Nach einem Freistoß kam Tim Kleindienst nur wenige Meter vor dem Tor zum Kopfball, köpfte dann aber knapp über das Gehäuse.
Anschließend bat der Schiedsrichter beide Teams in die Kabine.

Nach dem Wiederanpfiff blieb es lange ruhig vor den Toren.
Erst in der 68. Minute hatten die Heidenheimer ihre nächste Möglichkeit, die zum 1:0 führte.
Erneut war es Kleindienst, der bei einem Eckstoß an den Ball kam, einen Torwartfehler von Markus Kolke ausnutzte und so den Ball einnetzen konnte.
Nach dem Führungstreffer verlagerte sich das Spiel wieder in das Mittelfeld, ehe die Hanseaten in der 82. Minute erneut vor das Tor kamen.
Ridge Munsy dribbelte sich an drei Verteidigern vorbei und tunnelte anschließend Müller zum 1:1-Ausgleich.
Die Heidenheimer hatten in der Nachspielzeit nochmal die Möglichkeit zum erneuten Führungstreffer, doch Kolke konnte den Kopfball von Oliver Hüsing ohne Probleme fangen.
Somit blieb es beim 1:1-Unentschieden und der Punkteteilung.

Bei den Expected Goals lief das Spiel komplett anders, hier gewinnen die Gastgeber deutlich mit 2,03:0,66.
Entsprechend gewinnen die Heidenheimer fast 74% der Spiele, während die Rostocker in nur 8% der Spielen als Sieger den Platz verlässt.
Rund 18% der Spiele enden unentschieden.
Die wahrscheinlichsten Ergebnisse sind ein 2:0- (16,6%), ein 1:0- (12,9%) und ein 2:1-Heimsieg (12,7%).
Mit 1:1 enden 9,9% der Spiele.