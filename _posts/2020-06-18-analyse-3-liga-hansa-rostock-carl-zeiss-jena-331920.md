---
title: "Kurzanalyse 33. Spieltag: Hansa Rostock gegen Carl Zeiss Jena"
description: "Kurzanalyse zum 33. Spieltag 2019/20 zwischen Hansa Rostock und Carl Zeiss Jena"
tags: [3. Liga, FC Hansa Rostock, Carl Zeiss Jena, Pascal Breier, Patrick Schorr, Nico Granatowski, Max Reinthaler, Nikolas Nartey, Mirnes Pepic, Maximilian Ahlschwede, Jo Coppens, Rasmus Pedersen, Aaron Opoku, Nico Neidhart]
categories: []
---

Auch am 33. Spieltag der Saison konnten die Rostocker als Sieger den Platz verlassen.
Die Partie gegen Carl Zeiss Jena gewannen die Gastgeber mit 4:0.

<canvas id="xgplot331920"></canvas>
<canvas id="winratio331920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['33'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot331920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.CARL_ZEISS_JENA,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio331920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die erste Halbzeit begann ausgeglichen.
Die Gäste, deren Abstieg bereits besiegelt war, konnten ohne Druck mitspielen, während die Hanseaten nach sechs Wechseln in der Startelf besonders in der Abwehrkette Abstimmungsschwierigkeiten hatten.
So entwickelte sich in der Anfangsphase ein zerfahrenes Spiel mit vielen kleine Fehlern.
Einen dieser Fehler konnte Pascal Breier in der 16. Minute ausnutzen, als er einen Rückpass von Patrick Schorr abfing und zum 1:0 ins Tor schob.
Eine Viertelstunde später wurde es vor dem Jenaer Tor wieder gefährlich.
Bei einer Granatowski-Ecke hatte zunächst Max Reinthaler die Chance auf das nächste Tor, ehe anschließend Breier zum Nachschuss kam.
Doch auch dieser Schuss landete nicht im Tor.
Nur eine Minute später hatte Reinthaler nach einer Nartey-Ecke die nächste große Chance.
Dieses Mal landete sein Kopfball am Pfosten.
Kurz vor der Halbzeit sorgen die Ostseestädter für die Vorentscheidung.
Erst war es Breier, der nach einer Flanke von Mirnes Pepic auf 2:0 erhöhte, anschließend Maximilian Ahlschwede, der nach einem Foul an Nikolas Nartey im Strafraum einen Elfmeter zum 3:0 verwandelte.
Direkt danach beendete der Schiedsrichter die Halbzeit.

Im zweiten Spielabschnitt dominierten die Rostocker das Spiel und ließen den Gästen keine Chance mehr ins Spiel zurückzukommen.
In der Anfangsviertelstunde passierte vor den Toren aber zunächst nichts.
Erst in der 62. Minute hatten die Rostocker durch einen erneuten Elfmeter die nächste Torchance.
Der Gästetorwart Jo Coppens konnte den schwachen Schuss von Nico Granatowski aber halten.
Sieben Minute später hatte Hansa durch Mirnes Pepic die nächste Möglichkeit, doch auch er scheiterte am Torwart.
Drei Minuten vor Schluss fiel dann aber doch noch ein Tor.
Rasmus Pedersen schloss einen sehenswerten Angriff über Pepic, Aaron Opoku und Nico Neidhart ab und stellte den 4:0 Endstand her.

Der deutliche Sieg spiegelt sich auch in den Siegwahrscheinlichkeiten wieder.
Hansa gewinnt etwa 84% solcher Spiele und beendet etwa 12% der Spiele unentschieden.
Die restlichen drei Prozent der Spielen gewinnen die Gäste aus Jena.
Entsprechend einseitig sind auch die zu erwartenden Ergebnisse.
Mit 23% ist ein 2:0-Sieg am wahrscheinlichsten.
Mit jeweils 15% folgen ein 1:0- und 3:0-Sieg.
Das 4:0 wird in nur 5% der Spiele erreicht.