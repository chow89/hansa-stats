---
title: "Kurzanalyse 2. Spieltag: Hannover 96 gegen Hansa Rostock"
description: "Kurzanalyse zum 2. Spieltag 2021/22 zwischen Hannover 96 gegen Hansa Rostock"
tags: [2. Bundesliga, FC Hansa Rostock, Hannover 96, Marcel Franke, Streli Mamba, Bentley Baxter Bahn, Ron-Robert Zieler, Hanno Behrens, Svante Ingelsson, John Verhoek, Marvin Ducksch, Markus Kolke, Nik Omladic, Marc Lamti]
categories: []
---

Am 2. Spieltag musste der FC Hansa zum ersten Auswärtsspiel nach Hannover reisen.
Das Spiel gegen die Niedersachsen gewannen die Rostocker deutlich mit 0:3.

<canvas id="xgplot022122"></canvas>
<canvas id="winratio022122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['2'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot022122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.HANNOVER_96,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio022122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio022122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Während die Hanseaten im [vorherigen Spiel]({% post_url 2021-07-25-analyse-2-bundesliga-hansa-rostock-karlsruher-sc-012122 %}) mit 1:3 als Verlierer vom Platz gingen, gelang den Hannoveranern ein 1:1-Unentschieden gegen Bundesligaabsteiger Werder Bremen.
Die Hausherren gingen somit auch als Favorit in das Spiel.
Und so bestimmten die Gastgeber in der Anfangsphase auch das Spiel ohne dabei die ganz große Torgefahr zu entwickeln.
Die Rostocker ihrerseits brauchten einiges an Zeit um in das Spiel zu finden und sich auf den Gegner einzustellen.
Die erste gute Möglichkeit hatten folgerichtig die Niedersachsen nach zwölf Minuten.
Kapitän Marcel Franke stieg bei einer Ecke am höchsten, konnte den Ball aber nicht auf das Tor bringen.
Für die Rostocker dauerte es bis zur 39. Minute, ehe sie erstmals gefährlich vor das Tor kamen.
Streli Mamba wurde in der Szene mit einer Flanke von Bentley Baxter Bahn bedient und köpfte den Ball gut positioniert auf das Tor, doch Ron-Robert Zieler konnte den Ball mit einer starken Parade noch zur Ecke klären.
Eine Minute später hatten die Rostocker dann etwas mehr Glück im Abschluss und gingen mit 0:1 in Führung.
Eine kurz ausgeführte Ecke landete bei Hanno Behrens, der im zweiten Versuch das Leder in das Netz schlenzte.
Mit dieser Führung im Rücken ging es wenig später in die Halbzeitpause.

Nach der Pause konnten der FC Hansa gleich im ersten Angriff sogar auf 0:2 erhöhen.
Nach einem langen Einwurf von Svante Ingelsson netzte John Verhoek ohne Gegenwehr ein.
Die Hannoveraner taten sich in der Folge schwer hinter die jetzt sehr tiefstehende Rostocker Abwehr zu kommen.
Erst in der 63. Minute hatten die Hausherren wieder eine nennenswerte Torchance.
Sie spielten sich mit schnellem Kurzpassspiel durch die Rostocker Verteidigung und am Ende gelang das Spielgerät zu Marvin Ducksch, der den Ball aber nicht gefährlich genug auf das Tor brachte und Markus Kolke so nicht überwunden konnte.
Die Gäste, die jetzt nur noch durch Konter und Standardsituation vor das Tor kamen, hatten in der 77. Minute ihre nächste Möglichkeit.
Wieder war es Behrens, der nach einer Ecke als erster an den Ball kam.
Doch sein Kopfball verfehlte das Tor deutlich.
In der Nachspielzeit sollte dann aber doch noch das 0:3 gelingen.
Nik Omladic nahm Marc Lamti im Aufbauspiel den Ball ab und chippte anschließend über Zieler ins Tor ein.
Wenige Sekunden danach beendete der Schiedsrichter die Partie.

So deutlich wie das Ergebnis ausgefallen ist, ist es bei den Expected Goals nicht.
Hier liegen die Rostocker nur ganz knapp vorne und "gewinnen" mit 1,05:1,14.
Entsprechend ausgeglichen sind auch die Siegwahrscheinlichkeiten.
Die Hanseaten gewinnen über 36% der Spiele und verlieren über 32% der Spiele.
Die restlichen rund 31% aller Spiele enden Unentschieden.
Die wahrscheinlichsten Ergebnisse bei diesem Spiel sind somit ein 1:1 (18,1%), ein 1:0-Heimsieg (12,4%) und ein 0:1-Auswärtssieg (11,4%).
Mit 0:3 enden nur etwa 2% der Spiele.