---
title: "Kurzanalyse 32. Spieltag: FC Bayern München II gegen Hansa Rostock"
description: "Kurzanalyse zum 32. Spieltag 2020/21 zwischen FC Bayern München II und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, FC Bayern München II, Nik Omladic, Nico Neidhart, Ron-Thorben Hoffmann, Dennis Waidner, Nicolas Feldhahn, Markus Kolke, Pascal Breier, Sarpreet Singh, Simon Rhein, John Verhoek, Justin Che, Lukas Scherff]
categories: []
---

Zu Beginn des 32. Spieltages mussten die Rostocker bei der zweiten Mannschaft des FC Bayern München antreten.
Die chancenarme und umkämpfte Partie konnten die Gäste knapp mit 0:1 für sich entscheiden.

<canvas id="xgplot322021"></canvas>
<canvas id="winratio322021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['32'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot322021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.BAYERN_MÜNCHEN_II,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio322021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio322021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Obwohl die Hausherren von Beginn an das etwas bessere Team waren, hatten die Hanseaten die erste Torchance des Spiels.
In der 9. Minute kam Nik Omladic nach einer Flanke von Nico Neidhart zum Flugkopfball, konnte Ron-Thorben Hoffmann damit aber nicht überwinden.
Nach 21 gespielten Minuten tauchten dann auch die Münchener das erste Mal vor dem Tor der Rostocker auf.
Dennis Waidners Flanke fand mit Nicolas Feldhahn einer Abnehmer, der mit seinem Kopfball aber nicht an Markus Kolke vorbei kam.
Weitere zehn Minuten später legte Pascal Breier im eigenen Strafraum einen Ball für Sarpreet Singh auf, der Kolke dann zu einer Parade zwang.
Nur wenige Sekunden später versuchte Breier seinen Fehler wieder gutzumachen und dribbelte sich in den Strafraum der Gastgeber.
Sein Schuss wurde aber noch abgefälscht und ging weit über das Tor.
Kurz vor dem Halbzeitpfiff spielte Simon Rhein einen langen Ball in den Lauf von John Verhoek.
Der Niederländer spitzelte das Spielgerät vor dem herausgestürmten Bayern-Torwart weg und netzte anschließend sicher zur 0:1-Führung ein.

Nach der Pause erhöhten die Gastgeber den Druck und hatten in der 49. Minute die nächste gute Möglichkeit.
Wieder war es Singh, der von der Strafraumkante abzog und an der Latte scheiterte.
In der 78. Minute hatten die Bayern-Amateure nach einem Freistoß die nächste Chance auf den Ausgleich, doch Justin Che traf den Ball bei seinem Kopfbalversuch nicht richtig und so versiegte auch diese Chance.
Auf der Gegenseite setzte dann Breier in der 82. Minute den Schlusspunkt der Partie.
Der Stürmer konnte die scharf reingegebene Flanke von Lukas Scherff aber nicht ganz optimal treffen und so ging der Ball knapp über das Gehäuse der Bayern.
So blieb es bis zum Abpfiff bei der knappen 0:1-Führung für den FC Hansa.

Nach Expected Goals liegen die Hausherren in diesem Spiel mit 0,96:0,70 vorne.
Daraus ergeben sich für die Rostocker eine Siegwahrscheinlichkeit von etwa 25% und eine Niederlagenwahrscheinlichkeit von rund 40%.
Die restlichen 34% der Spiele enden Unentschieden.
Aufgrund der wenigen Chancen ist ein 1:0-Heimsieg mit 18,3% das wahrscheinlichste Ergebnis.
Knapp dahinter liegen ein 0:0 (16,7%) und ein 1:1-Unentschieden (15,3%).
Einen Rostocker 1:0-Sieg gibt es in nur 13,9% der Spiele.