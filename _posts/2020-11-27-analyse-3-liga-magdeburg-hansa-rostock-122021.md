---
title: "Kurzanalyse 12. Spieltag: FC Magdeburg gegen Hansa Rostock"
description: "Kurzanalyse zum 12. Spieltag 2020/21 zwischen FC Magdeburg und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, FC Magdeburg, Markus Kolke, Thore Jacobsen, Maurice Litka, John Verhoek, Max Reinthaler, Christian Beck, Korbinian Vollmann, Andreas Müller, Luca Horn, Morten Behrens, Björn Rother]
categories: []
---

Am 12. Spieltag stand für den FC Hansa das dritte Ostduell in Folge an, es ging zum FC Magdeburg.
Beide Teams trennten sich schließlich mit einem 1:1-Unentschieden.

<canvas id="xgplot122021"></canvas>
<canvas id="winratio122021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['12'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot122021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_MAGDEBURG,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio122021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Partie war von Beginn an ein umkämpftes und offenes Spiel.
Vier Minuten nach dem Anpfiff hatten die Hausherren bereits ihre erste Torchance.
Markus Kolke hatte mit dem Distanzschuss von Thore Jacobsen aber überhaupt keine Probleme.
In der zehnten Minute kamen auch die Gäste zu ihrer ersten Möglichkeit.
Maurice Litkas Schuss von der Strafraumgrenze rollte aber deutlich am Tor vorbei.
Sieben Minuten später tauchte Litka erneut vor dem Magdeburger Tor auf, verdribbelte sich jedoch, so dass ein Verteidiger seinen Schuss blocken konnte.
Nur zwei Minuten später hatte Litka die nächste Möglichkeit zur Führung, doch auch bei diesem Versuch wurde sein Schuss geblockt.
Und nur eine Minute danach war es wieder Litka der zum nächsten Torschuss ansetzte.
Dieses Mal ging der Ball weit über das Tor.
Nach der kleinen Druckphase wurde es wieder etwas ruhiger, ehe die Hanseaten in der 29. Spielminute die nächste Torchance hatten.
John Verhoek kam aus vollem Lauf mit dem Kopf an den Ball, konnte das Spielgerät aber nicht mehr am herauseilenden Magdeburger Torwart vorbeilegen.
Auf der Gegenseite wurde es zwei Minuten danach wieder gefährlich.
Nach einem Fehlpass von Max Reinthaler im Spielaufbau kam Christian Beck zur Schusschance.
Sein erster Schuss wurde noch geblockt, der zweite ging dann aber zur 1:0-Führung in das Tor der Rostocker.
Nach dem Tor spielten sich die beiden Teams keine weiteren Möglichkeiten heraus und so ging es mit diesem Spielstand in die Halbzeit.

Mit Beginn des zweiten Spielabschnitts traten die Gäste kompakter auf und bekamen die Angriffsversuche der Hausherren besser in den Griff.
Die erste Tormöglichkeit hatte Verhoek nach einer Ecke in der 52. Minute.
Der Stürmer konnte den Ball mit dem Kopf aber nicht in Richtung Tor drücken.
Zwei Minuten später machte der Niederländer es dann aber besser.
Nach einer Ablage von Korbinian Vollmann befördete Verhoek den Ball zum 1:1-Ausgleich in das Tor.
Die Gastgeber verfielen davon aber nicht Schockstarre sondern hatten ihrerseits zwei Minuten später die Möglichkeit zur erneuten Führung.
Der Schuss von Andreas Müller wurde aber geblockt.
Durch die darauffolgenden Auswechselungen spielte sich das Spiel weitestgehend im Mittelfeld ab und beide Mannschaften konnten lange Zeit keine Torgefahr entwickeln.
Erst in der 87. Minute hatten die Rostocker wieder eine Torchance durch Luca Horn.
Der eingewechselte Sommer-Neuzugang lief über 30 Meter alleine auf das Tor zu, scheiterte dann aber an Morten Behrens im Magdeburger Tor, der mit einem Reflex retten konnte.
In der Nachspielzeit hatte der ebenfalls eingewechselte Pascal Breier nochmal die Möglichkeit zum Torerfolg, doch sein Volleyschuss nach einer Flanke von Björn Rother ging deutlich über das Tor.
So blieb es bis zum Abpfiff beim 1:1-Unentschieden.

Bei den Expected Goals gewinnen die Hanseaten mit 1,77:1,12.
Das bedeutet eine Siegwahrscheinlichkeit von über 53% für die Gäste.
Gleichzeitig gewinnen die Hausherren in fast 22% aller Spiele, während rund 24% der Spiele unentschieden enden.
Das wahrscheinlichste Ergebnis bei diesem Spielverlauf ist ein 1:1 mit 12,3% Wahrscheinlichkeit, knapp gefolgt von einem 1:2-Auswärtssieg mit 12,2%.