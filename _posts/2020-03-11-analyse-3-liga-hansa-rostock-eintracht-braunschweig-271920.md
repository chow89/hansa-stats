---
title: "Kurzanalyse 27. Spieltag: Hansa Rostock gegen Eintracht Braunschweig"
description: "Kurzanalyse zum 27. Spieltag 2019/20 zwischen Hansa Rostock und Eintracht Braunschweig"
tags: [3. Liga, FC Hansa Rostock, Eintracht Braunschweig, John Verhoek, Nikolas Nartey, Daniel Hanslik, Aaron Opoku, Nico Neidhart, Robin Ziegele, Nils Butzen]
categories: []
---

Im zweiten Montagsspiel in Folge konnten die Rostocker eine bessere Leistung abrufen als [in der Vorwoche]({% post_url 2020-03-04-analyse-3-liga-preussen-muenster-hansa-rostock-261920 %}).
Nach einem starken Spiel gegen Eintracht Braunschweig stand am Ende ein ungefährdeter 3:0-Sieg zu Buche.

<canvas id="xgplot271920"></canvas>
<canvas id="winratio271920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['27'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot271920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.EINTRACHT_BRAUNSCHWEIG,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio271920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hanseaten spielten von Anfang an offensiv und ließen die Gäste überhaupt nicht ins Spiel kommen.
Gleich mit der ersten Torchance in der zweiten Minute konnten die Gastgeber in Führung geben.
John Verhoek spitzelte einen strammen Schuss von Nikolas Nartey am Torhüter vorbei ins Netz.
Nur neun Minuten später waren die Hanseaten erneut vor dem Braunschweiger Tor und erhöhten sogleich auf 2:0.
Dieses Mal war es Daniel Hanslik, der bei seinem ersten Treffer im Hansa-Trikot auf engstem Raum zwei Gegenspieler ausspielte und anschließend den Torhüter tunnelte.
Danach zogen sich die Rostocker etwas zurück ohne den Gästen das Spiel zu überlassen.
In der 27. Minute sorgten die Ostseestädter wieder für Aufregung im Strafraum der Gäste.
Die Rostocker störten die Niedersachsen erfolgreich beim Spielaufbau am eigenen Strafraum und Nartey legte den Ball anschließend auf den freistehenden Aaron Opoku ab, der bei seinem Abschluss aber nur den herausstürmenden Torwart anschoss.
Den Rest der Halbzeit passierte dann nichts mehr und so gingen die beiden Teams beim Stand von 2:0 in die Kabine.

Nach dem Wiederanpfiff ging das Spiel unverändert mit Rostocker Dauerdruck und Braunschweiger Harmlosigkeit weiter.
Bereits nach drei Minuten hatte Nico Neidhart nach einem langen Sololauf die erste Chance der Halbzeit.
Seinem Schuss von der Strafraumkante fehlte dann aber die Kraft.
Acht Minuten später versuchte sich Verhoek aus ähnlicher Position mit einem Volleyschuss.
Dieser ging jedoch deutlich am Tor vorbei.
Weitere vier Minuten später hatte dann Nartey die Möglichkeit und scheiterte am Block von Robin Ziegele.
In der 65. Minute war Nils Butzen an der Reihe, der nach starker Vorarbeit von Opoku aus spitzem Winkel aber nur die Latte traf.
In der Folge nahme die Qualität der Rostocker Chancen etwas ab.
Erst in der Nachspielzeit wurde es wieder gefährlich und Verhoek vollendete eine Passsequenz im Braunschweiger Strafraum zum 3:0.
Wenige Minuten später pfiff der Schiedsrichter dann das Spiel ab und die Hanseaten gingen als verdienter Sieger vom Platz.

Ebenso deutlich wie der Spielverlauf ist auch die stochastische Betrachtung.
Nach expected Goals gewinnen die Rostocker das Spiel mit 3,02:0,36.
Daraus ergibt sich eine Gewinnwahrscheinlichkeit von über 93%. Knapp über 5% der Spiele enden Unentschieden und etwas über 1% der Spiele enden mit einem Sieg der Braunschweiger.
Bei den erwarteten Ergebnissen liegt der 3:0-Heimsieg mit fast 20% Wahrscheinlichkeit vorne. Es folgen ein 2:0 mit etwa 16% und ein 4:0 mit etwa 14% Wahrscheinlichkeit.