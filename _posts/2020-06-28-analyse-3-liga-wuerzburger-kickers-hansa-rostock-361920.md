---
title: "Kurzanalyse 36. Spieltag: Würzburger Kickers gegen Hansa Rostock"
description: "Kurzanalyse zum 36. Spieltag 2019/20 zwischen Würzburger Kickers und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Würzburger Kickers, Daniel Hägele, Markus Kolke, Luca Pfeiffer, Daniel Hanslik, Lukas Scherff, Korbinian Vollmann, Pascal Breier]
categories: []
---

Am drittletzten Spieltag der Saison duellierten sich die beiden Aufstiegsaspiranten aus Rostock und Würzburg.
Nach einer schwachen Leistung der Hanseaten gewannen die Würzburger Kickers mit 3:1.

<canvas id="xgplot361920"></canvas>
<canvas id="winratio361920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['36'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot361920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.WÜRZBURGER_KICKERS,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio361920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften machten in der Anfangsphase viele Fehler im Spielaufbau und hatten große Probleme ins Spiel zu finden.
Als erstes konnten dann die Hausherren ihren Spielplan durchziehen und erzielten gleich mit ihrem ersten Torschuss in der 19. Minuten das 1:0.
Nach einer Ecke ging der abgefälschte Schuss von Daniel Hägele an Markus Kolke vorbei ins Netz.
Zwölf Minuten später erhöhten die Würzburger auf 2:0 nachdem sich die Rostocker Verteidigung nach einem bereits geklärten Einwurf mit einem hohem Ball in den Rücken ausspielen ließen und Luca Pfeiffer den Ball nur noch an Kolke vorbei schieben musste.
Anschließend ließen die Bayern den Gästen etwas mehr Platz bei der Spielgestaltung und so fanden die Hanseaten auch langsam ins Spiel.
Daniel Hanslik hatte drei Minuten vor der Halbzeit dann sogar noch die Chance auf den Anschlusstreffer.
Sein Schuss ging aber knapp über das Tor.
So verabschiedeten sich beide Teams beim Stand von 2:0 in die Halbzeitpause.

Der zweite Spielabschnitt begann mit unverändertem Bild und nur wenige Minuten nach dem Wiederanpfiff, hatte Pfeiffer die große Chance zur Vorentscheidung, scheiterte aber zweimal an den starken Reflexen von Kolke.
In der 67. Minute hatten die Ostseestädter erneut die Möglichkeit auf den Anschlusstreffer.
Lukas Scherff konnte einen Kopfball nach einer Ecke von Korbinian Vollmann aber nicht zielgerichtet auf das Tor bringen.
Drei Minuten danach folgten dann aber der Entscheidungstreffer zum 3:0.
Wieder war es Pfeiffer, der erst Scherff im Strafraum ausspielte und anschließend den Ball ins Tor beförderte.
In der 87. Minuten durften die Hanseaten nochmal Ergebniskosmetik betreiben und auf 3:1 verkürzen.
Pascal Breier konnte nach einer Einzelaktion, bei der er mehrere Verteidiger und den Torwart umspielte, aus kurzer Distanz einfach einschieben.
Anschließend beendete der Schiedsrichter das Spiel und die Würzburger gingen als Sieger vom Platz.

In der stochastischen Betrachtung sieht das Spiel nicht ganz so deutlich aus.
Die Würzburger gewinnen nach Expected Goals knapp mit 1,81:1,55.
Daraus ergibt sich eine Siegwahrscheinlichkeit von fast 43%.
Die Hanseaten gewinnen immerhin noch in fast 30% aller Spiele.
Die restlichen 27% der Spiele enden Unentschieden.
Entsprechend ist ein 2:1-Heimsieg mit gut 14% am wahrscheinlichsten.
Es folgen mit 13% Wahrscheinlichkeit ein 1:1-Unentschieden und mit 11% der 3:1-Heimsieg.