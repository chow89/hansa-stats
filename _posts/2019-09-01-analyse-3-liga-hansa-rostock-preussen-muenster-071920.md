---
title: "Kurzanalyse 7. Spieltag: Hansa Rostock gegen Preußen Münster"
description: "Kurzanalyse zum 7. Spieltag 2019/20 zwischen Hansa Rostock und Preußen Münster"
tags: [3. Liga, FC Hansa Rostock, SC Preußen Münster, Sven Sonnenberg, Mirnes Pepic, Osman Atilgan, Korbinian Vollmann, Aaron Opoku, John Verhoek, Pascal Breier, Marco Königs, Maximilian Schulze Niehues]
categories: []
---

Nach drei sieglosen (Liga-)Spielen in Folge konnte der FC Hansa endlich wieder gewinnen.
Gegen Preußen Münster setzten sich die Rostocker mit 1:0 durch.

<canvas id="xgplot071920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['7'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot071920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.PREUSSEN_MÜNSTER,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hanseaten machten gleich von Anfang an Druck und erspielten sich in der Anfagnsviertelstunde diverse Chancen (Vollmann, Verhoek, Pepic), die aber alle nicht auf das Tor gingen.
Die erste große Torchance hatte dann in der 16. Minute Aaron Opoku, der freistehend vor dem Tor aber nur den Münsteraner Torhüter Schulze Niehues anschoss.
Wenig später hatte Neuzugang Osman Atilgan nach einem Konter ebenfalls eine Chance, die aber knapp das Tor verfehlte.
In der Folge beruhigte sich das Spiel weitestgehend und so ging es mit 0:0 nach Toren und 0,68:0,14 nach xG in die Halbzeit.

Die zweite Halbzeit begann so wie der Erste.
Hansa erarbeitet sich in den ersten 15 Minuten viele Chancen (Opoku, Vollmann, Atilgan, Verhoek), aber keine davon war so richtig zwingend.
Nach einer kurzen Ruhephase, in der mit Pascal Breier und Marco Königs zwei neue Stürmer kamen, begann Hansa in der 70. Minute mit der nächsten Drangphase.
Aber auch hier blieben viele Chancen (Königs, Sonnenberg, Atilgan) ungenutzt, ehe es Aaron Opoku in der 77. Minute gelang den Ball ins Tor zu befördern.
Zwei Minuten später bekam Pascal Breier noch die große Chance auf 2:0 zu erhöhen, scheiterte aber am Münsteraner Torhüter.
Die Preußen hatten in der letzten Minute nochmals zwei Chancen nach Ecken, die aber ebenso ungenutzt blieben.
Am Ende trennten sich die beiden Teams mit 1:0 und 2.06:0,39 xG.
Unterm Strich also ein verdienter Sieg für die Ostseestädter, der auch hätte höher ausfallen können.