---
title: "Kurzanalyse 10. Spieltag: Hansa Rostock gegen SV Sandhausen"
description: "Kurzanalyse zum 10. Spieltag 2021/22 zwischen Hansa Rostock und SV Sandhausen"
tags: [2. Bundesliga, FC Hansa Rostock, SV Sandhausen, Arne Sicker, John Verhoek]
categories: []
---

<canvas id="xgplot102122"></canvas>
<canvas id="winratio102122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['10'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot102122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SV_SANDHAUSEN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio102122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio102122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>
