---
title: "Kurzanalyse 4. Spieltag: SC Verl gegen Hansa Rostock"
description: "Kurzanalyse zum 4. Spieltag 2020/21 zwischen SC Verl und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, SC Verl, Markus Kolke, Zlatko Janjic, Bentley Baxter Bahn, Nico Neidhart, Maurice Litka, Aygün Yildirim, John Verhoek, Pascal Breier, Mehmet Kurt, Damian Roßbach]
categories: []
---

Am vierten Spieltag der Saison mussten die Rostocker zum Liganeuling SC Verl.
Das umkämpfte Spiel gewannen die Hanseaten knapp mit 2:3.

<canvas id="xgplot042021"></canvas>
<canvas id="winratio042021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['4'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot042021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SC_VERL,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio042021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften fanden sofort gut in das Spiel und die Verler Hausherren kamen bereits in der sechsten Minute zu ihrer ersten großen Torchance.
Markus Kolke konnte Zlatko Janjic' Kopfball mit einer Parade noch klären.
Nach einer halben Stunde konnte die Verler Bentley Baxter Bahn im Strafraum nur noch mit einem Foul stoppen.
Der Gefoulte trat selbst zum Strafstoß an und verwandelte sicher zum 0:1.
Wenige Minuten später konnte Nico Neidhart nach einer Vorlage von Maurice Litka zum 0:2 nachlegen.
Die Hausherren zeigten sich davon aber nicht geschockt und konnten nach einem zu kurzen Rückpass von Sven Sonnenberg auf 1:2 verkürzen.
Aygün Yildirim war der Torschutze.
Kurz vor der Halbzeit stellten die Gäste den Zwei-Tore-Vorsprung wieder her.
Litka konnte nach einem Querpass von John Verhoek problemlos zum 1:3 einnetzen.
In der 45. Minute hätte Bahn sogar noch erhöhen können, schoss jedoch über das Tor.

Gleich nach dem Wiederanpfiff hatte Pascal Breier das 1:4 auf dem Fuß, doch auch er konnte den Ball nicht auf das Tor bringen.
In der 54. Minute gelang den Hausherren durch einen Fernschuss von Mehmet Kurt der erneute Anschlusstreffer.
Nachdem Damian Roßbach drei Minuten später vom Feld verwiesen wurde, bestimmten die Gastgeber den Rest des Spiels, während die Rostocker hinten Beton anrührten.
Dadurch ergaben sich für den Rest des Spiels aber keine größeren Torchancen mehr.

Nach Expected Goals gewinnen die Hanseaten kanpp mit 0,92:0,99.
Daraus ergibt sich ebenfalls ein ausgeglichenes Bild bei den Gewinnwahrscheinlichkeiten.
Die Verler gewinnen zu etwa 32% das Spiel, während die Rostocker zu fast 36% gewinnen. Die restlichen 32% der Spiele enden Unentschieden.
Die wahrscheinlichsten Ergebnisse sind 1:1 (16,7%), 0:1 (14,7%) und 1:0 (13,7%).
Mit 2:3 enden nur 0,9% der Spiele.