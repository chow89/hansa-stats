---
title: "Kurzanalyse 8. Spieltag: Eintracht Braunschweig gegen Hansa Rostock"
description: "Kurzanalyse zum 8. Spieltag 2019/20 zwischen Eintracht Braunschweig und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Eintracht Braunschweig, Aaron Opoku, Nico Neidhart, Nick Proschwitz, Martin Kobylanski, Korbinian Vollmann, Pascal Breier, Marcel Bär, Osman Atilgan]
categories: []
---

Für den FC Hansa ging es am achten Spieltag zum Tabellenführer der dritten Liga. Mit einer couragierten Leistung entschieden die Rostocker das Spiel gegen Eintracht Braunschweig für sich und gewannen mit 2:1.

<canvas id="xgplot081920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['8'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot081920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.EINTRACHT_BRAUNSCHWEIG,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Rostocker begannen sofort mit dem Anpfiff Druck auf die Braunschweiger Verteidigung auszuüben.
Die erste Chance von Aaron Opoku in der zweiten Minute blieb dabei noch ungenutzt.
Nur eine Minute später konnte Nico Neidhart mit einem sehenswerten Schuss von der Strafraumgrenze die Hanseaten mit 1:0 in Führung bringen.
In der Folge stabilisierten sich die Niedersachsen und so entwickelte sich ein ausgeglichenes Spiel mit Torchancen auf beiden Seiten (Nick Proschwitz, Martin Kobylanski, Korbinian Vollmann).
In der 35. Minute nutzten die Rostocker dann einen Konter ihrerseits und erhöhten durch Pascal Breier auf 2:0.
Nur wenige Minuten später konnten die Braunschweiger mit einem Tor von Marcel Bär wieder auf ein Tor Unterschied verkürzen.
In den letzten Minute vor der Halbzeit konnten sich bei Mannschaften nicht mehr entscheidend in Szene setzen, so dass es beim 2:1 blieb.
Zu diesem Zeitpunkt war der FC Hansa auch nach erwarteten Tore vorn und führte mit 0,74:0,64 xG.

Auch in der zweiten Halbzeit begann Hansa stark und die Braunschweiger brauchten einige Minuten um wieder ins Spiel zu kommen.
Allerdings sprang dabei nur ein Schussversuch von Vollmann raus, der aber vom Verteidiger geblockt wurde.
Es folgten wenig später weitere Chancen von Proschwitz für das Heimteam und erneut Vollmann für die Ostseestädter.
Danach beruhigte sich das Spiel in Folge mehrerer Wechsel auf Rostocker Seite etwas.
In der Schlussviertelstunde bauten die Niedersachsen aber nochmals ernormen Druck auf um zumindest den Ausgleichtreffer zu erzielen, konnten sich dabei aber nur zwei Kopfballchancen durch Proschwitz erspielen, die beide das Tor verfehlten.
Auf der Gegenseite kamen die Rostocker ihrerseites noch zu einer Chance von Osman Atilgan, die aber ebenso ungenutzt blieb.
Am Ende trennten sich beide Mannschaften mit 2:1 Toren und 1,61:1,22 xG.

Aus stochastischer Sicht ist der Sieg für die Rostocker auch verdient. Bei diesem Spielverlauf und der Mehrzahl an Torchancen gewinnt der FCH in 46,78% der Fällen, während die Braunschweiger nur in 26,74% der Fällen gewinnen, die restlichen 26,48% der Spiele enden unentschieden.
Das wahrscheinlichste Ergebnis bei einem solchen Spiel ist jedoch ein 1:1 (13,64%), dicht gefolgt vom 2:1-Sieg für Hansa (12,31%).