---
title: "Kurzanalyse 38. Spieltag: Chemnitzer FC gegen Hansa Rostock"
description: "Kurzanalyse zum 38. Spieltag 2019/20 zwischen Chemnitzer FC und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Chemnitzer FC, Nico Neidhart, Kai Bülow, Jakub Jakubov, Daniel Hanslik, Nikolas Nartey, Philipp Hosiner, Sven Sonnenberg, Max Reinthaler, Lukas Scherff, Mirnes Pepic, Sören Reddemann, Erik Engelhardt, Pascal Breier]
categories: []
---

Am letzten Spieltag der Saison ging es für den FC Hansa zum Chemnitzer FC.
In einer kuriosen Partie gewannen die Sachsen nach einer verrückten Schlussviertelstunde mit 4:2.

<canvas id="xgplot381920"></canvas>
<canvas id="winratio381920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['38'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot381920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.CHEMNITZER_FC,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio381920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften nutzten die Anfangsphase zum Abtasten und waren auf Spielkontrolle bedacht.
Durch die zunächst defensive Ausrichtung dauerte es bis zur 13. Minute ehe Nico Neidhart nach einem tiefen Pass von Kai Bülow vor dem Chemnitzer Tor auftauchte.
Der herausstürmende Chemnitzer Torwart Jakub Jakubov konnte Neidharts Schuss dann entschärfen.
Eine Viertelstunde später ereignete sich die gleiche Szene nochmal.
Dieses Mal schickte Daniel Hanslik Nikolas Nartey steil und wieder konnte Jakubov das Tor verhindern.
Der anschließende Kopfball von Hanslik wurde ebenfalls vom Chemnitzer Schlussmann pariert.
Auf der Gegenseite hatte Philipp Hosiner in der 34. Minute nach einem Fehler von Sven Sonnenberg die erste Torchance.
Hosiners Schuss ging knapp am Tor vorbei.
Beim Stand von 0:0 gingen beide Mannschaften dann in die Kabine.

Nach dem Wiederanpfiff richteten sich die Hausherren etwas offensiver aus und wurden dafür in der 50. Minute belohnt.
Der Schiedsrichter entschied auf Strafstoß nachdem Max Reinthaler Hosinser im Strafraum gefoult haben soll.
Der gefoulte Spieler trat selbst an und verwandelte zum 1:0.
Die Rostocker Reaktion ließ aber nicht lange auf sich warten.
Schon zwei Minuten nach dem Führungstreffer nutzte Lukas Scherff die Unordnung im Chemnitzer Strafraum zum 1:1.
Nur vier Minuten später hatten Neidhart, Scherff und Reinthaler verschiedene Möglichkeiten die Rostocker in Führung zu bringen.
Doch alle drei scheiterten entweder an Jakubov oder verfehlten das Tor deutlich.
In der 76. Minute hatte Mirnes Pepic die größte Chance zum Führungstreffer.
Er kam sieben Meter vor dem Tor ohne Bedrängnis an den Ball und hätte nur noch einschieben müssen.
Stattdessen spielte er erst den Torwart aus und so konnte ein Verteidiger nachrücken und den Ball schließlich auf der Linie klären.
Danach warfen die Gastgeber nochmal alles nach vorne (sie mussten mit vier Toren Vorsprung gewinnen um die Klasse zu halten) und erzielten in den nächsten Minuten drei Tore.
In der 79. Minute eröffnete Sören Reddemann nach einem Freistoß mit dem 2:1.
Vier Minuten vor Schluss legte Hosiner nach einer Ecke mit dem 3:1 nach und zwei Minuten erzielte er auch noch das 4:1.
Direkt danach konnte Erik Engelhardt aber den Traum der Chemnitzer beenden, als er sich durch die Chemnitzer Abwehr tankte und das 4:2 erzielte.
In der Nachspielzeit hatte Pascal Breier sogar noch die Chance um auf ein Tor zu verkürzen, doch auch er scheiterte mit einem Seitfallzeiher an Jakubov.
Den Schlusspunkt durfte der Dreifachtorschütze Hosiner setzen, der in der letzten Minute der Nachspielzeit bei einem Freistoß nochmal zum Kopfball kam, aber das Tor knapp verfehlte.
So endete das Spiel nach einer verrückten Schlussphase mit 4:2, wodurch die Rostocker ihre Mini-Chance auf den Relegationsplatz nicht nutzen konnten und die Chemnitzer ihrerseits den schweren Weg in die Regionalliga bestreiten müssen.

Ganz anders als das Ergebnis sieht es bei den Expected Goals aus.
Dabei gewinnen die Hanseaten deutlich mit 2,89:1,10.
Das bedeutet, dass die Rostocker in fast 79% aller Spiele gewinnen und in gut 13% der Spiele einen Punkt holen.
Die Chemnitzer gewinnen in 8% der Fälle.
Das wahrscheinlichste Ergebnis mit 11% Wahrscheinlichkeit ist ein 1:3-Sieg für die Hanseaten.
Mit 10% Wahrscheinlichkeit folgt ein Rostocker 1:2-Sieg.
Nur 0,4% der Spiele enden 4:2.