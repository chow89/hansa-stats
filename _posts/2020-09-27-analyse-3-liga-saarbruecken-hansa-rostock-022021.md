---
title: "Kurzanalyse 2. Spieltag: 1. FC Saarbrücken gegen Hansa Rostock"
description: "Kurzanalyse zum 2. Spieltag 2020/21 zwischen 1. FC Saarbrücken und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, 1. FC Saarbrücken, Sebastian Jacob, Markus Kolke, Pascal Breier, Maurice Deville, Damian Roßbach, Korbinian Vollmann, Nicklas Shipnoski]
categories: []
---

Im ersten Auswärtsspiel der Saison mussten die Rostocker zum am weitesten entfernten Ligakonkurrenten nach Saarbrücken.
Das Spiel gegen den Aufsteiger verloren die Hanseaten mit 2:0.

<canvas id="xgplot022021"></canvas>
<canvas id="winratio022021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['2'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot022021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_SAARBRÜCKEN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio022021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio022021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hausherren begannen furios und hatten nur wenige Sekunden nach dem Anpfiff die erste große Torchance.
Sebastian Jacobs Schuss von der Strafraumkante konnte von Markus Kolke aber problemlos aufgenommen werden.
Drei Minuten danach kam Jacob wieder vor dem Rostocker Gehäuse an den Ball, doch dieses Mal schießt er Pascal Breier, der mit dem Rücken zum Schützen steht, aus kurzer Distanz an den Arm.
Der Schiedsrichter entscheidet auf Elfmeter.
Im Duell mit Kolke bleibt Jacob letztlich Sieger und bringt die Gastgeber mit 1:0 in Führung.
Die Gäste fanden nach dem frühen Rückstand zwar besser ins Spiel und konnten sich einige Halbchancen erarbeiten, das aktivere Team blieben die Saarländer.
So war es in der 14. Minute Maurice Deville, der die nächste Chance hatte.
Auch er scheiterte an Kolke.
Nach einer halben Stunde hatte der FC Hansa seine erste Tormöglichkeit.
Breier trifft nach einem langen Solo über das halbe Spielfeld aber nur die Latte.
Sieben Minuten später hatte erneut Jacob alleine vor dem Tor die Möglichkeit zum Einnetzen, doch auch hier behielt Kolke die Oberhand.
Direkt vor dem Halbzeitpfiff hatten die Rostocker nach einer Ecke durch Damian Roßbach noch die Möglickeit zum Ausgleich.
Der Kopfball des Neuzugangs verfehlte das Tor deutlich.

Nach dem Pause erhöhten die Rostocker den Einsatz weiter und hatten in der 50. Minute die erste Chance des Spielabschnitts.
Korbinian Vollmanns Schuss ging jedoch knapp über das Tor.
In der Folge neutralisierten sich beide Mannschaften und erst in der 68. Minute wurde es wieder im Strafraum der Saarbrücker gefährlich.
Breiers Kopfball nach einem Eckball ging aber über das Tor.
In der 74. Minute war es erneut Breier, der aus ähnlicher Position nach einer Flanke wieder zum Kopfball kam, doch auch dieser ging über das Gehäuse.
Sieben Minuten vor Schluss erstickten die Hausherren dann aber die Hoffnungen der Gäste und erzielten das zweite Tor.
Die Gastgeber konnten sich ohne Gegenwehr durch eine unorganisierte Rostocker Abwehr passen und schlussendlich durch Nicklas Shipnoski auf 2:0 erhöhen.
Mir dem Schlusspfiff tauchte Shipnoski nach einem Konter erneut vor dem Rostocker Tor auf, doch bei diesem Schuss war der Saarbrücker nicht so treffsicher.

In der Expected Goals-Statistik gewinnen die Gastgeber knapp mit 1,78:1,31.
Das bedeutet, dass die Saarländer mit etwa 50% Wahrscheinlichkeit gewinnen.
Die Rostocker gewinnen nur mit 25% Wahrscheinlichkeit.
Ebenso hoch ist die Wahrscheinlichkeit für ein Unentschieden.
Mit 12,9% ist ein 2:1-Heimsieg das wahrscheinlichste Resultat.
Es folgen eine 1:1 mit 11,8% und 2:1 mit 8,7%.
Mit 8,3% Wahrscheinlichkeit endet das Spiel 2:0.