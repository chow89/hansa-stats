---
title: "Kurzanalyse 8. Spieltag: Hansa Rostock gegen Schalke 04"
description: "Kurzanalyse zum 8. Spieltag 2021/22 zwischen Hansa Rostock und Schalke 04"
tags: [2. Bundesliga, FC Hansa Rostock, Schalke 04, Simon Terodde]
categories: []
---

<canvas id="xgplot082122"></canvas>
<canvas id="winratio082122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['8'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot082122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SCHALKE_04,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio082122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio082122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>
