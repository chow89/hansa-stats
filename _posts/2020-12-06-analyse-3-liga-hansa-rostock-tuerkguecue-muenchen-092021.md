---
title: "Kurzanalyse 9. Spieltag: Hansa Rostock gegen Türkgücü München"
description: "Kurzanalyse zum 9. Spieltag 2020/21 zwischen Hansa Rostock und Türkgücü München"
tags: [3. Liga, FC Hansa Rostock, Türkgücü München, Björn Rother, Korbinian Vollmann, Bentley Baxter Bahn, Nico Gorzel, Maurice Litka, René Vollath, Aaron Berzel, Ünal Tosun, John Verhoek]
categories: []
---

Im Nachholspiel des 9. Spieltages empfingen die Rostocker mit Aufsteiger Türkgücü München den zweiten Club aus der bayerischen Landeshauptstadt binnen weniger Tage.
Nach einer kämpferischen starken Partie gewannen die Hanseaten mit 2:0.

<canvas id="xgplot092021"></canvas>
<canvas id="winratio092021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['9'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot092021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.TÜRKGÜCÜ_MÜNCHEN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio092021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio092021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Rostocker ließen von Beginn keinen Zweifel aufkommen, dass sie das Spiel unbedingt gewinnen wollen.
Gleich in der dritten Minute hatte Björn Rother die erste große Möglichkeit des Spiels.
Der defensive Mittelfeldspieler kam nach einer Flanke von Korbinian Vollmann mit dem Kopf an den Ball, verfehlte das Tor aber knapp.
Drei Minuten später tauchten die Heimherren erneut vor dem Tor auf.
Während der Schuss von Vollmann noch vom Torhüter abgewehrt wurde, konnte der Nachschuss von Bentley Baxter Bahn nur mit einem Foul verhindert werden.
Den anschließenden Elfmeter verwandelte der Gefoulte wie gewohnt sicher zum 1:0.
In der 22. Minute hatten die Aufsteiger ihre erste Torchance.
Die Münchener spielten Nico Gorzel im Strafraum frei, dessen Schuss dann aber noch geblockt werden konnte.
Vier Minuten später waren es wieder die Rostocker, die vor dem Tor auftauchten.
Maurice Litkas Schuss konnte aber von René Vollath im Münchener Tor zur Ecke geklärt werden.
Den anschließenden Eckstoß versenkte Aaron Berzel zum 2:0 ins eigene Tor.
Im restlichen Teil der Halbzeit passierte vor den Toren nichts mehr und so gingen die beiden Mannschaften mit dem Halbzeitstand von 2:0 in die Pause.

Nach dem Wiederanpfiff ging es genauso weiter wie vor dem Pausenpfiff und die Partie spielte sich ausschließlich zwischen den Strafräumen ab.
In der 70. Minute gab es dann doch noch einen Torschuss für die bayerischen Gäste.
Der Torschuss von Ünal Tosun landete aber in den Händen von Markus Kolke.
Drei Minuten vor Schluss hatten die Hanseaten nach einer Bahn-Ecke noch die Möglichkeit zum Erhöhen, doch der Kopfball von John Verhoek war für Vollath kein Problem.
So blieb es bis zum Abpfiff bei der 2:0-Führung für die Gastgeber.

Auch bei den Expected Goals gewinnen die Hanseaten das Spiel, wenn auch weniger klar, mit 0,83:0,42.
Daraus ergibt sich eine Gewinnwahrscheinlichkeit von fast 44% und eine Niederlagenwahrscheinlichkeit von 17%. 39% der Spiele enden unentschieden.
Das wahrscheinlichste Ergebnis bei diesem Spiel ist demnach ein 1:0-Heimsieg und ein 0:0-Unentschieden mit jeweils 25,4% Wahrscheinlichkeit.
Ein 2:0-Heimsieg tritt in 10% der Fällen ein.