---
title: "Kurzanalyse 6. Spieltag: FC Ingolstadt gegen Hansa Rostock"
description: "Kurzanalyse zum 6. Spieltag 2019/20 zwischen FC Ingolstadt und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, FC Ingolstadt, Aaron Opoku, Kai Bülow, Stefan Kutschke, Marco Königs, Nico Neidhart, Fabijan Buntic]
categories: []
---

Am 6. Spieltag ging es für die Rostocker zum Zweitligabasteiger und Tabellenführer nach Ingolstadt.
Mit einer großen Leistungssteigerung in der zweiten Halbzeit konnten die Hanseaten einen Punkt mit nach Hause nehmen.

<canvas id="xgplot061920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['6'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot061920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_INGOLSTADT,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die erste Halbzeit war geprägt von vielen Aktionen im Mittelfeld und wenigen Torchanchen.
Außer dem Elfmeter in der 8. Minute, den Stefan Kutschke sicher verwandelte, gab es keine nennenswerten Torchancen, so dass es mit einem 1:0 und 0,1:0 xG in die Halbzeit ging.

In der zweiten Halbzeit spielte die Elf von Jens Härtel von Beginn an etwas offensiver und wurde schon in der 50. Minute dafür belohnt.
Aaron Opoku dribbelte sich von der rechten Seite kommend durch die Ingolstadter Hintermannschaft und schoss den Ball am Torwart vorbei zum 1:1 ins Netz.
Anschließend versuchten die Bayern wieder mehr Druck auf die Rostocker Abwehr auszuüben, konnten aber bis auf einen Schuss von Stefan Kutschke von der Strafraumkante keine erfolgsversprechenden Chancen mehr verzeichnen.
Unterdessen versuchte der FC Hansa mit zwei frischen Stürmern von der Bank das Spiel komplett zu drehen.
So war es in der 73. Minute der eben eingewechselte Marco Königs, der die erste Möglichkeit aus kurzer Distanz vergab.
In der 86. Minute gelang dann Kai Bülow nach einer Pepic-Ecke, die inzwischen verdiente Führung.
Nico Neidhart, der sein Pflichtspieldebüt im Trikot der Rostocker gab, hatte in der Nachspielzeit noch die Chance auf 3:1 zu erhöhen, scheiterte aber am glänzend aufgelegten Ingolstädter Torwart Fabijan Buntic.
Im Gegenzug kamen die Oberbayern nochmals vor das Rostocker Gehäuse, in dessen Folge der Elfmeter zum 2:2 resultierte.
Am Ende stand es nach xG 0,3:0,85.