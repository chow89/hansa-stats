---
title: "Kurzanalyse 14. Spieltag: Carl Zeiss Jena gegen Hansa Rostock"
description: "Kurzanalyse zum 14. Spieltag 2019/20 zwischen Carl Zeiss Jena und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Carl Zeiss Jena, Anton Donkor, Sven Sonnenberg, Nik Omladic, Ole Käuper, Dominik Bock, John Verhoek]
categories: []
---

Nach acht ungeschlagenen Spielen in Folge kassierten die Hanseaten beim Tabellenletzten aus Jena wieder eine Niederlage.
Nach 90 Minuten stand es 3:1 für die Hausherren, die damit ihren ersten Saisonsieg feierten.

<canvas id="xgplot141920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['14'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot141920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.CARL_ZEISS_JENA,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

In einer insgesamt sehr ausgeglichenen ersten Halbzeiten taten sich beide Mannschaften schwer gefährlich vor das Tor zu kommen.
Zwar konnten sich beide Teams hin und wieder Torchancen erspielen, diese waren aufgrund der großen Distanz zum Tor oder dem zu spitzen Winkel aber keine ernsthafte Gefahr.
Nichtsdestotrotz konnte der ehemalige Hanseat Anton Donkor in der 43. Minute den Ball aus wenig aussichtsreicher Position in das Tor befördern.
Somit ging es mit 1:0 in die Halbzeit.
Zu diesem Zeitpunkt stand es nach expected Goals 0,45:0,39 für die Gastgeber.

Mit dem Wiederanpfiff richteten sich die Hanseaten offensiver aus und versuchten sogleich den Rückstand auszugleichen.
Dieser gelang dann auch mit der ersten Chance in der 54. Minute.
Innenverteidiger Sven Sonnenberg schob den Ball nach einer flachen Hereingabe von Nik Omladic in die Maschen.
Dabei blieb es aber nicht lange.
Nur sechs Minuten später stellte Ole Käuper wieder die Führung für die Jenaer her.
Weitere acht Minuten später sorgte Dominik Bock mit dem Tor zum 3:1 für die Vorentscheidung.
Die Rostocker versuchten anschließend zumindest noch ein Unentschieden zu erreichen, blieben bis auf eine Möglichkeit von John Verhoek in der 79. Minuten jedoch chancenlos.
So blieb es bis zum Abpfiff bei der 2-Tore-Führung für die Gastgeber.
Nach expected Goals lagen die Rostocker mit 1,16:1,04 aber knapp vorne.

In der stochastischen Betrachtung ist das Spiel somit auch ausgeglichen.
In 38,49% der Fälle gewinnen die Rostocker das Spiel, während die Jenaer mit einer Wahrscheinlichkeit von 31,63% gewinnen.
29,87% ist die Wahrscheinlichkeit für ein Unentschieden.
Dementsprechend ist auch ein 1:1 mit 15,44% das wahrscheinlichste Ergebnis, es folgen ein 1:0-Sieg für die Gäste mit 13,04% und ein Heimsieg mit gleichem Ergebnis mit 10,94%.
Ein 3:1-Heimsieg ereignet sich nur mit 2,43% Wahrscheinlichkeit.