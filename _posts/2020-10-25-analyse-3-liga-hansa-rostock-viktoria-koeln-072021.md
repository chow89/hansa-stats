---
title: "Kurzanalyse 7. Spieltag: Hansa Rostock gegen Viktoria Köln"
description: "Kurzanalyse zum 7. Spieltag 2020/21 zwischen Hansa Rostock und Viktoria Köln"
tags: [3. Liga, FC Hansa Rostock, Viktoria Köln, Julian Riedel, Maurice Litka, Max Reinthaler, Sebastian Mielitz, Bentley Baxter Bahn, John Verhoek, Korbinian Vollmann, Nico Neidhart, Mike Wunderlich]
categories: []
---

Zum Spitzenspiel des 7. Spieltages empfing der FC Hansa die Viktoria aus Köln.
Durch einen furiosen Auftritt gewannen die Rostocker mit 5:1 und sicherten sich vorerst die Tabellenführung.

<canvas id="xgplot072021"></canvas>
<canvas id="winratio072021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['7'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot072021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.VIKTORIA_KÖLN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio072021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio072021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Von Beginn an waren die Hanseaten in allen Belangen überlegen.
Dennoch dauerte es bis zur 9. Minute ehe Julian Riedel die erste Chance des Spiels hatte.
Den Freistoß von Maurice Litka konnte er noch gerade so mit der Fußspitze erreichen, das war aber nicht genug, um den Ball auf das Tor zu bringen.
In der 23. Minute hatte Max Reinthaler die nächste Möglichkeit zur Führung, doch sein gut positionierter Kopfball nach einer Litka-Ecke konnte von Sebastian Mielitz im gegnerischen Tor gehalten werden.
Nur eine Minute später klingelte es dann aber doch im Kölner Tor.
Durch das hohe Pressing der Rostocker verlor die Kölner Abwehr im Spielaufbau den Ball und die Rostocker konnte über Reinthaler und Riedel den Ball auf Bentley Baxter Bahn ablegen, der dann ähnlich wie beim [Spiel gegen 1860 München]({% post_url 2020-10-18-analyse-3-liga-hansa-rostock-1860-muenchen-052021 %}) von der Strafraumkante einnetzen konnte.
Nur 90 Sekunden später erhöhte der FC Hansa auf 2:0.
Dem Tor von John Verhoek ging ein langes Solo von Litka und eine präzise Flanke von Bahn auf den Kopf des Stürmers voraus, der dann einfach einnicken konnte.
Zehn Minuten später setzten die Hausherren zum Konter an, der über Litka und Korbinian Vollmann bei Nico Neidhart landete.
Seinen Volleyschuss konnte Mielitz dann aber mit einer Parade klären.
Eine weitere Minute später kamen die Rostocker wieder durch das kluge Pressing im Angriffsdrittel an den Ball.
Die anschließende Flanke von Neidhart landete beim völlig freistehenden Verhoek, der den Ball jedoch nicht richtig traf und somit kein Problem für den Torwart war.

Nach dem Seitenwechsel setzte sich die Dominanz der Gastgeber fort.
Nur drei Minuten nach dem Wiederanpfiff brachte Litka eine Ecke auf den Kopf von Verhoek.
Den Kopfball konnte Mielitz noch klären, der Ball landete aber direkt vor Riedel, der die Kugel dann über die Linie drückte.
Es war sein erstes Tor im 165. Drittliga-Spiel.
Wieder dauerte es nur 90 Sekunden ehe Verhoek das nächste Tor erzielte.
Die Flanke von Vollmann nahm der Niederländer ohne große Gegenwehr im Strafraum an und spitzelte den Ball danach zum 4:0 ins Tor.
Nach einer Stunde kam der Stürmer nach einer flachen Hereingabe erneut frei vor dem Tor an den Ball.
Dieses Mal konnte der Torwart allerdings mit dem Fuß parieren.
In der 64. Minute fingen die Rostocker dann an zu zaubern.
Wieder war es eine Hereingabe von Vollmann, die bei Bahn ankam.
Dieser legten per Hacke auf Verhoek ab, der dann vergeblich versuchte vier Gegenspieler auszuspielen.
Sein Schuss wurde dann geblockt.
Fünf Minuten später tauchte Vollmann nach einem tiefen Pass von Litka frei vor dem gegnerischen Tor auf und verwandelte zum 5:0.
Er setzte damit den Schlusspunkt der Rostocker Angriffsbemühungen.
Auf der Gegenseite erlaubten sich die Hanseaten in der 80. Minute dann aber doch eine Unkonzentriertheit, als Riedel Mike Wunderlich im Strafraum von den Beinen holte.
Den anschließenden Elfmeter verwandelte der Gefoulte souverän.
Dabei blieb es dann auch bis zum Ende des Spiels.

Nach Expected Goals gewinnen die Hanseaten deutlich mit 3,31:0,02;
Das bedeutet, dass die Rostocker mit 99%iger Wahrscheinlichkeit gewinnen.
Die Wahrscheinlichkeit das Köln so ein Spiel gewinnt liegt bei etwa 0,01%, die restlichen rund 0,97% entfallen auf ein Unentschieden.
Die wahrscheinlichsten Ergebnisse sind 3:0 (27,3%), 4:0 (22,3%) und 2:0 (20,8%).
Mit 5:1 enden nur etwa 0,2% der Spiele.