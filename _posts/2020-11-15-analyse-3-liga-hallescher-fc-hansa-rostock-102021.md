---
title: "Kurzanalyse 10. Spieltag: Hallescher FC gegen Hansa Rostock"
description: "Kurzanalyse zum 10. Spieltag 2020/21 zwischen Hallescher FC und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Hallescher FC, Michael Eberwein, Jan Löhmannsröben, Terrence Boyd, Bentley Baxter Bahn, Jonas Nietfeld, Max Reinthaler, Stipe Vucur, Markus Kolke, Sven Müller, Nico Neidhart, Korbinian Vollmann]
categories: []
---

Nach der Corona-bedingenten Pause ging es für den FC Hansa am 10. Spieltag zum Halleschen FC.
Die umkämpfte Partie endete mit einem 1:1-Unentschieden.

<canvas id="xgplot102021"></canvas>
<canvas id="winratio102021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['10'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot102021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.HALLESCHER_FC,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio102021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Der erste Spielabschnitt begann ausgeglichen, aber ohne Torchancen in der Anfangsphase.
In der elften Minute tauchten dann aber die Gastgeber zum ersten Mal vor dem Tor der Rostocker auf.
Michael Eberwein konnte in Folge eines Fehlers im Aufbauspiel von Jan Löhmannsröben frei vor dem Tor mit dem Kopf an den Ball kommen.
Dem Kopfball fehlte es aber an Präzision und ging deshalb am Gehäuse vorbei.
Eine Viertelstunde später konnten die Hallenser wieder in den Strafraum eindringen und Kapital daraus schlagen.
Terrence Boyd zog nach einem Steilpass in den Lauf sofort ab und erzielte so das 1:0.
Der FC Hansa zeigt sich vom Rückstand wenig geschockt und versuchte nach dem Tor offensiv mehr Druck zu entwickeln.
Zehn Minuten nach dem Tor konnten die Hanseaten durch Bentley Baxter Bahn ihre erste Torchance verbuchen.
Sein Schuss konnte aber noch vom Hallenser Kapitän Jonas Nietfeld geblockt werden.
Zwei Minuten später hatte Max Reinthaler bei einem Eckstoß die nächste Möglichkeit, doch sein Kopfball ging ein gutes Stück über das Tor.
Wenige Minuten vor dem Halbzeitpfiff hatten die Gastgeber mit zwei Ecken nochmal die Chance zu erhöhen.
In beiden Fällen landete der Ball auf dem Kopf von Stipe Vucur, der dann aber in Markus Kolke seinen Meister fand.
In der Nachspielzeit der ersten Halbzeit hatten die Rostocker erneut nach einer Ecke auf Reinthaler die nächste Ausgleichsmöglichkeit.
Dieses Mal landete der Ball aber in den Händen von Sven Müller im Hallenser Tor.

Die Rostocker fanden nach der Halbzeit noch besser ins Spiel und wurden dafür in der 51. Minute belohnt.
Nach einer Flanke von Nico Neidhart kam Korbinian Vollmann mit dem Kopf an den Ball, der dann über Torwart Müller zum 1:1 in das Tor flog.
Nach dem Tor begann das Spiel zunehmend abzuflachen und hatte in der 63. Minute das letzte Highlight.
Erneut war es Boyd, der nach einer Hereingabe frei an den Ball kam.
Sein Schuss ging aber über das Tor.
So blieb es bis zum Abpfiff beim 1:1-Unentschieden.

Nach Expected Goals gewinnen die Rostocker das Spiel knapp mit 1,14:0,95.
Das bedeutet, dass die Gäste 40% der Spiele gewinnen und in 31% der Spiele die Punkte geteilt werden.
29% der Spiele gewinnen die Hausherren.
Das 1:1 ist mit fast 17% Wahrscheinlichkeit auch der häufigste Spielausgang.
Mit knappen 15% Wahrscheinlichkeit folgt ein 1:0-Auswärtssieg und danach ein 1:0-Heimsieg mit 11%.