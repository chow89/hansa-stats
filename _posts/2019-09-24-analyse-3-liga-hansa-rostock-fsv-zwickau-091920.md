---
title: "Kurzanalyse 9. Spieltag: Hansa Rostock gegen FSV Zwickau"
description: "Kurzanalyse zum 9. Spieltag 2019/20 zwischen Hansa Rostock und FSV Zwickau"
tags: [3. Liga, FC Hansa Rostock, FSV Zwickau, Korbinian Vollmann, Pascal Breier, Davy Frick, Ronny König, Aaron Opoku, Fabio Viteritti, Nico Rieble, Johannes Brinkies, Rasmus Pedersen, John Verhoek]
categories: []
---

Nach zwei Siegen in Folge gelang den Rostockern am neunten Spieltag gegen den FSV Zwickau nur ein Unentschieden, das Spiel endete 1:1.

<canvas id="xgplot091920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['9'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot091920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FSV_ZWICKAU,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Rostocker begannen wie schon in den letzten Spielen sehr offensiv und konnten sich nur wenige Sekunden nach Anpfiff die erste große Chance herausspielen, die allerdings von Korbinian Vollmann nicht genutzt werden konnte.
Der anschließende Einwurf landete dann nach unfreiwiliiger Verlängerung der Zwickauer Abwehr vor den Füßen von Pascal Breier, der aus kurzer Distanz keine Schwierigkeiten hatte, den Ball über die Torlinie zu schießen und die Hanseaten mit 1:0 in Führung brachte.
In den nächsten Minute fanden die West-Sachsen dann besser ins Spiel und erspielten sich ihrerseite durch Davy Frick und Ronny König zwei Torchancen.
Anschließend verflachte das Spiel, ehe Aaron Opoku in der 40. Minute nochmal in aussichtsreicher Position vor das Tor kam, den Kopfball aber nicht platzieren konnte.
Mit dem Stand von 1:0 und 0,52:0,44 xG ging es somit in die Halbzeitpause.

Die zweite Halbzeit begann dann mit leichten Vorteilen für die Zwickauer.
Und so nutzten die Gäste durch Fabio Viteritti in der 50. Minute ihre erste Chance der Halbzeit zum 1:1-Ausgleich.
Nach dem Treffer entwickelte sich ein ausgeglichenes Spiel mit leichtem Ballbesitzvorteil der Zwickauer.
Es ergaben sich auf beiden Seiten nur wenige Tormöglichkeiten, am aussichtsreichsten war dabei nur ein Kobfball von Nico Rieble in der 58. Minute, den Torwart Johannes Brinkies aber problemlos aufnehmen konnte.
In der Schlussviertelstunde versuchten beide Mannschaften nochmal den Siegtreffer zu erzielen.
Den Anfang machte Rasmus Pedersen in der 74. Minute, der aber ebenso am Zwickauer Torwart scheiterte.
Es folgten weitere Chancen für Viteritti in der 78. Minute und John Verhoek in der 82. Minute, aber auch ihnen gelang nicht der siegbringende Treffer.
So blieb es am Ende beim 1:1 und einem xG-Verhältnis von 1,35:0,91.

Stochastisch gesehen war das 1:1 auch das wahrscheinlichste Ergebnis (15,22%). Ebenso wahrscheinlich war auch ein 1:0-Sieg für die Hanseaten (14,4%). Mit etwas Abstand folgt ein 2:1 (10,8%) und ein 2:0 (10,26%).
Insgesamt gewinnen die Rostocker in 47,97% der Fällen so ein Spiel und holen in 28,53% der Fällen einen Punkt. Die Gäste gewinnen in 23,5% der Fällen.