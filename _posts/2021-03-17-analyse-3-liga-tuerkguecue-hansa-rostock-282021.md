---
title: "Kurzanalyse 28. Spieltag: Türkgücü München gegen Hansa Rostock"
description: "Kurzanalyse zum 28. Spieltag 2020/21 zwischen Türkgücü München und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, Türkgücü München, Alexander Sorge, Sebastian Maier, Manuel Farrona Pulido, Lukas Scherff, Boubacar Barry, Bentley Baxter Bahn, Tobias Schwede, Aaron Herzog, Nik Omladic]
categories: []
---

Zum Abschluss des 28. Spieltages traten die Rostocker bei Türkgücü München an.
Die Hanseaten gewannen das Spiel deutlich, aber nicht ungefährdet mit 3:0. 

<canvas id="xgplot282021"></canvas>
<canvas id="winratio282021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['28'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot282021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.TÜRKGÜCÜ_MÜNCHEN,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio282021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio282021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die bayerischen Hausherren fanden sofort gut in das Spiel und erspielten sich in den ersten drei Minuten eine Vielzahl an Torchancen.
Besonders hervorzuheben sind dabei der Kopfball von Alexander Sorge nach einer Ecke, sowie der Schuss von Sebastian Maier aus kurzer Distanz.
Beide Versuche fanden aber nicht ihren Weg in das Tor.
Die Rostocker tauchten in der fünften Minute erstmals vor dem gegnerischen Tor auf.
Doch auch Manuel Farrona Pulido scheiterte mit seinem Schuss nach einer Hereingabe von Lukas Scherff.
Nach der ereignisreichen Anfangsphase wurde es erstmal etwas ruhiger ehe die Gastgeber in der 14. Minute zu ihrer nächsten guten Möglichkeit kamen.
Aber auch Boubacar Barry konnte seine Chance aus einem Konter heraus nicht krönen.
Mit zunehmender Spielzeit fanden die Rostocker immer besser in das Spiel und kontrollierten das Spiel und das Spieltempo.
Es dauerte dann aber noch bis zum Ende der ersten Halbzeit, ehe es nochmal eine gute Torchance gab.
Wieder war es Farrona Pulido, der nach einer Ecke von Bentley Baxter Bahn komplett frei zum Schuss kam und zur 1:0-Führung verwandelte.

Nach der Pause setzten die Hanseaten ihr kontrolliertes Offensivspiel weiter fort und hatten nur drei Minuten nach dem Wiederanpfiff die nächste gute Möglichkeit.
Scherffs Dropkick ging aber zu zentral auf das Tor und war somit kein großes Problem für den Münchener Torwart.
In der 59. Minute machte es Bahn dann besser.
Nachdem er von Farrona Pulido im Strafraum angespielt wurde und seinen Gegenspieler ausstiegen ließ, schlenzter er den Ball zum 2:0 in das Tor.
Die Rostocker schalteten anschließend einen Gang zurück und fokussierten sich mehr auf die Abwehrarbeit.
Dadurch blieb es in beiden Strafräumen lange weitestgehend ruhig.
Erst in der 88. Minute sorgten die Rostocker wieder für Aufregung als eine Ecke von Bahn bei Tobias Schwede landete.
Seine Direktabnahme ging aber direkt auf den Torwart und stellte keine größere Herausforderung für diesen dar.
In der Nachspielzeit gelang dem FC Hansa aber doch noch das 3:0.
Nach einem Steilpass von Aaron Herzog lief Nik Omladic nur von einem Verteidiger begleitet auf das Tor zu und verwandelte anschließend zum Endstand.
Kurz darauf pfiff der Schiedsrichter das Spiel ab.

Aufgrund des starken Beginns gewinnen die Münchener das Spiel nach Expected Goals mit 1,06:1,39.
Daraus ergibt sich für die Gastgeber eine Siegwahrscheinlichkeit von fast 45%, während die Rostocker nur etwa 26% der Spiele gewinnen.
29% aller Spiele enden Unentschieden.
Entsprechend der Expected Goals ist ein 1:1 das häufigste Ergebnis bei diesem Spiel mit 15,8% Wahrscheinlichkeit.
Dahinter folgen ein 2:1-Heimsieg (11,9%) und eine 1:0-Heimsieg (11,8%).
Auf ein 3:0-Auswärtssieg entfallen nur 1,1%.
