---
title: "Kurzanalyse 24. Spieltag: Sonnenhof Großaspach gegen Hansa Rostock"
description: "Kurzanalyse zum 24. Spieltag 2019/20 zwischen Sonnenhof Großaspach und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, SG Sonnenhof Großaspach, Eric Hottmann, Daniel Hanslik, Lukas Scherff, Orrin McKinze Gaines, John Verhoek, Nico Neidhart, Aaron Opoku, Julian Leist, Mirnes Pepic, Panagiotis Vlachodimos]
categories: []
---

Nach zwei sieglosen Spielen fanden die Rostocker am 24. Spieltag wieder in die Erfolgsspur zurück.
Das Gastspiel bei der Sonnenhof Großaspach gewann das Team von Jens Härtel knapp mit 1:0.

<canvas id="xgplot241920"></canvas>
<canvas id="winratio241920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['24'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot241920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SONNENHOF_GROSSASPACH,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio241920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften hatten mit Spielbeginn wenig Anfangsschwierigkeiten und versuchten gleich offensive Akzente zu setzen.
Den etwas besseren Eindruck hinterließen zunächst die Gastgeber, die sogleich in der neunten Minute nach einer Ecke zu ihrer ersten großen Chance kamen.
Eric Hottmanns Schuss verfehlte das Tor knapp.
Sechs Minuten später konnten die Hanseaten dann auch ihre erste gute Torchance verbuchen.
Daniel Hanslik traf den Ball jedoch nicht voll und so ging der Ball ein gutes Stück am Tor vorbei.
Im direkten Gegenzug tauchten die Aspacher wieder vor dem Rostocker Tor auf.
Lukas Scherff konnte den Schuss von Orrin McKinze Gaines aus guter Position aber blocken.
In der 21. Minute hatten die Kicker von der Ostsee erneut die Möglichkeit zur Führung und diesesmal nutzten sie sie auch.
John Verhoek veredelte einen gut herausgespielten Angriff über Nico Neidhart und Aaron Opoku zum 1:0.
Die Hausherren reagierten darauf keineswegs geschockt und hatten in der 25. Minute schon die Chance zum Ausgleich.
Erneut war es McKinze Gaines, der die gesamte Rostocker Abwehr mit seiner Schnelligkeit überlief und schließlich nur am Pfosten scheiterte.
Einige Minuten später hatte Verhoek nach einer sehr schönen Vorlage von Opoku die Chance auf sein zweites Tor.
Seine artistischer Heber über den Torwart ging aber am Tor vorbei.
Bis zur Halbzeitpause passierte anschließend nichts mehr und so ging es mit der 1:0-Führung der Gäste in die Kabinen.

Im zweiten Spielabschnitt waren die Rostocker zunächst das spielbestimmende Team.
Die erste Chance hatte wieder Verhoek, der einen langen Ball von Scherff am Fünfmeterraum annahm und direkt abschloss.
Julian Leist blockte diesen zur Ecke ab.
Auf der Gegenseite spielten sich die Hausherren ein ähnliche Chance heraus, der Kopfball von Hottmann ging dabei deutlich über das Tor.
Eine Viertelstunde vor Schluss hatten die Ostseestädter ihre letzte Großchance zur Vorentscheidung, aber auch die Direktabnahme von Mirnes Pepic verfehlte das Tor.
Den Abschluss des Spiels hatten nochmals die Aspacher in Person von Panagiotis Vlachodimos.
Aber auch sein Schuss ging nicht in das Tor und so trennten sich die beide Teams mit 1:0 für die Hanseaten.

Nach expected Goals endet das Spiel mit 1,17:1,19 quasi Unentschieden.
Dementsprechend sind auch die möglichen Spielausgänge fast ausgewogen.
Etwa 35% der Spiele gewinnen die Rostocker und 34% der Spiele gewinnen die Aspacher.
Die restlichen ungefähr 30% der Spiele enden mit einer Punkteteilung.
Ähnlich ausgeglichen sind die erwarteten Ergbnisse.
Mit 17% ist ein 1:1 am wahrscheinlichsten.
Dahinter folgen 1:0 (11%), 1:2 (11%), 2:1 (10%) und 0:1 (20%).