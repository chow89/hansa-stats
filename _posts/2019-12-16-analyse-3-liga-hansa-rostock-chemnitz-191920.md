---
title: "Kurzanalyse 19. Spieltag: Hansa Rostock gegen Chemnitzer FC"
description: "Kurzanalyse zum 19. Spieltag 2019/20 zwischen Hansa Rostock und Chemnitzer FC"
tags: [3. Liga, FC Hansa Rostock, Chemnitzer FC, Korbinian Vollmann, Pascal Breier, Philipp Hosiner, Matti Langer, Mirnes Pepic, Erik Tallig, John Verhoek]
categories: []
---

Auch am letzten Spieltag der Hinrunde konnten sich die Rostocker nicht durchsetzen und verloren ihr Heimspiel gegen den Chemnitzer FC mit 1:2.

<canvas id="xgplot191920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['19'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot191920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.CHEMNITZER_FC,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften versuchten von Beginn an die Spielkontrolle zu gewinnen und schnell in Führung zu gehen.
Dadurch entwickelte sich anfangs ein offenes Spiel mit Chancen auf beiden Seiten.
So hatten auf Rostocker Seite zunächst Korbinian Vollmann (3.) und Pascal Breier (9., 23.) und auf Chemnitzer Seite Philipp Hosiner (8., 20.) und Matti Langer (21.) Möglichkeiten für einen frühen Führungstreffer, den aber keinen der Genannten gelang.
Auf das erste Tor des Spiels mussten die Zuschauer dennoch nicht lange warten.
In der 25. Minute nutzte Breier eine Hereingabe von Mirnes Pepic für das 1:0.
Danach verflachte das Spiel und die Hanseaten versuchten die Führung zur verwalten.
Bis kurz vor dem Halbzeitpfiff gelang das auch, ehe die Chemnitzer am Strafraum ein Freistoß zugesprochen bekamen.
Diesen nutze Hosiner zum 1:1-Ausgleich.
Direkt danach ging es für beide Mannschaften in die Kabine.

Nach dem Wiederanpfiff kamen die Gäste aus Sachsen besser ins Spiel zurück und konnten in der 49. Minute durch den eingewechselten Erik Tallig den Treffer zum 1:2 erzielen.
Es folgte den Rest der Halbzeit ein ausgeglichenes Spiel ohne weitere Torchance.
Erst in der 89. Minute versuchte der FC Hansa sich noch gegen die Niederlage zu wehren.
Ein Kopfball von John Verhoek nach einer Ecke verfehlte das Tor der Himmelblauen aber deutlich und so blieb es bis zum Schluss beim 1:2-Auswärtssieg der Chemnitzer (1:36:1,92 xG).

In der stochstischen Betrachtung ist ebenfalls der CFC vorne.
Demnach gewinnen die Gäste 52,19% aller Partien und holen in 25,93% aller Spiele zumindest einen Punkt.
Die Rostocker gewinnen nur mit 21,88% Wahrscheinlichkeit.
Nicht ganz so deutlich ist das Verhältnis bei den Ergebnis.
Zwar ist das 1:2 mit 15,09% der wahrscheinlichste Spielausgang, danach folgen aber schon ein 1:1 (11,43%) und 2:2 (11,17%), gefolgt vom 2:1-Heimsieg (8,46%).
