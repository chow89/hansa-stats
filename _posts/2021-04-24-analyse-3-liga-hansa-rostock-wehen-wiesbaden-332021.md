---
title: "Kurzanalyse 33. Spieltag: Hansa Rostock gegen SV Wehen Wiesbaden"
description: "Kurzanalyse zum 33. Spieltag 2020/21 zwischen Hansa Rostock und SV Wehen Wiesbaden"
tags: [3. Liga, FC Hansa Rostock, SV Wehen Wiesbaden, Bentley Baxter Bahn, Erik Engelhardt, Aaron Herzog, Phillip Tietz, Markus Kolke, Florian Carstens]
categories: []
---

Die Rostocker begrüßten am 33. Spieltag den SV Wehen Wiesbaden im Ostseestadion.
Die chancenarme Partie endete schließlich leistungsgerecht mit 1:1.

<canvas id="xgplot332021"></canvas>
<canvas id="winratio332021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['33'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot332021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.WEHEN_WIESBADEN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio332021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio332021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Im ersten Spielabschnitt waren die Gäste aus Hessen die aktivere Mannschaft.
Sie spielten sich auch die größere Anzahl an Torchancen heraus, besonders hervorzuheben ist von diesen aber keine.
Im Gegensatz dazu fanden die Hanseaten bis auf einen Torschuss offensiv überhaupt nicht statt.

Nach der Pause änderte sich das dann aber.
Nur fünf Minuten nach dem Wiederanpfiff bekamen die Rostocker einen umstrittenen Handelfmeter zugesprochen, den Bentley Baxter Bahn souverän zur 1:0-Führung verwandelte.
In der 67. Minute hatte Erik Engelhardt dann schon das nächste Tor auf dem Fuß.
Sein Schuss konnte aber noch geblockt werden.
Zehn Minuten vor dem Ende der regulären Spielzeit dribbelte sich Aaron Herzog vom rechten Flügel in den Strafraum und ließ dabei drei Gegenspieler aussteigen, scheiterte dann aber am Vierten, der seinen Schuss blockte.
Vier Minuten später hatten auch die Gäste ihre erste gute Möglichkeit.
Phillip Tietz zwang Markus Kolke mit einem Freistoß zu einer Parade.
In der zweiten Minute der Nachspielzeit hatte Herzog dann die Chance zur Entscheidung.
Er lief frei auf den Wehener Torwart zu, zeigte dann aber Nerven und schoss über das Tor.
Dieses verpasste Tor sollte sich in der allerletzten Minute rächen.
Nach einem Freistoß bekamen die Hanseaten den Ball nicht aus dem Strafraum raus und der Ball landete irgendwann vor den Füßen von Florian Carstens, der von der Strafraumkante abzog und das 1:1 erzielte.

Nach Expected Goals endet das Spiel mit 0,73:0,68 ebenso Unentschieden.
Dabei entfallen fast 33% Wahrscheinlichkeit auf einen Rostocker Sieg und 38% auf eine Punkteteilung.
Die restlichen 29% der Spiele gewinnen die Gäste.
Mit 21,3% Wahrscheinlichkeit endet das Spiel ohne Tore.
Dahinter folgen ein 1:0-Heimsieg (19,3%), ein 0:1-Auswärtserfolg (16,8%) und das 1:1-Unentschieden (15,2%).