---
title: "Kurzanalyse 37. Spieltag: SpVgg Unterhaching gegen Hansa Rostock"
description: "Kurzanalyse zum 37. Spieltag 2020/21 zwischen SpVgg Unterhaching und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, SpVgg Unterhaching, Stephan Hain, Markus Kolke, Nico Neidhart, John Verhoek, Philip Türpitz, Markus Schwabl, Moritz Heinrich, Niclas Anspach, Nik Omaldic, Sven Sonnenberg]
categories: []
---

Am vorletzten Spieltag mussten die Rostocker die lange Fahrt zum Auswärtsspiel in Unterhaching antreten.
Die ausgeglichene Partie konnten die Hanseaten durch ihre gute Defensivarbeit mit 0:1 gewinnen.

<canvas id="xgplot372021"></canvas>
<canvas id="winratio372021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['37'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot372021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SPVGG_UNTERHACHING,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio372021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio372021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hanseaten fanden gut in das Spiel und bestimmten dieses von der ersten Minuten an.
Die Hausherren schafften es in der Anfangsphase nur selten den Ball aus der eingenen Hälfte herauszuspielen.
Die erste gute Torchance ging dennoch auch das Konto der Bayern.
Aus einem Konter heraus landete der Ball in der 17. Minuten bei Stephan Hain, der noch zwei Verteidiger ausstiegen ließ, dann aber an Markus Kolke im Tor scheiterte.
In der 25. Minute machten die Rostocker es auf der Gegenseite besser.
Nico Neidhart flankte von der rechten Seite auf John Verhoek, der den Ball auf Philip Türpitz ablegte.
Der Mittelfeldspieler zog sofort von der Strafraumkante ab und erzielte so das 0:1.
Nach der Führung zogen sich die Hanseaten etwas zurück und ließen die Hachinger mehr in das Spiel kommen.
Die Hausherren konnten sich so in der 29. Minute ihre nächste Chance erarbeiten.
Eine Flanke von Markus Schwabl landete nur wenige Meter vor dem Tor auf dem Kopf von Moritz Heinrich, der aber direkt in die Arme von Kolke köpfte.
Fünf Minuten vor dem Halbzeitpfiff hatte Heinrich aus ähnlicher Distanz die nächste Möglichkeit.
Seinen Schuss mit der Hacke konnte Neidhart aber noch vor der Torlinie klären.
In der Nachspielzeit probierte sich nochmal Hain mit einem Drehschuss, aber auch diesen konnte Kolke festhalten.

Nach der Pause machte die Gastgeber sofort weiter Druck und hatten nur wenigen Sekunden nach dem Wiederanpfiff die nächste große Torchance.
Niclas Anspach wurde im Strafraum gut freigespielt, schoss bei seinen beiden Versuchen aber beides Mal seinen eigenen Mitspieler an.
Auf der Gegenseite gab es erst in der 77. Minute die nächste Möglichkeit für die Gäste.
Nik Omaldic spielte sich mit einem Solo bis an der Strafraum, setzte seinen Schuss aber dann zu hoch an, so dass der Ball über das Tor flog.
Nur drei Minuten später hatte Sven Sonnenberg nach einem Freistoß von Aaron Herzog die nächste Möglichkeit.
Sein Kopfball endete aber in den Händen des Hachinger Torwarts.
So blieb es bis zum Ende bei der knappen 0:1-Führung für die Hanseaten, die nach diesem Sieg sehr gute Chancen auf den Aufstieg im nächsten Spiel haben. 

Nach den Expected Goals gehen die Hausherren als Sieger aus dem Spiel.
Demnach gewinnen sie mit 1,65:0,71.
Das bedeutet auch, dass die Bayern über 64% der Spiele gewinnen, während die Hanseaten in nur guten 12% der Spiele als Sieger vom Platz gehen.
Die restlichen über 23% der Spiele enden unentschieden.
Bei den wahrscheinlichsten Ergebnissen liegt ein 1:0-Heimsieg mit 17,2% vorne.
Dahinter folgen ein 2:0-Heimsieg (16,0%), ein 1:1-Unentschieden (13,8%) und ein 2:1-Heimsieg (12,8%).
Ein 0:1-Auswärtssieg ist mit 4,2% eher unwahrscheinlich.