---
title: "Kurzanalyse 3. Spieltag: Hansa Rostock gegen KFC Uerdingen"
description: "Kurzanalyse zum 3. Spieltag 2020/21 zwischen Hansa Rostock und KFC Uerdingen"
tags: [3. Liga, FC Hansa Rostock, KFC Uerdingen, Nico Neidhart, Maurice Litka]
categories: []
---

Am dritten Spieltag trafen die Rostocker im heimischen Ostseestadion auf den bis dahin punktlosen KFC Uerdingen. Die ereignisarme Partie endete mit 0:0.

<canvas id="xgplot032021"></canvas>
<canvas id="winratio032021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['3'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot032021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.KFC_UERDINGEN,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio032021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Der FC Hansa war von Beginn an das spiel- und tempobestimmende Team.
Die Krefelder konzentrierten sich hauptsächtlich auf die Defensivarbeit und versuchten über Konter vor das Rostocker Tor zu kommen.
Aufgrund dieser Ausgangslage taten sich die Hanseaten schwer in das letzte Angriffsdrittel zu kommen und hatten nur durch Nico Neidhart in der 12. Minute und Maurice Litka in der 43. Minute ernsthafte Chancen auf einen Torerfolg.
Beide Möglichkeiten verfehlten das Tor jedoch deutlich.
Auf der Gegenseite blieben die Gäste gänzlich ohne gefährliche Torchance.

Nach der Halbzeit nahmen die Rostocker das Tempo etwas raus und ließen die Gäste mehr in das Spiel kommen.
Doch auch aus dieser Konstellation entwickelte sich kein ansehnliches Spiel und beide Mannschaften schafften es nicht gefährliche Torchancen zu kreieren.

Nach Expected Goals liegen die Rostocker knapp mit 0,49:0,21 vorne.
Das bedeutet, dass über die Hälfte der Spiele (54%) Unentschieden enden.
Die Hanseaten gewinnen mit fast 34% Wahrscheinlichkeit und verlieren mit 12% Wahrscheinlichkeit.
Entsprechend ist ein 0:0 mit über 48% auch das wahrscheinlichste Ergebnis.
Mit fast 26% Wahrscheinlichkeit gewinnen die Hausherren mit 1:0.
Die Gäste gewinnen mit etwa 10% Wahrscheinlickeit mit 0:1.