---
title: "Kurzanalyse 7. Spieltag: 1. FC Nürnberg gegen Hansa Rostock"
description: "Kurzanalyse zum 7. Spieltag 2021/22 zwischen 1. FC Nürnberg gegen Hansa Rostock"
tags: [2. Bundesliga, FC Hansa Rostock, 1. FC Nürnberg, Erik Shuranov]
categories: []
---

<canvas id="xgplot072122"></canvas>
<canvas id="winratio072122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['7'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot072122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_NÜRNBERG,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio072122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio072122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>
