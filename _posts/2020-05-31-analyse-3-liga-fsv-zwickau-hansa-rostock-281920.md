---
title: "Kurzanalyse 28. Spieltag: FSV Zwickau gegen Hansa Rostock"
description: "Kurzanalyse zum 28. Spieltag 2019/20 zwischen FSV Zwickau und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, FSV Zwickau, John Verhoek, Markus Kolke, Elias Huth, Max Reinthaler, Nikolas Nartey, Johannes Brinkies, Leon Jensen, Davy Frick, Nico Granatowski, Julius Reinhardt, Daniel Hanslik, Nico Neidhart]
categories: []
---

Im ersten Spiel nach der Saisonunterbrechung ging es für die Kicker von der Ostseeküste mit den Flieger nach Zwickau.
Nach 90 umkämpften Minuten teilten sich beide Mannschaften mit einem 2:2 die Punkte.

<canvas id="xgplot281920"></canvas>
<canvas id="winratio281920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['28'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot281920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FSV_ZWICKAU,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio281920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die erste Halbzeit begann ohne große Abtastphase und mit zwei Mannschaften auf Augenhöhe.
Die erste Torchance hatten dann gleich die Rostocker in der zweiten Minute.
John Verhoeks Schuss aus vollem Lauf konnte in letzter Sekunde aber noch vom Zwickauer Verteidiger geblockt werden.
Nur eine Minute später konnten die Hausherren nach einem Konter mit einem Distanzschuss erstmals auf sich aufmerksam machen.
Markus Kolke konnte den Schuss von Elias Huth ohne Probleme parieren.
Zehn Minuten später hatten die Rostocker durch Max Reinthaler, der eine Nartey-Ecke auf den Kopf bekam, die nächste gute Möglichkeit.
Er scheiterte jedoch am Zwickau Schlussmann Johannes Brinkies.
Kurze Zeit später wurde es wieder auf der anderen Seite des Platzes gefährlich.
Reinthaler passt nach einer bereits geklärten Situation den Ball direkt zu Leon Jensen, der sofort aus der Distanz den Abschluss sucht.
Doch auch diesen Ball konnte Kolke parieren.
Anschließend übernahmen die Hanseaten immer mehr die Spielkontrolle und hatten in der 21. Minute die nächste gute Chance.
Wieder war es eine Nartey-Ecke, die dieses Mal bei Verhoek landete und von Huth geklärt wurde.
Bei der darauffolgenden Ecke konnte sich der Zwickauer Davy Frick nur noch mit einer Ringereinlage gegen Verhoek wehren und so musste der Schiedsrichter auf Elfmeter entscheiden.
Diesen verwandelte Nico Granatowski souverän zu seinem ersten Treffer für den FC Hansa.
Nur fünf Minute danach erhöhten die Rostocker zum 2:0.
Granatowski setzte zuvor den Zwickauer Verteidiger unter Druck und gewann dabei den Ball in Straufraumnähe.
Den Querpass auf Verhoek verwandelte dieser dann sicher.
Mit der sicheren Führung im Rücken schalteten die Rostocker dann wieder einen Gang zurück ohne dabei die Spielkontrolle abzugeben.
Zwei Minuten vor der Halbzeit hatten die Sachsen aber nochmal die Chance auf den Anschlusstreffer.
Julius Reinhardts Seitfallzieher ging aber weit über das Tor und landete auf der leeren Tribüne hinter dem Tor.
Mit der 2:0-Führung der Rostocker ging es für beide Mannschaften dann in die Kabine.

Nach der Pause traten die Zwickauer mutiger auf und bestimmten das Spiel.
Es gab zunächst mehrere ungefährlichere Chancen auf beiden Seiten (Nartey, Frick, Reinhardt) ehe der eingewechslte Nils Miatke nach einer unfreiwilligen Vorlage von Daniel Hanslik alleine auf das Tor zulief und zum 2:1 verwandelte.
Kurz danach hatte Nico Neidhart auf der Gegenseite die Chance auf 3:1 zu erhöhen.
Die Flanke von Hanslik konnte er aber nicht voll treffen und so konnte Brinkies diesen Schuss über die Latte lenken.
Im Anschluss setzten die Zwickauer immer wieder kleine Nadelstiche, die allesamt nicht so richtig gefährlich wurden.
In der 73. Minute wurde es aber nochmal etwas chaotischer vor dem Gehäuse von Kolke.
Huth nutzte das Durcheinander und stocherte den Ball zum 2:2 ins Tor.
In der letzten Viertelstunde passierte vor beiden Toren dann nichts mehr und so endete das Spiel mit einem Unentschieden.

Nach Expected Goals-Metrik waren die Rostocker mit 1,34:1,05 xG das etwas bessere Team.
Die Siegwahrschienlichkeit der Hanseaten liegt bei etwa 43%, während die der Zwickauer nur bei 28% liegt.
Ebenso hoch ist auch die Unentschiedenwahrscheinlichkeit.
Mit 15% Wahrscheinlichkeit ist das 1:1 das erwartbarste Ergebnis.
Etwas unwahrscheinlicher sind ein 1:0- und 2:1-Auswärtssieg (12% und 11%).
Etwa 6% Wahrscheinlichkeit entfallen auf ein 2:2.