---
title: "Kurzanalyse 11. Spieltag: Hansa Rostock gegen Dynamo Dresden"
description: "Kurzanalyse zum 11. Spieltag 2020/21 zwischen Hansa Rostock und Dynamo Dresden"
tags: [3. Liga, FC Hansa Rostock, Dynamo Dresden, Korbinian Vollmann, Tim Knipping, Damian Roßbach, Patrick Weihrauch, Christoph Daferner, Nico Neidhart, Kevin Broll, Marco Hartmann, Maurice Litka, Pascal Breier, Philipp Hosiner, Ransford Königsdörffer, Kevin Ehlers, Bentley Baxter Bahn, Manuel Farrona Pulido, Sven Sonnenberg, Julian Riedel]
categories: []
---

Am 11. Spieltag begrüßten die Rostocker die SG Dynamo Dresden zum Ostklassiker.
Die umkämpfte Partie zwischen den beiden letzten Ostmeistern gewannen die Sachsen mit 1:3.

<canvas id="xgplot112021"></canvas>
<canvas id="winratio112021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['11'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot112021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.DYNAMO_DRESDEN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio112021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio112021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften fanden gut in die Partie und so entwickelte sich zu Beginn ein ausgeglichenes Spiel.
Gleich in der ersten Spielminute hatte Korbinian Vollmann die erste Torchance des Spiels, sein Drehschuss blieb aber in der Dresdner Abwehr hängen.
Drei Minuten später tauchten auch die Gäste erstmals vor dem Rostocker Tor auf.
Bei der Ecke kam Tim Knipping als Erster an den Ball, konnte die Kugel aber nicht zielgerichtet auf das Tor bringen, so dass das Spielgerät über das Gehäuse ging.
Weitere zehn Minuten später hatten die Sachsen die nächste Möglichkeit.
Nach einem missglückten Rückpass von Damian Roßbach fängt Patrick Weihrauch den Ball ab und legt auf Christoph Daferner ab, der dann zum 0:1 einnetzen konnte.
Die Hanseaten ließen sich davon nicht beeindrucken und versuchten den Rückstand sofort wieder auszugleichen.
In der 16. Minute hatte Nico Neidhart die nächsten Rostocker Möglichkeit, doch Kevin Broll im Dresdner Tor konnte den Ball ohne Probleme halten.
Danach beruhigte sich das Spiel wieder etwas und erst in der 30. Minute wurde es wieder vor dem Rostocker Tor gefährlich.
Nach einem Freistoß von Weihrauch kam Marco Hartmann direkt vor dem Tor an den Ball und verwandelte souverän zum 0:2.
Auch nach diesem Gegentor dauerte es nicht lange bis die Hanseaten ihrerseits zur nächsten Möglichkeit kamen.
Vier Minuten nach dem Tor konnte sich Maurice Litka mit einem Doppelpass mit Pascal Breier durch die gesamte Dresdner Abwehrreihe kombinieren, scheiterte aber dann am Dresdner Schlussmann.
Vier Minuten vor der Halbzeit konnten die Gäste dann noch auf 0:3 erhöhen, nachdem Philipp Hosiner Ransford Königsdörffer den Ball in den Lauf spielte und dieser dann nur noch einschieben musste.
Doch auch nach diesem erneuten Rückschlag steckten die Rostocker nicht auf und konnte in der 43. Minute noch den Anschlusstreffer erzielen.
Vollmann schickte Breier tief, der sich dann gegen Kevin Ehlers durchsetzte und zum 1:3 verwandelte.
Anschließend verabschiedeten sich beide Mannschaften in die Halbzeitpause.

Nach der Pause nahm die Intensität des Spiels ab und es blieb vor beiden Toren lange ruhig.
Erst in der 75. Minute gelang Bentley Baxter Bahn die erste nennenswerte Torchance der Halbzeit, sein Schuss aus etwa zehn Metern wurde aber von einem Verteidiger geblockt.
Eine Minute später hatte Manuel Farrona Pulido die nächste Möglichkeit, doch auch dieser Schuss wurde geblockt.
Fünf Minuten vor Schluss hatte Sven Sonnenberg nach einer Hereingabe von Bahn wieder eine Torchance, doch auch sein Kopfball blieb in der Verteidigungsreihe stecken.
Die letzte Möglichkeit des Spiels hatte dann Julian Riedel, aber auch hier blieb der Schuss an einem Verteidiger hängen.
Somit gingen die Gäste mit 1:3 als Sieger vom Platz.

In der Expected Goals-Statistik sieht das Spiel deutlich anders aus.
Dort gewinnen die Hanseaten mit 1,84:1,48.
Daraus ergibt sich für die Hanseaten eine Siegwahrscheinlichkeit von über 44%. Die Gäste gewinnen nur in über 28% aller Spiele. Über 26% der Spiele enden Unentschieden.
Die wahrscheinlichsten Ergebnisse sind ein 2:1-Heimsieg mit 12,7% und ein 1:1-Unentschieden mit 12,2%.
Ein 1:3-Auswärtssieg ereignet sich nur in 3% aller Spiele.