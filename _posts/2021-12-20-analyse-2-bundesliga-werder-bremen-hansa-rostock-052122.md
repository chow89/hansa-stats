---
title: "Kurzanalyse 5. Spieltag: Werder Bremen gegen Hansa Rostock"
description: "Kurzanalyse zum 5. Spieltag 2021/22 zwischen Werder Bremen gegen Hansa Rostock"
tags: [2. Bundesliga, FC Hansa Rostock, Werder Bremen, Marvin Ducksch, Nicolai Rapp]
categories: []
---

<canvas id="xgplot052122"></canvas>
<canvas id="winratio052122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['5'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot052122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.WERDER_BREMEN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio052122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio052122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>
