---
title: "Kurzanalyse 35. Spieltag: Hansa Rostock gegen FC Ingolstadt"
description: "Kurzanalyse zum 35. Spieltag 2020/21 zwischen Hansa Rostock und FC Ingolstadt"
tags: [3. Liga, FC Hansa Rostock, FC Ingolstadt, Filip Bilbija, Markus Kolke, Stefan Kutschke, Dennis Eckert Ayensa, Nik Omladic, John Verhoek]
categories: []
---

Zum aufstiegsvorentscheidenden Spiel am 35. Spieltag begrüßte der FC Hansa den FC Ingolstadt.
Die chancenarme und umkämpfte Partie endete mit einem 1:1 Unentschieden.

<canvas id="xgplot352021"></canvas>
<canvas id="winratio352021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['35'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot352021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FC_INGOLSTADT,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio352021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio352021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften begannen das Spiel mit einem defensiv Fokus, wobei die Gäste mehr um die Spielkontrolle bemüht waren.
In der 12. Minute wurden die Ingolstädter das erstmal vor dem Rostocker Tor gefährlich.
Die Verteidigung der Hanseaten konnte eine flache Hereingabe nicht klären und so landete der Ball vor den Füßen von Filip Bilbija, der aus fünf Metern dann aber an Markus Kolke scheiterte.
Nach einer halben Stunde tauchten die Bayern erneut im Rostocker Strafraum.
Wieder war es eine Flanke von der rechten Seite, die bei Stefan Kutschke landete.
Der Ingolstädter Top-Torjäger köpfte allerdings am Tor vorbei.
Nach einer ereignisarmen Schlussviertelstunde ging es für beide Mannschaften wieder in die Kabinen.

Im zweiten Spielabschnitt änderte sich zunächste nicht viel.
So kamen die Gäste in der 57. Minute zur ersten Torchance der Halbzeit.
Bei einer Ecke kam Dennis Eckert Ayensa umgeben von drei Verteidigern als Erster an den Ball und köpfte zur 0:1-Führung ein.
Die Rostocker wurden nach dem Gegentreffer aktiver und übernahmen die Spielkontrolle.
Diese Bemühungen wurden auch schnell belohnt als Nik Omladic in der 69. Minute eine langen Ball in den Strafraum der Gäste chippte und mit John Verhoek nach einem Abwehrfehler einen Abnehmer fand.
Der Niederländer schob den Ball anschließend zum 1:1 in das gegnerische Tor.
Auch in der zweiten Halbzeit blieb die Schlussviertelstunde ereignislos und so trennten sich die beiden Teams mit dem Unentschieden.

Nach Expected Goals geht das Spiel mit 0,21:1,41 klar an die Ingolstädter.
Damit gewinnen die Gäste so ein Spiel mit über 73% Wahrscheinlichkeit.
Über 22% der Spiele enden Unentschieden, während die Hanseaten nur gut 4% der Spiele gewinnen.
Entsprechend ist ein 0:1-Auswärtssieg mit 30% Wahrscheinlichkeit das häufigste Ergebnis bei diesem Spielverlauf.
Dahinter folgen ein 0:2-Auswärtssieg (23%) und ein 0:0-Unentschieden (15%).
Nur 7% der Spiele enden mit einem 1:1-Unentschieden.