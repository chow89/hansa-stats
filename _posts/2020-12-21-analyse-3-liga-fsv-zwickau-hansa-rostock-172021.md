---
title: "Kurzanalyse 17. Spieltag: FSV Zwickau gegen Hansa Rostock"
description: "Kurzanalyse zum 17. Spieltag 2020/21 zwischen FSV Zwickau und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, FSV Zwickau, Nik Omladic, Manfred Starke, Markus Kolke, Maximilian Wolfram, Marco Schikora]
categories: []
---

Im letzten Spiel des Jahres musste der FC Hansa zum FSV Zwickau.
In einem umkämpften Ostduell setzten sich die Kicker von der Ostsee am Ende mit 2:0 durch.

<canvas id="xgplot172021"></canvas>
<canvas id="winratio172021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['17'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot172021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.FSV_ZWICKAU,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio172021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio172021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Teams kämpften von Anfang an um jeden Ball und versuchten die jeweils andere Mannschaft vom eigenen Tor fernzuhalten.
So entwickelte sich ein Spiel, dass sich zu großen Teilen im Mittelfeld abspielte und wenige Highlights bot.
Die erste Chance der Partie hatten dann die Rostocker in der 16. Minute.
Nik Omladic dribbelte sich vom rechten Flügel in den Strafraum der Zwickau, zog einfach mal ab und netzte so zur 0:1-Führung ein.
Danach dauerte es nochmal 16 Minuten ehe die Hausherren durch einen Freistoß von Manfred Starke ihre erste Torchance hatte.
Markus Kolke konnte den gut getretenen Ball aber mit einer ebenso starken Parade klären.
Bei der anschließenden Ecke landete das Spielgerät auf dem Fuß von Maximilian Wolfram, dessen Schuss aber geblockt wurde.
In den verbleibenen Minuten der ersten Halbzeit ereigneten sich keine weiteren Torchancen und so ging mit der 0:1-Führung der Rostocker in die Pause.

Nur vier Minuten nach dem Seitenwechsel dribbelte sich der eingewechselte Manuel Farrona Pulido an der linken Torlinie entlang in Richtung Tor und legte den Ball danach so in die Mitte ab, dass der Zwickauer Marco Schikora den Ball ins eigene Tor schoß.
Auch nach dem Tor zum 2:0 dauerte es wieder lange, bis die Hausherren zu ihrem Spiel zurück fanden und Torgefahr kreieren konnten.
In der 62. Minute war es wieder Wolfram, der nach einem Steilpass vor dem Rostocker Tor auftauchte.
Der rausgerückte Kolke konnte aber schlimmeres verhindern und hielt den Ball im Nachfassen fest.
Vier Minuten später hatte Gerrit Wegkamp auch schon den letzten guten Torschuss der Partie, doch sein Versuch wurde von der Rostocker Verteidigung abgeblockt.
Somit blieb es bis zum Ende beim 0:2-Auswärtssieg der Hanseaten.

Nach Expected Goals gestaltet sich das Spiel jedoch etwas anders, hier gewinnen die Hausherren deutlich mit 1,08:0,11.
Dadurch gewinnen die Gastgeber diesen Vergleich auch in über 67% aller Spiele, während die Rostocker auf nur etwas mehr als 3% Siegwahrscheinlichkeit kommen, fast 30% der Spiele enden unentschieden.
Mit über 38% Wahrscheinlichkeit ist ein 1:0-Heimsieg das wahrscheinlichste Ergebnis, gefolgt von einem 0:0-Unentschieden mit über 25%.
Die Wahrscheinlichkeit für einen 0:2-Auswärtssieg liegt gerade einmal bei 0,05%.