---
title: "Kurzanalyse 5. Spieltag: Hansa Rostock gegen Sonnenhof Großaspach"
description: "Kurzanalyse zum 5. Spieltag 2019/20 zwischen Hansa Rostock und Sonnenhof Großaspach"
tags: [3. Liga, FC Hansa Rostock, SG Sonnenhof Großaspach, Nils Butzen, Maximilian Reule, Sven Sonnenberg, Kai Gehring, Pascal Breier, Kai Brünker, Jonas Behounek]
categories: []
---

Nur fünf Tage nach dem [starken Auftritt im DFB-Pokal]({% post_url 2019-08-14-analyse-dfb-pokal-hansa-rostock-vfb-stuttgart-011920 %}) ging es nun wieder im Liga-Alltag weiter.
Gast im heimischen Ostseetadion war dieses Mal die SG Sonnenhof Großaspach, die das Spiel am Ende mit 0:1 gewannen.

<canvas id="xgplot051920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['5'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot051920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SONNENHOF_GROSSASPACH,
            points: team2Shots
        }
    });
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften haben sich in der ersten Halbzeit darauf konzentriert aus einer sicheren Defensive heraus zu spielen und wenig Risiko im Spiel nach vorne einzugehen.
So neutralisierten sich beide Mannschaften im Mittelfeld und Torchancen waren auf beiden Seiten Mangelware.
Die größte Chance hatte dabei Nils Butzen, der in der 36. Minute unbedrängt abschließen konnte.
Sein Schuss aus etwa 12 Metern mit seinem schwächeren linken Fuß konnte Torhüter Maximilian Reule jedoch problemlos klären.
Vier Minuten später kam Sven Sonnenberg nach einer Ecke noch zu einer Chance per Kopf, die er aber nicht auf das Tor bringen konnte.
Die Schwaben konnten sich in den ersten 45 Minute keine nennenswerten Chancen erarbeiten, so dass es torlos und mit 0,46:0,02 xG in die Halbzeit ging.

Nach der Pause traten die Aspacher offensiver auf.
In der ersten Viertelstunde kam Verteidiger Kai Gehring zweimal nach einer Ecke aus aussichtsreichen Position zum Kopfball.
Auf der Gegenseite konnte sich Pascal Breier in der 59. Minute in eine gute Schussposition bringen.
Sein Schuss wurde aber von Maximilian Reule zur Ecke geklärt.
In der Folge neutralisierten sich beide Mannschaften wieder, ehe Großaspach in der 72. Minute mit einem Schuss von Kai Brünker die Schlussoffensive begann.
Im Anschluss erspielten sich die Schwaben mehrere kleinere Torchancen, bevor Jonas Behounek mit einem abgefälschten Schuss in der 79. Minute das einzige Tor der Partie schoss.
In den letzten zehn Minuten fokussierten sich die Gäste auf das Verteidigen dieser Führung, während die Rostocker vor allem mit langen Bällen versuchten den Ausgleich zu erzielen.
Nach 90 Minute endete das Spiel mit 0:1 und 0:75:0,84 xG.
