---
title: "Kurzanalyse 9. Spieltag: Holstein Kiel gegen Hansa Rostock"
description: "Kurzanalyse zum 9. Spieltag 2021/22 zwischen Holstein Kiel gegen Hansa Rostock"
tags: [2. Bundesliga, FC Hansa Rostock, Holstein Kiel, John Verhoek]
categories: []
---

<canvas id="xgplot092122"></canvas>
<canvas id="winratio092122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['9'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot092122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.HOLSTEIN_KIEL,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio092122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio092122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>
