---
title: "Kurzanalyse 22. Spieltag: KFC Uerdingen gegen Hansa Rostock"
description: "Kurzanalyse zum 22. Spieltag 2020/21 zwischen KFC Uerdingen und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, KFC Uerdingen, Manuel Farrona Pulido, Lukas Scherff, Pascal Breier, Hidde Jurjus, Adriano Grimaldi, Markus Kolke, Jens Härtel, Bentley Baxter Bahn, Omar Haktab Traoré, Nico Neidhart, Kolja Pusch, Dave Gnaase, Hans Anapak]
categories: []
---

Im Nachholspiel des 22. Spieltages musste der FC Hansa beim KFC Uerdingen antreten.
Nach einer sehr turbulenten Schlussphase gingen die Rostocker mit einem 0:1-Sieg vom Platz.

<canvas id="xgplot222021"></canvas>
<canvas id="winratio222021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['22'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot222021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.KFC_UERDINGEN,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio222021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio222021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Teams fokussierten sich anfangs vor allem auf die Defensivarbeit, so dass es in den Strafräumen lange ruhig blieb.
Die Hanseaten spielten sich in der 24. Minute die erste Möglichkeit der Partie heraus.
Manuel Farrona Pulido köpfte eine Flanke von Lukas Scherff, die von Pascal Breier verlängert wurde, auf das Tor.
Hidde Jurjus im Uerdinger Torwart konnte den Ball aber noch zur Ecke klären.
Die Gastgeber brauchten ihrerseits bis zur 36. Minute ehe sie erstmals gefährlich vor das Tor kamen.
Nach einer Flanke von links köpfte Adriano Grimaldi auf das Rostocker Tor.
Sein Kopfball war aber zu schwach und somit kein Problem für Markus Kolke.
Nach weiteren ereignisarmen Minuten ging es für beide Mannschaften wieder in die Kabine.

Nach dem Seitenwechsel setzte sich das Spiel zunächst so fort wie im ersten Spielabschnitt.
Die erste Chance der Halbzeit ging in der 51. Minute auf das Konto der Uerdinger.
Wieder war es Grimaldi, der nach einem Freistoß an den Ball kam, aber auch dieser Kopfball war kein Problem für Kolke.
Ab der 60. Minute erhöhten das Team von Jens Härtel die Schlagzahl und hatte auch gleich eine große Chance zur Führung.
Nach einem Freistoß von Scherff kam Breier frei vor dem Tor zum Abschluss, scheiterte aber an den Reflexen von Jurjus.
Sieben Minuten später machte es Breier dann besser.
Eine Ecke von Bentley Baxter Bahn landete vor den Füßen des Stürmers, der ohne Mühe zur 0:1-Führung einnetzte.
Mit der Führung im Rücken schalteten die Rostocker wieder einen Gang zurück und ließen die Uerdinger wieder mehr in das Spiel kommen.
Die Hausherren nutzten das Mehr an Spielanteilen aber lange nicht und kamen erst in der Nachspielzeit zu zahlreichen Torchancen.
Die erste Möglichkeit hatte Omar Haktab Traoré, der bei einer Ecke am höchsten stieg.
Nico Neidhart konnte den Kopfball auf der Linie nur noch mit der Hand klären und sah zurecht die Rote Karte.
Zum anschließenden Strafstoß trat Kolja Pusch an.
Sein Schuss konnte aber von Kolke parierte werden.
Der darauffolgende Nachschuss von Dave Gnaase ging deutlich am Tor vorbei.
Wenige Sekunden vor dem Abpfiff kam Hans Anapak nochmal zu einem Kopfball aus kurzer Distanz.
Aber auch sein Versuch ging deutlich am Tor vorbei und so blieb es beim 0:1-Auswärtserfolg für den FC Hansa.

Bei den Expected Goals liegen die Hanseaten mit 1,34:1,22 knapp vorne.
Daraus ergibt sich eine Gewinnwahrscheinlichkeit von fast 40% und eine Niederlagenwahrscheinlichkeit von guten 31%.
Die restlichen 29% entfallen auf eine Punkteteilung.
Das häufigste Ergebnis bei diesem Spielverlauf ist ein 1:1-Unentschieden (16,5%).
Dahinter folgen ein 1:2-Auswärtssieg (12%), ein 0:1-Auswärtssieg (10,7%) und ein 2:1-Heimsieg (10,4%).