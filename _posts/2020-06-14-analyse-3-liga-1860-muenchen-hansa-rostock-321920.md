---
title: "Kurzanalyse 32. Spieltag: 1860 München gegen Hansa Rostock"
description: "Kurzanalyse zum 32. Spieltag 2019/20 zwischen 1860 München und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, 1860 München, Stefan Lex, Efkan Bekiroglu, Pascal Breier, Mirnes Pepic, Kai Bülow, Dennis Dressel, Nico Granatowski]
categories: []
---

Am 32. Spieltag ging es für den FC Hansa zum Spitzenspiel zu den Löwen nach München.
Nach 90 chancenarmen Minuten gingen die Rostocker mit einem knappen 1:0-Sieg vom Platz.

<canvas id="xgplot321920"></canvas>
<canvas id="winratio321920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['32'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot321920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.TSV_1860_MÜNCHEN,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio321920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften begegneten sich im ersten Spielabschnitt auf Augenhöhe.
Die Gastgeber kontrollierten das Spiel, während die Gäste versuchten aus schnellen Umschaltsituationen Profit zu schlagen.
Durch die solide Defensivarbeit auf beiden Seiten, zeigten die Offensivversuche beider Mannschaften aber weinig Erfolg.
So waren die beiden Torchancen von Stefan Lex in der 26. Minute und Efkan Bekiroglu in der 33. Minute die einzigen Möglichkeiten der Halbzeit.
Richtig gefährlich war aber keine der beiden.
Somit ging es dann auch ohne Tore wieder in die Kabinen.

Nach dem Wiederanpfiff und der Einwechslung von Pascal Breier und Mirnes Pepic auf Rostocker Seite drehte sich das Spiel.
Die Gäste kontrollierten nun das Geschehen und ließen die Löwen nur selten in Tornähe kommen.
Nach zwei Distanzschussversuchen von Kai Bülow und Dennis Dressel konnten sich die Hanseaten nach einer Stunde für ihre Mühen belohnen.
Nico Granatowski dirbbelte sich ohne Bedrängnis in den gegnerischen Strafraum und legte dann auf den freistehende Breier ab, der dann problemlos zum 1:0 einschieben konnte.
In der Folge kippte das Spiel zwar langsam wieder in Richtung der Hausherren, doch diese konnten daraus kein Kapitel mehr schlagen.
Auch von den Rostockern kam bis auf wenige Halbchancen nichts mehr und so trennten sich beide Mannschaften schlussendlich mit 1:0.

Durch die wenigen Torchancen enden etwa 55% dieser Spiele Unentschieden.
Die Ostseekicker gewinnen 28% der Spiele, die Sechziger 17% der Spiele.
Entsprechend ist das 0:0 mit 48% Wahrscheinlichkeit das häufigste Ergebnis.
Mit 22% Wahrscheinlichkeit folgt der 1:0-Auswärtssieg, danach folgt ein 1:0-Heimsieg mit 15% Wahrscheinlichkeit.