---
title: "Kurzanalyse 22. Spieltag: Bayern München II gegen Hansa Rostock"
description: "Kurzanalyse zum 22. Spieltag 2019/20 zwischen Bayern München II und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, FC Bayern München II, Timo Kern, Pascal Breier, Max Reinthaler, Kwasi Okyere Wriedt, Mirnes Pepic, Nicolas Kühn, Markus Kolke, Maximilian Welzmüller, Nico Rieble, Nico Granatowski, Woo-Yeong Jeong]
categories: []
---

Im ersten Auswärtsspiel des Jahres zeigen die Rostocker eine ganz schwache Leistung und verlieren beim Nachwuchs des Rekordmeisters mit 1:0.

<canvas id="xgplot221920"></canvas>
<canvas id="winratio221920"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['1920'].league['22'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot221920", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.BAYERN_MÜNCHEN_II,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );

    new HansaStatsChart.WinRatioPlot("#winratio221920", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Das Spiel begann zunächst ausgeglichen.
Die Rostocker versuchten aus einer stabilen Defensive heraus offensive Akzente zu setzen, während die Münchener zunächst auf Fehler der Gäste warteten um dann schnell nach vorne zu spielen.
Die erste Chance des Spiels hatten dann auch die Gastgeber durch Timo Kern in der 9. Minute.
Sein aufgesetzter Kopfball ging über die Latte.
Eine knappe Viertelstunde später hatte erneut Kern, der dieses Mal freistehend im Rostocker Strafraum an den Ball kam, die Möglichkeit zur Führung. Der Mittelfeldspieler verzog seinen Schuss, so dass der Ball weit am Tor vorbei ging.
Nur drei Minuten später kamen auch die Hanseaten erstmals vor das bayerische Tor.
Der Kopfball von Pascal Breier verfehlt das Tor aber deutlich.
Kurz danach pfiff der Schiedsrichter ein Handspiel von Max Reinthaler im Strafraum ab.
Den fälligen Elfmeter verwandelte Kwasi Okyere Wriedt ohne Probleme zum 1:0.
In der Folge versuchten die Rostocker ihrerseits mehr am Spiel teilzunehmen.
Gegen die gut verteidigenden Bayern tat sich die Rostocker Offensive aber schwer.
In der 40. Minute entschied der Schiedsrichter erneut auf Elfmeter.
Kern soll ebenfalls im Strafraum den Ball mit der Hand gespielt haben.
Mirnes Pepic trat zum Straßstoß an und schoss den Ball über den Tor.
So blieb es bis zum Halbzeitpfiff beim 1:0 für die Gastgeber.

Im zweiten Spielabschnitt verflachte das Spiel etwas.
Die Münchener waren weiterhin die spielbestimmende Mannschaft, das nötige Risiko zur Vorentscheidung wollten sie aber nicht eingehen.
So dauerte es 30 Minuten bis wieder etwas in der Partie passierte.
Der eingewechselte Nicolas Kühn schloss eine sehenswerte Einzelaktion ab, ohne dabei eine ernsthafte Gefahr für Markus Kolke darstellen zu können.
Nur eine Minute später tauchte Maximilian Welzmüller vor dem Rostocker Tor auf.
Sein Schuss konnte von Reinthaler geblockt werden.
In der 80. Minute hatte erneut Wriedt die Möglichkeit zur Entscheidung.
Nico Rieble konnte den Schuss in letzter Sekunde noch blocken.
Vier Minuten später kamen die Ostseestädter zu ihrer einzigen Chance der Halbzeit.
Der eingewechselte Nico Granatowski verfehlte das Tor aber knapp.
Die letzte Aktion des Spiels gehörte dann den Bayern, die durch ihren Neuzugang Woo-Yeong Jeong nochmal zum Schuss kamen.
Kolke konnte den Ball aber problemlos festhalten.
Es blieb somit beim 1:0-Sieg für den Rekordmeister-Nachwuchs.

In der stochastischen Betrachtung gewinnen die Bayern sogar deutlich.
Nach Expected Goals (ohne Elfmeter) endet das Spiel mit 1,72:0,29.
Dementsprechend gewinnen die Hausherren mit 77,39% Wahrscheinlichkeit dieses Spiel.
In 18,3% der Spielen teilen sich die Mannschaften die Punkte, während die Hanseaten nur nach 4,32% der Spiele die drei Punkte mit nach Hause nehmen.
Ein ähnliches Bild zeichnen auch die erwarteten Ergebnisse.
Die Bayern gewinnen zu 23,5% mit 1:0 und zu 23,1% mit 2:0.
0:0 endet das Spiel immerhin noch in 9,78% der Fälle.
