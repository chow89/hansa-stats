---
title: "Kurzanalyse 6. Spieltag: Hansa Rostock gegen SV Darmstadt"
description: "Kurzanalyse zum 6. Spieltag 2021/22 zwischen Hansa Rostock und SV Darmstadt"
tags: [2. Bundesliga, FC Hansa Rostock, SV Darmstadt, John Verhoek, Tobias Kempe, Lukas Fröde]
categories: []
---

<canvas id="xgplot062122"></canvas>
<canvas id="winratio062122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['6'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot062122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.SV_DARMSTADT,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio062122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio062122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>
