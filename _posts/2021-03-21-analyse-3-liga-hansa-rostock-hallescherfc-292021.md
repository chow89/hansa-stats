---
title: "Kurzanalyse 29. Spieltag: Hansa Rostock gegen Hallescher FC"
description: "Kurzanalyse zum 29. Spieltag 2020/21 zwischen Hansa Rostock und Hallescher FC"
tags: [3. Liga, FC Hansa Rostock, Hallescher FC, Pascal Breier, John Verhoek, Björn Rother, Simon Rhein, Jonas Nietfeld, Markus Kolke, Terrence Boyd, Bentley Baxter Bahn, Sven Sonnenberg]
categories: []
---

Am 29. Spieltag begrüßten die Rostocker den Halleschen FC und nach einer Reihe von Geisterspielen erstmals wieder Fans im Ostseestadion.
Die chancenarme Partie gewann der FC Hansa knapp mit 1:0.

<canvas id="xgplot292021"></canvas>
<canvas id="winratio292021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['29'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot292021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.HALLESCHER_FC,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio292021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio292021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hanseaten fanden etwas besser in das Spiel und konnten gleich mit ihrer ersten Torchance in der siebten Minute in Führung gehen.
Pascal Breier schlug eine Flanke aus dem linken Halbfeld direkt auf John Verhoek, der den Ball sicher zum 1:0 einköpfen konnte.
Nach einer Viertelstunde kamen die Rostocker zu ihrer nächsten Chance.
Dieses Mal war es Björn Rother, der nach einer Ecke von Simon Rhein seinen Fuß reinhielt, aber an einem Hallenser auf der Torlinie scheiterte.
Nach der Anfangsoffensive der Heimherren fanden auch die Gäste besser in die Partie und so entwickelte sich in der Folge ein ausgegelichenes Spiel, allerdings mit wenigen Szenen in den Strafräumen.
So blieb der Rest der ersten Halbzeit gänzlich ohne nennenswerte Tormöglichkeiten.

Nach dem Wiederanpfiff dauerte es nur eine Minute bis die Gäste ihre erste gute Möglichkeit hatten.
Jonas Nietfeld dribbelte sich gut durch den Rostocker Sechzehner, kam mit seinem Abschluss aber nicht an Markus Kolke vorbei.
Zehn Minuten später landete ein langer Ball bei Terrence Boyd, der sofort abzog.
Sein Schuss konnte aber noch leicht abgefälscht werden und war dadurch keine Gefahr für Kolke.
In der 79. Minute kamen die Hanseaten erneut nach einer Ecke zu ihrer nächsten Chance.
Der Eckstoß von Bentley Baxter Bahn landete bei Sven Sonnenberg, der den Hallenser Torwart nochmal zu einer Glanzparade zwang.
Bis zum Schluss blieb es dann bei der knappen 1:0-Führung des FC Hansa.

Nach Expected Goals gewinnen die Rostocker das Spiel ebenso knapp mit 0,98:0,77.
Das bedeutet eine Siegwahrscheinlichkeit von fast 39% bei einer Niederlagenwahrscheinlichkeit von rund 27%.
Die restlichen 34% entfallen auf ein Unentschieden.
Das wahrscheinlichste Ergebnis ist ein 1:1 mit 17,7% Wahrscheinlichkeit.
Es folgen ein 1:0-Heimsieg (16,5%), ein 0:1-Auswärtssieg (14,8%) und ein 0:0-Unentschieden (13,8%).