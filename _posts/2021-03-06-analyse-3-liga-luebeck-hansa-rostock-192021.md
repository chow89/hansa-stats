---
title: "Kurzanalyse 19. Spieltag: VfB Lübeck gegen Hansa Rostock"
description: "Kurzanalyse zum 19. Spieltag 2020/21 zwischen VfB Lübeck und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, VfB Lübeck, Sven Sonnenberg, Lukas Raeder, Lion Lauberbach, Yannick Deichmann, Ryan Malone, Soufian Benyamina, Tommy Grupe, Markus Kolke, Nico Neidhart, Pascal Breier]
categories: []
---

Nach acht ungeschlagenen Spielen in Folge mussten die Rostocker im Nachholspiel des 19. Spieltages wieder eine Niederlage hinnehmen.
Das erste Aufeinandertreffen mit dem VfB Lückeck gewann der Aufsteiger knapp mit 1:0.

<canvas id="xgplot192021"></canvas>
<canvas id="winratio192021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['19'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot192021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.VFB_LÜBECK,
            points: team2Shots
        }
    });
    
    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio192021", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio192021", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Beide Mannschaften begegneten sich von Beginn an auf Augenhöhe und taten sich schwer gefährlich in das Angriffsdrittel zu kommen.
Die einzige gute Torchance der Anfangsphase hatte Sven Sonnenberg in der neunten Minute.
Der Verteidiger stieg bei einer Ecke am höchsten, konnte den Lübecker Torwart Lukas Raeder mit seinem Kopfball aber nicht überwinden.
Es dauerte bis zur 35. Minute ehe die Gäste erneut vor das Tor kamen.
Dieses Mal war es Lion Lauberbach, der von der Strafraumkante abzog, das Tor aber ein gutes Stück verfehlte.
In der Nachspielzeit der ersten Halbzeit kamen die Hausherren dann auch zu ihrer ersten guten Torchance.
Yannick Deichmann kam wenige Meter vor dem Tor freistehend an den Ball, konnte seine Möglichkeit aber nicht nutzen und schoß weit am Tor vorbei.
Anschließend ging es für beide Mannschaften in die Halbzeitpause.

Nach dem Seitenwechsel kamen die Gastgeber verbessert in das Spiel zurück.
Bereits in der 47. Minute hatte Ryan Malone bei einer Ecke die Chance zur Führung, doch sein Kopfball war nicht platziert genug und verfehlte das Tor.
Nur eine Minute später machte es dann der Ex-Hanseat Soufian Benyamina besser, der nach einem langen Einwurf und einer Vorlage vom anderen Ex-Rostocker Tommy Grupe zum 1:0 einköpfen konnte.
In der 64. Minute hatten die Lübecker sogar die Chance zum Erhöhen.
Deichmann scheiterte mit seinem Schuss aber an Markus Kolke im Rostocker Gehäuse.
In den letzten 20 Minute der Partie drehten die Rostocker dann mehr auf und versuchten zumindest einen Punkt mitzunehmen.
In der 74. Minute hatte Nico Neidhart die erste gute Möglichkeit zum Ausgleich, er scheiterte jedoch an der guten Parade des Lübecker Schlussmanns.
Nur zwei Minuten später bekam Pascal Breier einen langen Ball in den Lauf gespielt und lief auf das Lübecker Tor zu.
Ein Lübecker Verteidiger konnte ihn dann aber noch einholen und entscheidend beim Abschluss stören.
So blieb es bis zum Abpfiff bei der 1:0-Führung des Aufsteigers.

Während es im Spiel einen Sieger gab, endet das Spiel nach Expected Goals mit 1,15:1,24 fast Unentschieden.
Demnach gewinnen die Rostocker in über 32% aller Spiele und holen zumindest einen Punkt in über 30% der Spiele.
Die Lübecker gewinnen genau 37% der Partien.
Das häufigste Ergebnis ist folgerichtig ein 1:1-Unentschieden mit 17,8% Wahrscheinlichkeit.
Es folgen ein 2:1-Heimsieg (11,3%), ein 1:2-Auswärtssieg (10,4%) und ein 1:0-Heimsieg (10,3%).