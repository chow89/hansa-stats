---
title: "Kurzanalyse 4. Spieltag: Hansa Rostock gegen Dynamo Dresden"
description: "Kurzanalyse zum 4. Spieltag 2021/22 zwischen Hansa Rostock und Dynamo Dresden"
tags: [2. Bundesliga, FC Hansa Rostock, Dynamo Dresden, Heinz Mörschel, Streli Mamba, Panagiotis Vlachodimos, Julius Kade]
categories: []
---

Am 4. Spieltag empfingen die Hanseaten Dynamo Dresden zum Ostduell im Ostseestadion.
Nach 90 Minuten gewannen die Sachsen klar mit 1:3.

<canvas id="xgplot042122"></canvas>
<canvas id="winratio042122"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2122'].league['4'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot042122", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.DYNAMO_DRESDEN,
            points: team2Shots
        }
    });

    if (data.probabilities) {
        new HansaStatsChart.WinRatioPlot("#winratio042122", data.probabilities)
    } else {
        var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
            team1Shots.map(e => e[1]),
            team2Shots.map(e => e[1])
        );
        new HansaStatsChart.WinRatioPlot("#winratio042122", resultMatrix);
    }
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Wie im Spiel gewinnen die Dresdner auch bei den Expected Goals.
Hier endet die Partie mit 1,24:2,38.
Das bedeutet, dass die Gäste knapp über zwei Drittel der Spiele gewinnen, während die Rostock nur gute 13% der Spiele siegreich beenden können.
Die restlichen über 20% der Spiele enden unentscheiden.
Die wahrscheinlichsten Ergebnisse bei diesem Spielverlauf sind 1:2- (13,9%) und ein 1:3-Auswärtssieg (11,4%).