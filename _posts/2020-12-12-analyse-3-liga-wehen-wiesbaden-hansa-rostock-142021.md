---
title: "Kurzanalyse 14. Spieltag: SV Wehen Wiesbaden gegen Hansa Rostock"
description: "Kurzanalyse zum 14. Spieltag 2020/21 zwischen SV Wehen Wiesbaden und Hansa Rostock"
tags: [3. Liga, FC Hansa Rostock, SV Wehen Wiesbaden, John Verhoek, Maurice Litka, Korbinian Vollmann, Bentley Baxter Bahn, Jan Löhmannsröben, Benedict Hollerbach, Markus Kolke, Maurice Malone, Jakov Medic, Manuel Farrona Pulido]
categories: []
---

Nach zwei gewonnenen Spielen in Folge mussten die Rostocker am 14. Spieltag zum Zweitligaabsteiger Wehen Wiesbaden.
Obwohl die Gäste das bessere Team waren verloren sie mit 2:1.

<canvas id="xgplot142021"></canvas>
<canvas id="winratio142021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['14'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot142021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.WEHEN_WIESBADEN,
            points: team2Shots
        }
    });
    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio142021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Die Hanseaten fanden besser in das Spiel und hatten gleich in der vierten Minute die erste große Torchance.
John Verhoek konnte einen Rückpass auf den Torwart abfangen und dann Maurice Litka steil schicken.
Der Winkel wurde dann aber zu spitz, so dass sein Schuss am Tor vorbeiging.
In der 18. Minute hatte Verhoek dann selber die Chance die Führung zu erzielen.
Nach einem Konter über Korbinian Vollmann und Bentley Baxter Bahn lief der Niederlände frei auf den herausstürmenden Torwart zu, der den Schuss dann auch parieren konnte.
Drei Minuten später hatte der Stürmer mit einem Schuss von der Strafraumkante die nächste Möglichkeit.
Dieses Mal ging der Ball weit am Tor vorbei.
Nach 35 Minuten kamen die Hausherren auch das erste Mal gefährlich vor das Tor der Hanseaten.
Nach einem verunglückten Pass von Jan Löhmannsröben konterten die Wehener sofort und spielten Benedict Hollerbach im Strafraum frei, der dann zur 1:0-Führung einnetzen konnte.
Die Rostocker waren nach dem Gegentor verunsichert und fanden vor dem Halbzeitpfiff nicht mehr zu ihrem Spiel.
Stattdessen machten die Gastgeber weiter Druck und hatten in der 42. Minute die nächste große Möglichkeit.
Wieder konterten die Wehener die Rostocker aus und Hollerbach kam wieder zum Schuss, Markus Kolke im Rostocker Tor konnte diesen Schuss jedoch halten.
Zwei Minuten später landete der Ball bei Maurice Malone am Fünfmeterraum.
Durch die gute Deckung der Rostocker Verteidigung konnte der Stürmer an einem besseren Abschluss gehindert werden und Kolke konnte den Ball ohne größere Probleme festhalten.
Kurz danach bat der Schiedsrichter beide Mannschaften wieder in die Kabinen.

Nach der Pause setzte sich das Spiel zunächst so fort wie nach dem Tor.
Nur zwei Minuten nach dem Wiederanpfiff rollte der nächste Konter auf das Rostocker Tor zu.
Der Schuss von Malone verfehlte das Gehäuse jedoch knapp.
Nur eine Minute später spielten die Wehener mit dem nächsten Konter Hollerbach vor dem leeren Tor frei, der den Ball dann zum 2:0 einschieben konnte.
In der 66. Minute hatten die Hausherren sogar die Chance zur Vorentscheidung.
Der Freistoß von Jakov Medic landete aber in der Rostocker Mauer.
Danach fanden auch die Hanseaten wieder besser ins Spiel und hatten in der 68. Minute ihre erste Torchance nach dem Seitenwechsel.
Die flache Hereingabe von Manuel Farrona Pulido konnte Verhoek dabei zum 2:1 verwandeln.
Zehn Minuten vor dem Abpfiff hatten die Wehener durch Malone ihre letzte Möglichkeit des Spiels.
Der Kopfball des Stürmers konnte aber von Kolke noch geradeso herausgefischt werden.
Vier Minuten danach hatten die Rostocker ihrerseites die letzte Torchance auf den Ausgleich.
Die beiden Kopfbälle von Verhoek nach einer Bahn-Ecke konnten aber beide von einem Verteidiger auf der Torlinie geklärt werden und so blieb es bis zum Ende beim knappen 2:1-Sieg für die Hausherren.

In der Expected Goals-Statistik gewinnen die Hanseaten das Spiel mit 1,68:2,25.
Demnach gewinnen die Gäste in fast 53 von 100 Fällen das Spiel.
Die Hausherren gewinnen über 23% der Spiele, fast 24% der Spiele enden mit einer Punkteteilung.
Die wahrscheinlichsten Ergbnisse bei dieser Partie sind ein 1:2-Auswärtssieg (12,3%) und ein 2:2-Unentschieden (11,8%).
Mit 2:1 gewinnen die Wehener in nur 6,4% aller Spiele.