---
title: "Kurzanalyse 1. Spieltag: Hansa Rostock gegen MSV Duisburg"
description: "Kurzanalyse zum 1. Spieltag 2020/21 zwischen Hansa Rostock und MSV Duisburg"
tags: [3. Liga, FC Hansa Rostock, MSV Duisburg, Jan Löhmannsröben, Mirnes Pepic, Vincent Vermeij, Maurice Litka, Julian Riedel, Bentley Baxter Bahn, Markus Kolke, Lukas Scepanik, Björn Rother, Leo Weinkauf, Pascal Breier, Korbinian Vollmann, Nils Butzen, Ahmet Engin]
categories: []
---

Eine Woche nach dem [DFB-Pokal-Spiel gegen den VfB Stuttgart]({% post_url 2020-09-14-analyse-dfb-pokal-hansa-rostock-vfb-stuttgart-012021 %}) ging auch die 3. Liga wieder los.
Die Hanseaten begrüßten am ersten Spieltag den MSV Duisburg und gewannen mit 3:1.

<canvas id="xgplot012021"></canvas>
<canvas id="winratio012021"></canvas>
<script>
var start = function() {
    var data = {{ site.data.xg['2021'].league['1'] | jsonify }};
    var team1Shots = data.hansa;
    var team2Shots = data.opponent;

    new HansaStatsChart.XgPlot("#xgplot012021", {
        team1: {
            name: HansaStatsChart.Teams.HANSA_ROSTOCK,
            points: team1Shots
        },
        team2: {
            name: HansaStatsChart.Teams.MSV_DUISBURG,
            points: team2Shots
        }
    });

    var resultMatrix = new HansaStatsChart.ResultProbabilityCalculator().getResultProbabilities(
        team1Shots.map(e => e[1]),
        team2Shots.map(e => e[1])
    );
    new HansaStatsChart.WinRatioPlot("#winratio012021", resultMatrix);
}
if (['loaded', 'complete', 'interactive'].indexOf(document.readyState) !== -1) {
    start();
} else {
    document.addEventListener('DOMContentLoaded', start);
}
</script>

Der FC Hansa Rostock fand zunächst besser in die Partie und versuchte die Gäste in den ersten fünf Minuten förmlich zu überlaufen, große Torchancen sprangen dabei aber nicht raus.
Nachdem die Gäste aus dem Ruhrgebiet auch ins Spiel fanden, entwickelte sich ein Duell auf Augenhöhe bei dem zunächst die Duisburger die besseren Chancen hatten.
In der 9. Minuten hatten die Gäste nach einem Fehlpass von Jan Löhmannsröben durch Mirnes Pepic und Vincent Vermeij die Doppelchance zur Führung.
Beide konnten den Ball aber nicht über die Torlinie drücken.
Auf der Gegenseite kamen die Rostocker in der 23. Minute zu ihrer ersten Torchance.
Nach einem Freistoß von Maurice Litka kam Julian Riedel im Strafraum frei zum Kopfball, verfehlte das Tor jedoch knapp.
Fünf Minuten später hatte Bentley Baxter Bahn nach einem schnell nach vorne gespielten Abstoß von Kapitän Markus Kolke die nächste Chance für die Ostseekicker, doch sein Schuss ging direkt auf den Torwart und war somit keine große Gefahr.
In der 32. Minute wurde es wild im Strafraum der Hausherren.
Auf eine verunglückte Klärungsaktion von Kolke folgten mehrere Schüsse der Duisburger, die entweder geblockt werden konnten oder von Kolke selbst pariert wurden.
Der vierte Schuss von Lukas Scepanik fand dann aber seinen Weg ins Tor und so führten die Gäste mit 0:1.
Im weiteren Verlauf der Halbzeit verloren die Rostocker komplett den Faden und die Duisburger drängten auf das zweite Tor, zu einer größeren Chance führten die Angriffsbemühungen aber nicht mehr.
Somit gingen beide Mannschaften mit dem 0:1-Zwischenstand zur Halbzeit in die Kabinen.

Nach dem Seitenwechsel veränderte sich das Bild.
Hansa übernahm die Kontrolle und ließ die Duisburger gar nicht mehr zu ihrem Spiel zurückfinden.
Die erste Möglichkeit zum Ausgleich in der zweiten Halbzeit hatte Björn Rother in der 50. Minute nach einer Bahn-Ecke. Duisburgs Torhüter Leo Weinkauf konnte den Ball aber fangen.
Sechs Minuten später, wieder nach einer Bahn-Ecke, gelang den Hausherren durch Löhmannsröben dann aber der 1:1-Ausgleich.
In der 65. Minute legten die Rostocker dann gleich nach.
Pascal Breier bekam von Korbinian Vollmann einen 40-Meter-Pass direkt in den Lauf, den er dann ohne Gegenwehr der Duisburger Verteidiger zum 2:1 verwandeln konnte.
Aber damit noch nicht genug, weitere sieben Minuten später tauchte Breier erneut frei vor dem Gäste-Tor auch, doch dieses Mal blieb der Gäste-Torwart der Sieger.
In der 75. Minute folgte dann doch noch der zweite Treffer von Breier in diesem Spiel.
Nach einer flachen Hereingabe von Nils Butzen musste Breier nur noch den Fuß hinhalten und erzielte so das 3:1.
In der Folge schalteten die Rostocker einen Gang zurück und überließen den Gästen wieder mehr vom Spiel.
So kamen diese in der 80. Minute noch zu einer Möglichkeit auf den Anschlusstreffer doch Kolke konnte den Schuss von Ahmet Engin abwehren.
Bis zum Abpfiff blieb es somit beim 3:1-Heimsieg für den FC Hansa.

Bei der Betrachtung der Zahlen ist das Ergebnis nicht so deutlich, stattdessen ist es sogar ausgeglichen.
Beide Teams kommen insgesamt auf 1,68 Expected Goals.
Für die Hanseaten ergibt sich daraus eine Siegwahrscheinlichkeit von 36,8%, während die Duisburger aufgrund der etwas besseren Einzelchancen auf 37,2% Siegwahrscheinlichkeit kommen. In 26% aller Spiele endet die Partie Unentschieden.
Die wahrscheinlichsten Ergebnis bei diesem Spiel sind 1:1 (11,1%), 1:2 (11%), 2:1 (10,5%) und 2:2 (10,5%).
Mit 3:1 enden 5,2% der Spiele.